# 

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {
    
    FUD custom invoice procs
    
    @author <yourname> (<your email>)
    @creation-date 2012-03-11
    @cvs-id $Id$
}

# ---------------------------------------------------------------
# Procdures to create an invoice PDF
# ---------------------------------------------------------------

ad_proc -public fud_create_invoice_pdf {
    {-invoice_id:required}
} {
    Create a PDF for the current invoice and store it with the naming convention
    
    InvoiceNr_CustomerName_InvoiceDate_ChangeDate.pdf
    
    in a special folder.
    
    @param invoice_id invoice for which to create the PDF file 
} {

    set user_id [ad_maybe_redirect_for_registration]
    set company_project_nr_exists [im_column_exists im_projects company_project_nr]

    # ---------------------------------------------------------------
    # Get everything about the invoice
    # ---------------------------------------------------------------
    
    # A Customer document
    set customer_or_provider_join "and ci.customer_id = c.company_id"
    set provider_company "Customer"
    set recipient_select "ci.customer_id as recipient_company_id"    
    
    db_1row invoice_info "
        select
            c.*,
            i.*,
            $recipient_select ,
            ci.effective_date::date + ci.payment_days AS due_date,
            to_char(ci.effective_date,'YYYYMMDD') as ansi_invoice_date,
            ci.effective_date AS invoice_date,
            ci.cost_status_id AS invoice_status_id,
            ci.cost_type_id AS invoice_type_id,
            ci.template_id AS invoice_template_id,
            ci.*,
            ci.note as cost_note,
            ci.project_id as cost_project_id,
            to_date(to_char(ci.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + ci.payment_days as calculated_due_date,
            im_cost_center_name_from_id(ci.cost_center_id) as cost_center_name,
            im_category_from_id(ci.cost_status_id) as cost_status,
            im_category_from_id(ci.template_id) as template,
            im_category_from_id(c.default_payment_method_id) as default_payment_method,
            im_category_from_id(c.company_type_id) as company_type
        from
            im_invoices i,
            im_costs ci,
                im_companies c
        where 
            i.invoice_id=:invoice_id
            and ci.cost_id = i.invoice_id
            $customer_or_provider_join
    "

    if {![db_0or1row office_info_query "
        select *
        from im_offices
        where office_id = :invoice_office_id
    "]} {
	ns_log Notice "No office found for $invoice_id :: $invoice_office_id :: $company_name"
	return
	ad_script_abort
    }

    # ---------------------------------------------------------------
    # Check if this is an ODT template, otherwise stop
    # ---------------------------------------------------------------

    set template_type ""
    if {0 != $invoice_template_id} {
    
        # New convention, "invoice.en_US.adp"
        if {[regexp {(.*)\.([_a-zA-Z]*)\.([a-zA-Z][a-zA-Z][a-zA-Z])} $template match body loc template_type]} {
            set locale $loc
        }
    }
    
    # don't continue if it is not an ODT template
    if {$template_type ne "odt"} {
        return
        ad_script_abort
    }
    
    
    db_0or1row accounting_contact_info "
        select
                im_name_from_user_id(person_id) as company_contact_name,
                im_email_from_user_id(person_id) as company_contact_email,
                first_names as company_contact_first_names,
                last_name as company_contact_last_name
        from    persons
        where   person_id = :company_contact_id
    "
    
    # Get contact person's contact information
    set contact_person_work_phone ""
    set contact_person_work_fax ""
    set contact_person_email ""
    db_0or1row contact_info "
        select
            work_phone as contact_person_work_phone,
            fax as contact_person_work_fax,
            im_email_from_user_id(user_id) as contact_person_email
        from
            users_contact
        where
            user_id = :company_contact_id
    "
    
    
    # ----------------------------------------------------------------------------------------
    # Check if there are Dynamic Fields of type date and localize them 
    # ----------------------------------------------------------------------------------------
    
    set date_fields [list]
    set column_sql "
            select  w.widget_name,
                    aa.attribute_name
            from    im_dynfield_widgets w,
                    im_dynfield_attributes a,
                    acs_attributes aa
            where   a.widget_name = w.widget_name and
                    a.acs_attribute_id = aa.attribute_id and
                    aa.object_type = 'im_invoice' and
                    w.widget_name = 'date'
    "
    db_foreach column_list_sql $column_sql {
        set y ${attribute_name}
        set z [lc_time_fmt [subst $${y}] "%x" $locale]
        set ${attribute_name} $z
    }
    
    # ---------------------------------------------------------------
    # Format Invoice date information according to locale
    # ---------------------------------------------------------------
    
    set invoice_date_pretty [lc_time_fmt $invoice_date "%x" $locale]
    #set delivery_date_pretty2 [lc_time_fmt $delivery_date "%x" $locale]
    set delivery_date_pretty2 $delivery_date
    
    set calculated_due_date_pretty [lc_time_fmt $calculated_due_date "%x" $locale]
    
    # ---------------------------------------------------------------
    # Add subtotal + VAT + TAX = Grand Total
    # ---------------------------------------------------------------
    
    if {[im_column_exists im_costs vat_type_id]} {
        # get the VAT note. We do not overwrite the VAT value stored in
        # the invoice in case the default rate has changed for the
        # vat_type_id and this is just a reprint of the invoice
        set vat_note [im_category_string1 -category_id $vat_type_id -locale $locale]
    } else {
        set vat_note ""
    }
    
    # -------------------------
    # Deal with payment terms and variables in them
    # -------------------------
    
    if {"" == $payment_term_id} {
        set payment_term_id [db_string payment_term "select payment_term_id from im_companies where company_id = :recipient_company_id" -default ""]
    }
    set payment_terms [im_category_from_id -locale $locale $payment_term_id]
    set payment_terms_note [im_category_string1 -category_id $payment_term_id -locale $locale]
    eval [template::adp_compile -string $payment_terms_note]
    set payment_terms_note $__adp_output
    
    # -------------------------
    # Deal with payment method and variables in them
    # -------------------------
    
    set payment_method [im_category_from_id -locale $locale $payment_method_id]
    set payment_method_note [im_category_string1 -category_id $payment_method_id -locale $locale]
    eval [template::adp_compile -string $payment_method_note]
    set payment_method_note $__adp_output
    set invoice_payment_method_l10n $payment_method
    set invoice_payment_method $payment_method
    set invoice_payment_method_desc $payment_method_note
    
    # -------------------------------
    # Support for cost center text
    # -------------------------------
    set cost_center_note [lang::message::lookup $locale intranet-cost.cc_invoice_text_${cost_center_id} " "]
    
    # Set these values to 0 in order to allow to calculate the
    # formatted grand total
    if {"" == $vat} { set vat 0}
    if {"" == $tax} { set tax 0}
        
    # ---------------------------------------------------------------
    # Determine the country name and localize
    # ---------------------------------------------------------------
    
    set country_name ""
    if {"" != $address_country_code} {
        set query "
        select  cc.country_name
        from    country_codes cc
        where   cc.iso = :address_country_code"
        if { ![db_0or1row country_info_query $query] } {
            set country_name $address_country_code
        }
        set country_name [lang::message::lookup $locale intranet-core.$country_name $country_name]
    }

    # ---------------------------------------------------------------
    # Calculate the grand total
    # ---------------------------------------------------------------

    # Number formats
    set cur_format [im_l10n_sql_currency_format]
    set vat_format $cur_format
    set tax_format $cur_format
    
    # Rounding precision can be between 2 (USD,EUR, ...) and -5 (Old Turkish Lira, ...).
    set rounding_precision 2
    set rounding_factor [expr exp(log(10) * $rounding_precision)]
    set rf $rounding_factor

    db_1row calc_grand_total "select	i.*,
        round((i.grand_total * :vat / 100 ), :rounding_precision) as vat_amount,
        round((i.grand_total * :tax / 100 ), :rounding_precision) as tax_amount,
        i.grand_total
            + round((i.grand_total * :vat / 100 ), :rounding_precision)
            + round((i.grand_total * :tax / 100 ), :rounding_precision)
        as total_due
    from
        (select
            max(i.currency) as currency,
            sum(i.amount) as subtotal,
            round(sum(i.amount) * :surcharge_perc::numeric) / 100.0 as surcharge_amount,
            round(sum(i.amount) * :discount_perc::numeric) / 100.0 as discount_amount,
            sum(i.amount)
                + round(sum(i.amount) * :surcharge_perc::numeric) / 100.0
                + round(sum(i.amount) * :discount_perc::numeric) / 100.0
            as grand_total
        from 
            (select	ii.*,
                round((ii.price_per_unit * ii.item_units ), :rounding_precision) as amount
            from	im_invoice_items ii,
                im_invoices i
            where	i.invoice_id = ii.invoice_id
                and i.invoice_id = :invoice_id
            ) i
        ) i"
    
    set subtotal_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $subtotal+0] $rounding_precision] "" $locale]
    set vat_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat_amount+0] $rounding_precision] "" $locale]
    set tax_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax_amount+0] $rounding_precision] "" $locale]
    
    set vat_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat+0] $rounding_precision] "" $locale]
    set tax_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax+0] $rounding_precision] "" $locale]
    set grand_total_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $grand_total+0] $rounding_precision] "" $locale]
    set total_due_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $total_due+0] $rounding_precision] "" $locale]
    set discount_perc_pretty $discount_perc
    set surcharge_perc_pretty $surcharge_perc
    
    set discount_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $discount_amount+0] $rounding_precision] "" $locale]
    set surcharge_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $surcharge_amount+0] $rounding_precision] "" $locale]

    # ---------------------------------------------------------------
    # Get everything about the "internal" company
    # ---------------------------------------------------------------
    
    set internal_company_id [im_company_internal]
    
    db_1row internal_company_info "
        select
            c.company_name as internal_name,
            c.company_path as internal_path,
            c.vat_number as internal_vat_number,
            c.site_concept as internal_web_site,
            im_name_from_user_id(c.manager_id) as internal_manager_name,
            im_email_from_user_id(c.manager_id) as internal_manager_email,
            c.primary_contact_id as internal_primary_contact_id,
            im_name_from_user_id(c.primary_contact_id) as internal_primary_contact_name,
            im_email_from_user_id(c.primary_contact_id) as internal_primary_contact_email,
            c.accounting_contact_id as internal_accounting_contact_id,
            im_name_from_user_id(c.accounting_contact_id) as internal_accounting_contact_name,
            im_email_from_user_id(c.accounting_contact_id) as internal_accounting_contact_email,
            o.office_name as internal_office_name,
            o.fax as internal_fax,
            o.phone as internal_phone,
            o.address_line1 as internal_address_line1,
            o.address_line2 as internal_address_line2,
            o.address_city as internal_city,
            o.address_state as internal_state,
            o.address_postal_code as internal_postal_code,
            o.address_country_code as internal_country_code,
            cou.country_name as internal_country_name,
            paymeth.category_description as internal_payment_method_desc
        from
            im_companies c
            LEFT OUTER JOIN im_offices o ON (c.main_office_id = o.office_id)
            LEFT OUTER JOIN country_codes cou ON (o.address_country_code = iso)
            LEFT OUTER JOIN im_categories paymeth ON (c.default_payment_method_id = paymeth.category_id)
        where
            c.company_id = :internal_company_id
    "
    
    
    # Set the email and name of the current user as internal contact
    db_1row accounting_contact_info "
        select
        im_name_from_user_id(:user_id) as internal_contact_name,
        im_email_from_user_id(:user_id) as internal_contact_email,
        uc.work_phone as internal_contact_work_phone,
        uc.home_phone as internal_contact_home_phone,
        uc.cell_phone as internal_contact_cell_phone,
        uc.fax as internal_contact_fax,
        uc.wa_line1 as internal_contact_wa_line1,
        uc.wa_line2 as internal_contact_wa_line2,
        uc.wa_city as internal_contact_wa_city,
        uc.wa_state as internal_contact_wa_state,
        uc.wa_postal_code as internal_contact_wa_postal_code,
        uc.wa_country_code as internal_contact_wa_country_code
        from
        users u
        LEFT OUTER JOIN users_contact uc ON (u.user_id = uc.user_id)
        where
        u.user_id = :user_id
    "
    
    # ---------------------------------------------------------------
    # Get more about the invoice's project
    # ---------------------------------------------------------------


    # We give priority to the project specified in the cost item,
    # instead of associated projects.
    if {"" != $cost_project_id && 0 != $cost_project_id} {
        set rel_project_id $cost_project_id
    } else {
        
        set rel_project_id [db_string related_projects "
            select  distinct r.object_id_one
            from    acs_rels r, im_projects p
            where   r.object_id_one = p.project_id
            and     r.object_id_two = :invoice_id
            order by r.object_id_one
            limit 1
        " -default 0]
    }
    
    set project_short_name_default ""
    db_0or1row project_info_query "
            select
                    project_nr as project_short_name_default,                                                                                   
                    im_category_from_id(project_type_id) as project_type_pretty                                                                 
            from                                                                                                                                
                    im_projects                                                                                                                 
            where                                                                                                                               
                    project_id = :rel_project_id                                                                                                
     "                                                                                                                                          
    
    set customer_project_nr_default ""
    if {$company_project_nr_exists && $rel_project_id} {
        set customer_project_nr_default [db_string project_nr_default "select company_project_nr from im_projects where project_id=:rel_project_id" -default ""]
    }
    
    
    # ---------------------------------------------------------------
    # Prepare the template
    # ---------------------------------------------------------------

    # Check if the given locale throws an error
    # Reset the locale to the default locale then
    if {[catch {
        lang::message::lookup $locale "intranet-core.Reporting"
    } errmsg]} {
        set locale $user_locale
    }
    
    set odt_tmp_path [ns_tmpnam]
  
    ns_mkdir $odt_tmp_path
    
    # The document 
    set odt_zip "${odt_tmp_path}.odt"
    set odt_content "${odt_tmp_path}/content.xml"
    set odt_styles "${odt_tmp_path}/styles.xml"
    
    # ------------------------------------------------
    # Create a copy of the ODT
    set invoice_template_base_path [ad_parameter -package_id [im_package_invoices_id] InvoiceTemplatePathUnix "" "/tmp/templates/"]
    set invoice_template_path "$invoice_template_base_path/$template"
    ns_cp $invoice_template_path $odt_zip
    exec unzip -d $odt_tmp_path $odt_zip 
    
    # ------------------------------------------------
    # Read the content.xml file
    set file [open $odt_content]
    fconfigure $file -encoding "utf-8"
    set odt_template_content [read $file]

    close $file
    
    # ------------------------------------------------
    # Search the <row> ...<cell>..</cell>.. </row> line
    # representing the part of the template that needs to
    # be repeated for every template.

    # Get the list of all "tables" in the document
    set odt_doc [dom parse $odt_template_content]
    set root [$odt_doc documentElement]
    set odt_table_nodes [$root selectNodes "//table:table"]

    # Search for the table that contains "@item_name_pretty"
    set odt_template_table_node ""
    foreach table_node $odt_table_nodes {
        set table_as_list [$table_node asList]
        if {[regexp {item_units_pretty} $table_as_list match]} { set odt_template_table_node $table_node }
    }

    # Deal with the the situation that we didn't find the line
    if {"" == $odt_template_table_node} {
        ns_log Error "
        <b>Didn't find table including '@item_units_pretty'</b>:<br>
        We have found a valid OOoo template at '$invoice_template_path'.
        However, this template does not include a table with the value
        above.
    "
        ad_script_abort
    }

    # Search for the 2nd table:table-row tag
    set odt_table_rows_nodes [$odt_template_table_node selectNodes "//table:table-row"]
    set odt_template_row_node ""
    set odt_template_row_count 0
    foreach row_node $odt_table_rows_nodes {
        set row_as_list [$row_node asList]
        if {[regexp {item_units_pretty} $row_as_list match]} { set odt_template_row_node $row_node }
        incr odt_template_row_count
    }
    
    if {"" == $odt_template_row_node} {
        ns_log Error "
            <b>Didn't find row including '@item_units_pretty'</b>:<br>
            We have found a valid OOoo template at '$invoice_template_path'.
            However, this template does not include a row with the value
            above.
        "
        ad_script_abort
    }
    
    # Convert the tDom tree into XML for rendering
    set odt_row_template_xml [$odt_template_row_node asXML]
    
    set ctr 1
    set oo_table_xml ""


    # ---------------------------------------------------------------
    # Prepare the invoice line items
    # ---------------------------------------------------------------
    db_foreach invoice_items "
        select
            i.*,
            p.*,
            now() as delivery_date_pretty,
            im_category_from_id(i.item_type_id) as item_type,
            im_category_from_id(i.item_uom_id) as item_uom,
            p.project_nr as project_short_name,
            round((i.price_per_unit * i.item_units ), :rounding_precision) as amount,
            to_char(round((i.price_per_unit * i.item_units ), :rounding_precision), :cur_format) as amount_formatted,
            i.currency as item_currency
       from
            im_invoice_items i
            LEFT JOIN im_projects p on i.project_id=p.project_id
       where
            i.invoice_id=:invoice_id
       order by
            i.sort_order,
            i.item_type_id
    " {

        set amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $amount+0] $rounding_precision] "" $locale]
        set item_units_pretty [lc_numeric [expr $item_units+0] "" $locale]
        set price_per_unit_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $price_per_unit+0] $rounding_precision] "" $locale]

        # Insert a new XML table row into OpenOffice document
	    set item_uom [lang::message::lookup $locale intranet-core.$item_uom $item_uom]
	
	    # Replace placeholders in the OpenOffice template row with values
	    eval [template::adp_compile -string $odt_row_template_xml]
	    set odt_row_xml [intranet_oo::convert -content $__adp_output]
	
	    # Parse the new row and insert into OOoo document
	    set row_doc [dom parse $odt_row_xml]
	    set new_row [$row_doc documentElement]
	    $odt_template_table_node insertBefore $new_row $odt_template_row_node
	

	    incr ctr
    }
    
    # ---------------------------------------------------------------
    # Build the PDF file from the ODT
    # ---------------------------------------------------------------

    # ------------------------------------------------
    # Delete the original template row, which is duplicate
    $odt_template_table_node removeChild $odt_template_row_node
    
    # ------------------------------------------------
    # Process the content.xml file
    
    set odt_template_content [$root asXML -indent 1]
    set vars_escaped [list]

    # Escaping other vars used, skip vars already escaped for multiple lines  
    set lines [split $odt_template_content \n]
    foreach line $lines {
        set var_to_be_escaped ""
        regexp -nocase {@(.*?)@} $line var_to_be_escaped    
        regsub -all "@" $var_to_be_escaped "" var_to_be_escaped
        regsub -all ";noquote" $var_to_be_escaped "" var_to_be_escaped
        if { -1 == [lsearch $vars_escaped $var_to_be_escaped] } {
            if { "" != $var_to_be_escaped  } {
                if { [info exists $var_to_be_escaped] } {
                    set value [eval "set value \"$$var_to_be_escaped\""]
		    set value [encodeXmlValue $value]
		    regsub -all {\"} $value {'} value
                    set cmd "set $var_to_be_escaped \"$value\""
                    eval $cmd
                }
            }
        }
    }
    
    # Perform replacements
    regsub -all "&lt;%" $odt_template_content "<%" odt_template_content
    regsub -all "%&gt;" $odt_template_content "%>" odt_template_content
    
    # Rendering 
    if {[catch {
        eval [template::adp_compile -string $odt_template_content]
    } err_msg]} {
        set err_txt "Error rendering Template. You might have used a placeholder that is not available. Here's a detailed error message:<br/> <strong>$err_msg</strong><br/>"
        append err_txt "Check the Configuration Manuals at <a href='www.project-open.org'>www.project-open.org</a> for a list of placeholders available and more information and tips on configuring templates."
        ns_log Error "$err_txt"
        ad_script_abort
    }
    
    set content $__adp_output
    
    # Save the content to a file.
    set file [open $odt_content w]
    fconfigure $file -encoding "utf-8"
    
    # Make some last minute conversions
    puts $file [intranet_oo::convert -content $content]
    flush $file
    close $file
    
    # Process the styles.xml file
    set file [open $odt_styles]
    fconfigure $file -encoding "utf-8"
    set style_content [read $file]
    close $file
    
    # Perform replacements
    eval [template::adp_compile -string $style_content]
    set style $__adp_output
    
    # Save the content to a file.
    set file [open $odt_styles w]
    fconfigure $file -encoding "utf-8"
    puts $file [intranet_oo::convert -content $style]
    flush $file
    close $file
    
    # Replace the files inside the odt file by the processed files
    exec zip -j $odt_zip $odt_content
    exec zip -j $odt_zip $odt_styles
    
    db_release_unused_handles

    # ---------------------------------------------------------------
    # Create the PDF file and rename it in the proper location
    # ---------------------------------------------------------------
    set pdf_path [im_filestorage_cost_path invoices]
    
    regsub -all {[^a-zA-Z0-9_]} $company_name "" pdf_company_name
    set pdf_company_name [string range $pdf_company_name 0 19]
    
    set ansi_change_date [clock format [clock seconds] -format "%Y%m%d"]
        
    set pdf_filename "${invoice_nr}_${pdf_company_name}_${ansi_invoice_date}.pdf"
    
    set output_file "${pdf_path}/$pdf_filename"
    # Remove the file if it exists
    
    
    # convert t PDF
    intranet_oo::jodconvert -oo_file $odt_zip -output_file $output_file
    
    regsub "[file dirname $odt_zip]/" [file rootname $odt_zip] "" tmpname
    set oo_file_pdf $tmpname.pdf
    
    exec /bin/mv ${pdf_path}/$oo_file_pdf ${pdf_path}/$pdf_filename
    ns_log Notice "FUD Invoices:: Created $output_file"
    return $output_file
    
}


ad_proc -public etm_create_invoice_withdir_pdf {
    {-invoice_id:required}
    {-pdfdir ""}
} {
    Create a PDF for the current invoice and store it with the naming convention
    
    InvoiceNr_CustomerName_InvoiceDate.pdf
    
    in a special folder pdfdir.
    
 
} {

    set user_id [ad_maybe_redirect_for_registration]
    set company_project_nr_exists [im_column_exists im_projects company_project_nr]
  

    # ---------------------------------------------------------------
    # Get everything about the invoice
    # ---------------------------------------------------------------
    
    # A Customer document
    set customer_or_provider_join "and ci.customer_id = c.company_id"
    set provider_company "Customer"
    set recipient_select "ci.customer_id as recipient_company_id"    
    
    db_1row invoice_info "
        select
            c.*,
            i.*,
            $recipient_select ,
            ci.effective_date::date + ci.payment_days AS due_date,
            to_char(ci.effective_date,'YYYYMMDD') as ansi_invoice_date,
            ci.effective_date AS invoice_date,
            ci.cost_status_id AS invoice_status_id,
            ci.cost_type_id AS invoice_type_id,
            ci.template_id AS invoice_template_id,
            ci.*,
            ci.note as cost_note,
            ci.project_id as cost_project_id,
            to_date(to_char(ci.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + ci.payment_days as calculated_due_date,
            im_cost_center_name_from_id(ci.cost_center_id) as cost_center_name,
            im_category_from_id(ci.cost_status_id) as cost_status,
            im_category_from_id(ci.template_id) as template,
            im_category_from_id(c.default_payment_method_id) as default_payment_method,
            im_category_from_id(c.company_type_id) as company_type
        from
            im_invoices i,
            im_costs ci,
                im_companies c
        where 
            i.invoice_id=:invoice_id
            and ci.cost_id = i.invoice_id
            $customer_or_provider_join
    "

    if {![db_0or1row office_info_query "
        select *
        from im_offices
        where office_id = :invoice_office_id
    "]} {
	ns_log Notice "No office found for $invoice_id :: $invoice_office_id :: $company_name"
	return
	ad_script_abort
    }

    # ---------------------------------------------------------------
    # Check if this is an ODT template, otherwise stop
    # ---------------------------------------------------------------

    set template_type ""
    if {0 != $invoice_template_id} {
    
        # New convention, "invoice.en_US.adp"
        if {[regexp {(.*)\.([_a-zA-Z]*)\.([a-zA-Z][a-zA-Z][a-zA-Z])} $template match body loc template_type]} {
            set locale $loc
        }
    }
    
    # don't continue if it is not an ODT template
    if {$template_type ne "odt"} {
        return
        ad_script_abort
    }
    
    
    db_0or1row accounting_contact_info "
        select
                im_name_from_user_id(person_id) as company_contact_name,
                im_email_from_user_id(person_id) as company_contact_email,
                first_names as company_contact_first_names,
                last_name as company_contact_last_name
        from    persons
        where   person_id = :company_contact_id
    "
    
    # Get contact person's contact information
    set contact_person_work_phone ""
    set contact_person_work_fax ""
    set contact_person_email ""
    db_0or1row contact_info "
        select
            work_phone as contact_person_work_phone,
            fax as contact_person_work_fax,
            im_email_from_user_id(user_id) as contact_person_email
        from
            users_contact
        where
            user_id = :company_contact_id
    "
    
    
    # ----------------------------------------------------------------------------------------
    # Check if there are Dynamic Fields of type date and localize them 
    # ----------------------------------------------------------------------------------------
    
    set date_fields [list]
    set column_sql "
            select  w.widget_name,
                    aa.attribute_name
            from    im_dynfield_widgets w,
                    im_dynfield_attributes a,
                    acs_attributes aa
            where   a.widget_name = w.widget_name and
                    a.acs_attribute_id = aa.attribute_id and
                    aa.object_type = 'im_invoice' and
                    w.widget_name = 'date'
    "
    db_foreach column_list_sql $column_sql {
        set y ${attribute_name}
        set z [lc_time_fmt [subst $${y}] "%x" $locale]
        set ${attribute_name} $z
    }
    
    # ---------------------------------------------------------------
    # Format Invoice date information according to locale
    # ---------------------------------------------------------------
    
    set invoice_date_pretty [lc_time_fmt $invoice_date "%x" $locale]
    #set delivery_date_pretty2 [lc_time_fmt $delivery_date "%x" $locale]
    set delivery_date_pretty2 $delivery_date
    
    set calculated_due_date_pretty [lc_time_fmt $calculated_due_date "%x" $locale]
    
    # ---------------------------------------------------------------
    # Add subtotal + VAT + TAX = Grand Total
    # ---------------------------------------------------------------
    
    if {[im_column_exists im_costs vat_type_id]} {
        # get the VAT note. We do not overwrite the VAT value stored in
        # the invoice in case the default rate has changed for the
        # vat_type_id and this is just a reprint of the invoice
        set vat_note [im_category_string1 -category_id $vat_type_id -locale $locale]
    } else {
        set vat_note ""
    }
    
    # -------------------------
    # Deal with payment terms and variables in them
    # -------------------------
    
    if {"" == $payment_term_id} {
        set payment_term_id [db_string payment_term "select payment_term_id from im_companies where company_id = :recipient_company_id" -default ""]
    }
    set payment_terms [im_category_from_id -locale $locale $payment_term_id]
    set payment_terms_note [im_category_string1 -category_id $payment_term_id -locale $locale]
    eval [template::adp_compile -string $payment_terms_note]
    set payment_terms_note $__adp_output
    
    # -------------------------
    # Deal with payment method and variables in them
    # -------------------------
    
    set payment_method [im_category_from_id -locale $locale $payment_method_id]
    set payment_method_note [im_category_string1 -category_id $payment_method_id -locale $locale]
    eval [template::adp_compile -string $payment_method_note]
    set payment_method_note $__adp_output
    set invoice_payment_method_l10n $payment_method
    set invoice_payment_method $payment_method
    set invoice_payment_method_desc $payment_method_note
    
    # -------------------------------
    # Support for cost center text
    # -------------------------------
    set cost_center_note [lang::message::lookup $locale intranet-cost.cc_invoice_text_${cost_center_id} " "]
    
    # Set these values to 0 in order to allow to calculate the
    # formatted grand total
    if {"" == $vat} { set vat 0}
    if {"" == $tax} { set tax 0}
        
    # ---------------------------------------------------------------
    # Determine the country name and localize
    # ---------------------------------------------------------------
    
    set country_name ""
    if {"" != $address_country_code} {
        set query "
        select  cc.country_name
        from    country_codes cc
        where   cc.iso = :address_country_code"
        if { ![db_0or1row country_info_query $query] } {
            set country_name $address_country_code
        }
        set country_name [lang::message::lookup $locale intranet-core.$country_name $country_name]
    }

    # ---------------------------------------------------------------
    # Calculate the grand total
    # ---------------------------------------------------------------

    # Number formats
    set cur_format [im_l10n_sql_currency_format]
    set vat_format $cur_format
    set tax_format $cur_format
    
    # Rounding precision can be between 2 (USD,EUR, ...) and -5 (Old Turkish Lira, ...).
    set rounding_precision 2
    set rounding_factor [expr exp(log(10) * $rounding_precision)]
    set rf $rounding_factor

    db_1row calc_grand_total "select	i.*,
        round((i.grand_total * :vat / 100 ), :rounding_precision) as vat_amount,
        round((i.grand_total * :tax / 100 ), :rounding_precision) as tax_amount,
        i.grand_total
            + round((i.grand_total * :vat / 100 ), :rounding_precision)
            + round((i.grand_total * :tax / 100 ), :rounding_precision)
        as total_due
    from
        (select
            max(i.currency) as currency,
            sum(i.amount) as subtotal,
            round(sum(i.amount) * :surcharge_perc::numeric) / 100.0 as surcharge_amount,
            round(sum(i.amount) * :discount_perc::numeric) / 100.0 as discount_amount,
            sum(i.amount)
                + round(sum(i.amount) * :surcharge_perc::numeric) / 100.0
                + round(sum(i.amount) * :discount_perc::numeric) / 100.0
            as grand_total
        from 
            (select	ii.*,
                round((ii.price_per_unit * ii.item_units ), :rounding_precision) as amount
            from	im_invoice_items ii,
                im_invoices i
            where	i.invoice_id = ii.invoice_id
                and i.invoice_id = :invoice_id
            ) i
        ) i"
    
    set subtotal_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $subtotal+0] $rounding_precision] "" $locale]
    set vat_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat_amount+0] $rounding_precision] "" $locale]
    set tax_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax_amount+0] $rounding_precision] "" $locale]
    
    set vat_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat+0] $rounding_precision] "" $locale]
    set tax_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax+0] $rounding_precision] "" $locale]
    set grand_total_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $grand_total+0] $rounding_precision] "" $locale]
    set total_due_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $total_due+0] $rounding_precision] "" $locale]
    set discount_perc_pretty $discount_perc
    set surcharge_perc_pretty $surcharge_perc
    
    set discount_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $discount_amount+0] $rounding_precision] "" $locale]
    set surcharge_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $surcharge_amount+0] $rounding_precision] "" $locale]

    # ---------------------------------------------------------------
    # Get everything about the "internal" company
    # ---------------------------------------------------------------
    
    set internal_company_id [im_company_internal]
    
    db_1row internal_company_info "
        select
            c.company_name as internal_name,
            c.company_path as internal_path,
            c.vat_number as internal_vat_number,
            c.site_concept as internal_web_site,
            im_name_from_user_id(c.manager_id) as internal_manager_name,
            im_email_from_user_id(c.manager_id) as internal_manager_email,
            c.primary_contact_id as internal_primary_contact_id,
            im_name_from_user_id(c.primary_contact_id) as internal_primary_contact_name,
            im_email_from_user_id(c.primary_contact_id) as internal_primary_contact_email,
            c.accounting_contact_id as internal_accounting_contact_id,
            im_name_from_user_id(c.accounting_contact_id) as internal_accounting_contact_name,
            im_email_from_user_id(c.accounting_contact_id) as internal_accounting_contact_email,
            o.office_name as internal_office_name,
            o.fax as internal_fax,
            o.phone as internal_phone,
            o.address_line1 as internal_address_line1,
            o.address_line2 as internal_address_line2,
            o.address_city as internal_city,
            o.address_state as internal_state,
            o.address_postal_code as internal_postal_code,
            o.address_country_code as internal_country_code,
            cou.country_name as internal_country_name,
            paymeth.category_description as internal_payment_method_desc
        from
            im_companies c
            LEFT OUTER JOIN im_offices o ON (c.main_office_id = o.office_id)
            LEFT OUTER JOIN country_codes cou ON (o.address_country_code = iso)
            LEFT OUTER JOIN im_categories paymeth ON (c.default_payment_method_id = paymeth.category_id)
        where
            c.company_id = :internal_company_id
    "
    
    
    # Set the email and name of the current user as internal contact
    db_1row accounting_contact_info "
        select
        im_name_from_user_id(:user_id) as internal_contact_name,
        im_email_from_user_id(:user_id) as internal_contact_email,
        uc.work_phone as internal_contact_work_phone,
        uc.home_phone as internal_contact_home_phone,
        uc.cell_phone as internal_contact_cell_phone,
        uc.fax as internal_contact_fax,
        uc.wa_line1 as internal_contact_wa_line1,
        uc.wa_line2 as internal_contact_wa_line2,
        uc.wa_city as internal_contact_wa_city,
        uc.wa_state as internal_contact_wa_state,
        uc.wa_postal_code as internal_contact_wa_postal_code,
        uc.wa_country_code as internal_contact_wa_country_code
        from
        users u
        LEFT OUTER JOIN users_contact uc ON (u.user_id = uc.user_id)
        where
        u.user_id = :user_id
    "
    
    # ---------------------------------------------------------------
    # Get more about the invoice's project
    # ---------------------------------------------------------------


    # We give priority to the project specified in the cost item,
    # instead of associated projects.
    if {"" != $cost_project_id && 0 != $cost_project_id} {
        set rel_project_id $cost_project_id
    } else {
        
        set rel_project_id [db_string related_projects "
            select  distinct r.object_id_one
            from    acs_rels r, im_projects p
            where   r.object_id_one = p.project_id
            and     r.object_id_two = :invoice_id
            order by r.object_id_one
            limit 1
        " -default 0]
    }
    
    set project_short_name_default ""
    db_0or1row project_info_query "
            select
                    project_nr as project_short_name_default,                                                                                   
                    im_category_from_id(project_type_id) as project_type_pretty                                                                 
            from                                                                                                                                
                    im_projects                                                                                                                 
            where                                                                                                                               
                    project_id = :rel_project_id                                                                                                
     "                                                                                                                                          
    
    set customer_project_nr_default ""
    if {$company_project_nr_exists && $rel_project_id} {
        set customer_project_nr_default [db_string project_nr_default "select company_project_nr from im_projects where project_id=:rel_project_id" -default ""]
    }
    
    
    # ---------------------------------------------------------------
    # Prepare the template
    # ---------------------------------------------------------------

    # Check if the given locale throws an error
    # Reset the locale to the default locale then
    if {[catch {
        lang::message::lookup $locale "intranet-core.Reporting"
    } errmsg]} {
        set locale $user_locale
    }
    
    set odt_tmp_path [ns_tmpnam]
  
    ns_mkdir $odt_tmp_path
    
    # The document 
    set odt_zip "${odt_tmp_path}.odt"
    set odt_content "${odt_tmp_path}/content.xml"
    set odt_styles "${odt_tmp_path}/styles.xml"
    
    # ------------------------------------------------
    # Create a copy of the ODT
    set invoice_template_base_path [ad_parameter -package_id [im_package_invoices_id] InvoiceTemplatePathUnix "" "/tmp/templates/"]
    set invoice_template_path "$invoice_template_base_path/$template"
    ns_cp $invoice_template_path $odt_zip
    exec unzip -d $odt_tmp_path $odt_zip 
    
    # ------------------------------------------------
    # Read the content.xml file
    set file [open $odt_content]
    fconfigure $file -encoding "utf-8"
    set odt_template_content [read $file]

    close $file
    
    # ------------------------------------------------
    # Search the <row> ...<cell>..</cell>.. </row> line
    # representing the part of the template that needs to
    # be repeated for every template.

    # Get the list of all "tables" in the document
    set odt_doc [dom parse $odt_template_content]
    set root [$odt_doc documentElement]
    set odt_table_nodes [$root selectNodes "//table:table"]

    # Search for the table that contains "@item_name_pretty"
    set odt_template_table_node ""
    foreach table_node $odt_table_nodes {
        set table_as_list [$table_node asList]
        if {[regexp {item_units_pretty} $table_as_list match]} { set odt_template_table_node $table_node }
    }

    # Deal with the the situation that we didn't find the line
    if {"" == $odt_template_table_node} {
        ns_log Error "
        <b>Didn't find table including '@item_units_pretty'</b>:<br>
        We have found a valid OOoo template at '$invoice_template_path'.
        However, this template does not include a table with the value
        above.
    "
        ad_script_abort
    }

    # Search for the 2nd table:table-row tag
    set odt_table_rows_nodes [$odt_template_table_node selectNodes "//table:table-row"]
    set odt_template_row_node ""
    set odt_template_row_count 0
    foreach row_node $odt_table_rows_nodes {
        set row_as_list [$row_node asList]
        if {[regexp {item_units_pretty} $row_as_list match]} { set odt_template_row_node $row_node }
        incr odt_template_row_count
    }
    
    if {"" == $odt_template_row_node} {
        ns_log Error "
            <b>Didn't find row including '@item_units_pretty'</b>:<br>
            We have found a valid OOoo template at '$invoice_template_path'.
            However, this template does not include a row with the value
            above.
        "
        ad_script_abort
    }
    
    # Convert the tDom tree into XML for rendering
    set odt_row_template_xml [$odt_template_row_node asXML]
    
    set ctr 1
    set oo_table_xml ""


    # ---------------------------------------------------------------
    # Prepare the invoice line items
    # ---------------------------------------------------------------
    db_foreach invoice_items "
        select
            i.*,
            p.*,
            now() as delivery_date_pretty,
            im_category_from_id(i.item_type_id) as item_type,
            im_category_from_id(i.item_uom_id) as item_uom,
            p.project_nr as project_short_name,
            round((i.price_per_unit * i.item_units ), :rounding_precision) as amount,
            to_char(round((i.price_per_unit * i.item_units ), :rounding_precision), :cur_format) as amount_formatted,
            i.currency as item_currency
       from
            im_invoice_items i
            LEFT JOIN im_projects p on i.project_id=p.project_id
       where
            i.invoice_id=:invoice_id
       order by
            i.sort_order,
            i.item_type_id
    " {

        set amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $amount+0] $rounding_precision] "" $locale]
        set item_units_pretty [lc_numeric [expr $item_units+0] "" $locale]
        set price_per_unit_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $price_per_unit+0] $rounding_precision] "" $locale]

        # Insert a new XML table row into OpenOffice document
	    set item_uom [lang::message::lookup $locale intranet-core.$item_uom $item_uom]
	
	    # Replace placeholders in the OpenOffice template row with values
	    eval [template::adp_compile -string $odt_row_template_xml]
	    set odt_row_xml [intranet_oo::convert -content $__adp_output]
	
	    # Parse the new row and insert into OOoo document
	    set row_doc [dom parse $odt_row_xml]
	    set new_row [$row_doc documentElement]
	    $odt_template_table_node insertBefore $new_row $odt_template_row_node
	

	    incr ctr
    }
    
    # ---------------------------------------------------------------
    # Build the PDF file from the ODT
    # ---------------------------------------------------------------

    # ------------------------------------------------
    # Delete the original template row, which is duplicate
    $odt_template_table_node removeChild $odt_template_row_node
    
    # ------------------------------------------------
    # Process the content.xml file
    
    set odt_template_content [$root asXML -indent 1]
    set vars_escaped [list]

    # Escaping other vars used, skip vars already escaped for multiple lines  
    set lines [split $odt_template_content \n]
    foreach line $lines {
        set var_to_be_escaped ""
        regexp -nocase {@(.*?)@} $line var_to_be_escaped    
        regsub -all "@" $var_to_be_escaped "" var_to_be_escaped
        regsub -all ";noquote" $var_to_be_escaped "" var_to_be_escaped
        if { -1 == [lsearch $vars_escaped $var_to_be_escaped] } {
            if { "" != $var_to_be_escaped  } {
                if { [info exists $var_to_be_escaped] } {
                    set value [eval "set value \"$$var_to_be_escaped\""]
		    set value [encodeXmlValue $value]
		    regsub -all {\"} $value {'} value
                    set cmd "set $var_to_be_escaped \"$value\""
                    eval $cmd
                }
            }
        }
    }
    
    # Perform replacements
    regsub -all "&lt;%" $odt_template_content "<%" odt_template_content
    regsub -all "%&gt;" $odt_template_content "%>" odt_template_content
    
    # Rendering 
    if {[catch {
        eval [template::adp_compile -string $odt_template_content]
    } err_msg]} {
        set err_txt "Error rendering Template. You might have used a placeholder that is not available. Here's a detailed error message:<br/> <strong>$err_msg</strong><br/>"
        append err_txt "Check the Configuration Manuals at <a href='www.project-open.org'>www.project-open.org</a> for a list of placeholders available and more information and tips on configuring templates."
        ns_log Error "$err_txt"
        ad_script_abort
    }
    
    set content $__adp_output
    
    # Save the content to a file.
    set file [open $odt_content w]
    fconfigure $file -encoding "utf-8"
    
    # Make some last minute conversions
    puts $file [intranet_oo::convert -content $content]
    flush $file
    close $file
    
    # Process the styles.xml file
    set file [open $odt_styles]
    fconfigure $file -encoding "utf-8"
    set style_content [read $file]
    close $file
    
    # Perform replacements
    eval [template::adp_compile -string $style_content]
    set style $__adp_output
    
    # Save the content to a file.
    set file [open $odt_styles w]
    fconfigure $file -encoding "utf-8"
    puts $file [intranet_oo::convert -content $style]
    flush $file
    close $file
    
    # Replace the files inside the odt file by the processed files
    exec zip -j $odt_zip $odt_content
    exec zip -j $odt_zip $odt_styles
    
    db_release_unused_handles

    # ---------------------------------------------------------------
    # Create the PDF file and rename it in the proper location
    # ---------------------------------------------------------------
    if {$pdfdir ne ""} {set pdf_path [im_filestorage_cost_path $pdfdir]} else {set pdf_path [im_filestorage_cost_path invoices]}
    
    regsub -all {[^a-zA-Z0-9_]} $company_name "" pdf_company_name
    set pdf_company_name [string range $pdf_company_name 0 19]
    
    #set ansi_change_date [clock format [clock seconds] -format "%Y%m%d"]
        
    #set pdf_filename "${invoice_nr}_${pdf_company_name}_${ansi_invoice_date}_${ansi_change_date}.pdf"
    set pdf_filename "${invoice_nr}_${pdf_company_name}_${ansi_invoice_date}.pdf"

    set output_file "${pdf_path}/$pdf_filename"

    
    ns_log Notice "pdf_filename:: $pdf_filename \n 
		   odt_filename:: $odt_zip"

    
    # convert the PDF, reactivate when jodconvert works again
    intranet_oo::jodconvert -oo_file $odt_zip -output_file $output_file
    
    regsub "[file dirname $odt_zip]/" [file rootname $odt_zip] "" tmpname
    set oo_file_pdf $tmpname.pdf
    
    exec /bin/mv ${pdf_path}/$oo_file_pdf ${pdf_path}/$pdf_filename
    ns_log Notice "FUD Invoices:: Created $output_file"
    return $output_file
     
}


ad_proc -public etm_create_invpdfs_for_agent_year {
    {-ag:required}
    {-year ""}
} {
    Create invoice.pdfs for agency for a specific year defaults to this year
} {

    set ag_lower [string tolower $ag]
    set ag_upper [string toupper $ag]

    set path "$ag_upper/$year/${ag}_AR"

    if {$year eq ""} {set year [clock format [clock seconds] -format %Y]}

  set invlist_sql [db_list invlist "select cost_id as invoice_id from im_costs 
			  where cost_type_id in ('3700','3725') 
				and to_char(effective_date, 'YYYY') = :year
				and im_name_from_id(template_id) like ('$ag_lower%') 
		    
    "]
    ns_log Notice "invpdfs for $ag_upper ready to export to $path"
    ns_log Notice "$invlist_sql"

    db_foreach invoice_id $invlist_sql {
	  etm_create_invoice_withdir_pdf -invoice_id $invoice_id -pdfdir $path
	}


}




# etm defaults
# company defaults
# invoice-template new company
ad_proc -public im_etm_invoicetemplate_default {} {
	set invt ""
	
	if {[im_profile::member_p -profile_id 467 -user_id [ad_conn user_id]]} {
       switch  [etm_dept_from_employee_id -user_id [ad_conn user_id]] {
		FUD {set invt 11000249} 
		PAN {set invt 11000300} 
		ZIS {set invt 11000228}
		DIA {set invt 11000228}
	    }
	    return $invt
	} else {
    	return ""
    }
}


# quote-template by user dept
ad_proc -public im_etm_quotetemplate_default {} {
	set qtt ""
	
	if {[im_profile::member_p -profile_id 467 -user_id [ad_conn user_id]]} {
       switch  [etm_dept_from_employee_id -user_id [ad_conn user_id]] {
            FUD {set qtt 11000253} 
            PAN {set qtt 11000304} 
            ZIS {set qtt 11000220}
            DIA {set qtt 11000220}
        }
	    return $qtt
	} else { 
        return ""
    }
}


# invoice-template by project dept
ad_proc -public etm_invoice_project_default  {
	{ -project_id "" }
} {	invoice-template by project dept } {
	
	set invt ""
	
	if {[im_profile::member_p -profile_id 467 -user_id [ad_conn user_id]]} {
       switch  [etm_dept_from_project_id -project_id $project_id] {
		FUD {set invt 11000249} 
		PAN {set invt 11000300} 
		ZIS {set invt 11000228}
		DIA {set invt 11000228}
	    }
	    return $invt
	} else {
    	return ""
    }
}


# invoice-template by project dept
ad_proc -public etm_invoice_project_default  {
	{ -project_id "" }
} {	invoice-template by project dept } {
	
	set invt ""
	
	if {[im_profile::member_p -profile_id 467 -user_id [ad_conn user_id]]} {
       switch  [etm_dept_from_project_id -project_id $project_id] {
		FUD {set invt 11000249} 
		PAN {set invt 11000300} 
		ZIS {set invt 11000228}
		DIA {set invt 11000228}
	    }
	    return $invt
	} else {
    	return ""
    }
}




# quote-template by project dept
ad_proc -public etm_quote_project_default  {
	{ -project_id "" }
} {	quote-template by project dept } {
	
	set qtt ""
	
      switch  [etm_dept_from_project_id -project_id $project_id] {
            FUD {set qtt 11000253} 
            PAN {set qtt 11000304} 
            ZIS {set qtt 11000220}
            DIA {set qtt 11000220}
        }
	    return $qtt
}


# vat-rate default
ad_proc -public etm_vatbyagent_default {
    {-user_id ""}
    {-ch "11000424"}
    {-de "11000290"}

} {
    Return vat-default depending on the user
} {
    if {$user_id eq ""} {set user_id [ad_conn user_id]}

    set vat ""
	switch  [etm_dept_from_employee_id -user_id [ad_conn user_id]] {
		FUD {set vat $ch} 
		PAN {set vat $ch} 
		ZIS {set vat $de}
		DIA {set vat $de}

    }

    return  $vat
}

# vat-rate default
ad_proc -public im_etm_vatbyagent_default {
    {-user_id ""}
    {-ch "11000424"}
    {-de "11000290"}

} {
    Return vat-default depending on the user
} {
    if {$user_id eq ""} {set user_id [ad_conn user_id]}

    set vat ""
	switch  [etm_dept_from_employee_id -user_id [ad_conn user_id]] {
		FUD {set vat $ch} 
		PAN {set vat $ch} 
		ZIS {set vat $de}
		DIA {set vat $de}

    }

    return  $vat
}

# # vat-rate default
# ad_proc -public etm_vatbyagent_default {
#     {-user_id ""}
#     {-ch "11000424"}
#     {-de "11000290"}
# 
# } {
#     Return vat-default depending on the user
# } {
#     if {$user_id eq ""} {set user_id [ad_conn user_id]}
# 
#     set vat ""
# 	switch  [etm_dept_from_employee_id -user_id [ad_conn user_id]] {
# 		FUD {set vat $ch} 
# 		PAN {set vat $ch} 
# 		ZIS {set vat $de}
# 		DIA {set vat $de}
# 
#     }
# 
#     return  $vat
# }



# vat-rate default by project dept
ad_proc -public etm_vatypebyinvoicedept_default {
    { -invoice_id "" }
    {-ch_8200 "11000424"}
    {-ch_8400 "11000465"}
    {-de_8336 "42030"}
    {-de_8338 "11000425"}
    {-de_8400 "11000290"}
    {-de_8400_2020 "11000538"}
} {
    Return vat-rate default by invoice dept
} {
    
    set vat ""
    db_1row invoice_info "select c.project_id, c.customer_id, invoice_office_id,to_char(effective_date, 'YYYYMMDD') effective_date,  
			      vat_number, comp.main_office_id,comp.company_type_id
			  from im_costs c, im_invoices i , im_offices o, im_companies comp
			  where invoice_id = cost_id 
			   and c.customer_id = comp.company_id
			   and i.invoice_office_id = o.office_id
			   and cost_id = :invoice_id"
    if {$invoice_office_id == "" } {set invoice_office_id $main_office_id}
    set country_code [db_string cc "select lower(address_country_code) as country_code from im_offices where office_id = :invoice_office_id" -default ""]
    
    
    
    switch  [etm_dept_from_project_id -project_id $project_id] {
		"FUD" - "PAN" { if {$country_code == "ch" } {set vat $ch_8400} else { set vat $ch_8200 } }
				  
		"ZIS" - "DIA" { if {$country_code == "de" } {
					 if {$effective_date > "20200630" && $effective_date < "20210101"} {set vat $de_8400_2020} else { set vat $de_8400 }
				    } elseif {[etm_country_is_eu_p -country_code $country_code]} {
					 if {$vat_number != "" } {
					  set vat $de_8336
					 } else { if { $effective_date > "20200630" && $effective_date < "20210101" } { set vat $de_8400_2020 } else { set vat $de_8400 } }
				    } else { set vat $de_8338 }
			     }
	}
    

    return  $vat
}


# vat-rate default by project dept
ad_proc -public etm_vattypebyprojectagent_default {
    { -project_id "" }
    {-ch_8200 "11000424"}
    {-ch_8400 "11000465"}
    {-de_8336 "42030"}
    {-de_8338 "11000425"}
    {-de_8400 "11000290"}
} {
    Return vat-type-id default for customer by project dept
} {
    
    set vat ""
    db_1row project_info "select vat_number, comp.main_office_id,comp.company_type_id
			  from im_projects p , im_companies comp
			  where p.company_id = comp.company_id
			    and p.project_id = :project_id"
    
    
    set country_code [db_string cc "select lower(address_country_code) as country_code from im_offices where office_id = :main_office_id" -default ""]
    
    
    
    switch  [etm_dept_from_project_id -project_id $project_id] {
		"FUD" - "PAN" { if {$country_code == "ch"} {set vat $ch_8400} else { set vat $ch_8200 } }
				  
		"ZIS" - "DIA" { if {$country_code == "de"} {
					set vat $de_8400
				    } elseif {[etm_country_is_eu_p -country_code $country_code]} {
					if {$vat_number != "" } {
					  set vat $de_8336
					} else {set vat $de_8400}
				    } else {set vat $de_8338}
			      }
	  }

    return  $vat
}


ad_proc -public fud_invt {
    {-user_id ""}
    {-fud "11000249"}
    {-zis "11000228"}
    {-pan "11000300"}
} {
    Return the invoice template depending on the user
} {
    if {$user_id eq ""} {set user_id [ad_conn user_id]}

    set invt $fud
    switch -glob [im_email_from_user_id_helper $user_id] {
        *@fachuebersetzungsdienst.com {set invt $fud}
        *@panoramalanguages.com {set invt $pan}
        *@fachuebersetzungsagentur.com {set invt $zis}
        *@fachuebersetzungsservice.com {set invt $zis}
    }

    return  $invt
}

ad_proc -public fud_next_invoice_nr {
    -cost_type_id
    -cost_center_id
    {-cost_id ""}
    {-date_format "YYYY_MM"}
    {-par_im_next_invoice_nr ""}
    {-company_id ""}
} { 
    if {$cost_center_id eq 0} {
        set cost_center_id [fud_cost_center_from_interco -interco_company_id [im_etm_agency_by_cm_default]]
    }
    set cost_center_name [im_cost_center_name $cost_center_id]
    if {$cost_id ne ""} { set cost_id_sql " and cost_id <> :cost_id" } else { set cost_id_sql ""}
    switch $cost_type_id {
        "3700" - "3725" - "3740" {
            # Invoice
            switch $cost_center_name {
                "PAN" {
                    set invoice_nr [db_string pan_invoice "select max(cost_name)::numeric +1 from im_costs where cost_name like '3%'$cost_id_sql"]
                }
                "ZIS" {
                    set invoice_nr [db_string fud_invoice "select max(cost_name)::numeric + 1 from im_costs where cost_name like '5%'$cost_id_sql"]
                }
                default {
                    set invoice_nr [db_string fud_invoice "select max(cost_name)::numeric + 1 from im_costs where cost_name like '9%'$cost_id_sql"]
                }
            }
        }
        "3702" {
            switch $cost_center_name {
                "PAN" {
                    set prefix "PAN"
                    set date_format "YYYYMM"
                    set num "0000"
		    set start_nr 1
                }
                "ZIS" {
                    set prefix "KV_"
                    set date_format "YYYYMM"
                    set num "0000"
		    set start_nr 2
                }
                default {
                    set prefix "Q"
                    set date_format "YYYY_MM"
                    set num "0000"
		    set start_nr 2
                }
            }
             
            # ----------------------------------------------------
            # Calculate the next invoice Nr by finding out the last
            # one +1
                
            # Adjust the position of the start of date and nr in the invoice_nr
            set prefix_len [string length $prefix]
            set date_start_idx [expr 1+$prefix_len]
            set date_format_len [string length $date_format]

            set nr_start_idx [expr $start_nr+$date_format_len+$prefix_len]
            set num_len [string length $num]
            set prefix_where ""

            if {$prefix_len} {
                set prefix_where "and substr(invoice_nr, 1, :prefix_len) = :prefix"
            }
                
            set sql "
                select
                    trim(max(i.nr)) as last_invoice_nr
                from
                    (select	substr(invoice_nr, :nr_start_idx,:num_len) as nr
                     from	im_invoices, dual
                     where
                        substr(invoice_nr, :date_start_idx, :date_format_len) = to_char(sysdate, :date_format)
                        $prefix_where
                    UNION
                     select '$num' as nr from dual
                    ) i
                where
                        ascii(substr(i.nr,1,1)) > 47 and ascii(substr(i.nr,1,1)) < 58 and
                        ascii(substr(i.nr,2,1)) > 47 and ascii(substr(i.nr,2,1)) < 58 and
                        ascii(substr(i.nr,3,1)) > 47 and ascii(substr(i.nr,3,1)) < 58 and
                        ascii(substr(i.nr,4,1)) > 47 and ascii(substr(i.nr,4,1)) < 58
                "
            
            set last_invoice_nr [db_string max_invoice_nr $sql -default ""]
            set last_invoice_nr [string trimleft $last_invoice_nr "0"] 

            if {[empty_string_p $last_invoice_nr]} {
                set last_invoice_nr 0
            }
            set next_number [expr $last_invoice_nr + 1]

                
            # ----------------------------------------------------
            # Put together the new invoice_nr
            switch $cost_center_name {
                "PAN" {
                    set invoice_sql "select to_char(sysdate, :date_format) || trim(to_char($next_number,'$num')) as invoice_nr from dual"
                }
                default {
                    set invoice_sql "select to_char(sysdate, :date_format)||'_'|| trim(to_char($next_number,'$num')) as invoice_nr from dual"
                }
            }
            
            set invoice_nr "$prefix[db_string next_invoice_nr $invoice_sql -default ""]"
        }
	default {
	    # ----------------------------------------------------
	    # Set the prefix for financial document type
	    set prefix ""
	    switch $cost_type_id {
		3700 { set prefix "I" }
		3702 { set prefix "Q" }
		3704 { set prefix "B" }
		3706 { set prefix "P" }
		3718 { set prefix "T" }
		3720 { set prefix "E" }
		3720 { set prefix "R" }
		3724 { set prefix "D" }
		3725 { set prefix "C" }
		default { set prefix "" }
	    }
	    # ----------------------------------------------------
	    # Calculate the next invoice Nr by finding out the last
	    # one +1

	    # Adjust the position of the start of date and nr in the invoice_nr
	    set prefix_len [string length $prefix]
	    set date_start_idx [expr 1+$prefix_len]
	    set date_format_len [string length $date_format]
	    set nr_start_idx [expr 2+$date_format_len+$prefix_len]

	    set prefix_where ""
	    if {$prefix_len} {
		set prefix_where "and substr(invoice_nr, 1, 1) = :prefix"
	    }

	    set sql "
select
    trim(max(i.nr)) as last_invoice_nr
from
    (select    substr(invoice_nr, :nr_start_idx,4) as nr
     from    im_invoices, dual
     where
        substr(invoice_nr, :date_start_idx, :date_format_len) = to_char(sysdate, :date_format)
        $prefix_where
    UNION
     select '0000' as nr from dual
    ) i
where
        ascii(substr(i.nr,1,1)) > 47 and ascii(substr(i.nr,1,1)) < 58 and
        ascii(substr(i.nr,2,1)) > 47 and ascii(substr(i.nr,2,1)) < 58 and
        ascii(substr(i.nr,3,1)) > 47 and ascii(substr(i.nr,3,1)) < 58 and
        ascii(substr(i.nr,4,1)) > 47 and ascii(substr(i.nr,4,1)) < 58
"

	    set last_invoice_nr [db_string max_invoice_nr $sql -default ""]
	    set last_invoice_nr [string trimleft $last_invoice_nr "0"]
	    if {[empty_string_p $last_invoice_nr]} {
		set last_invoice_nr 0
	    }
	    set next_number [expr $last_invoice_nr + 1]

	    # ----------------------------------------------------
	    # Put together the new invoice_nr
	    set sql "
    select
            to_char(sysdate, :date_format)||'_'||
            trim(to_char($next_number,'0000')) as invoice_nr
    from
            dual
    "
	    set invoice_nr [db_string next_invoice_nr $sql -default ""]
	    set invoice_nr "$prefix$invoice_nr"
	}
    }
    return $invoice_nr
}

ad_proc -public fud_cost_center_from_interco {
    -interco_company_id
} {
    switch $interco_company_id {
        28022 {
            # FUD
            set cost_center_id 85004
        }
        552735 {
            # PAN
            set cost_center_id 554725
        }
        279215 {
            # ZIS
            set cost_center_id 304791
        }
        default {
            #
            set cost_center_id ""
        }
    }
    return $cost_center_id
}



#proc for adding payments with received_date as optional parameter


ad_proc -public etm_payment_create_payment {
    {-cost_id ""}
    {-payment_type_id ""}
    {-note ""}
    {-actual_amount ""}
    {-received_date ""}
    {-payment_currency ""}
} {
    Generate a new payment
} {

    # ---------------------------------------------------------------
    # Defaults & Security
    # ---------------------------------------------------------------
    
    # User id already verified by filters
    if {
    [catch {
        set user_id [ad_conn user_id]
        set peeraddr [ns_conn peeraddr]
    }]
    } {
    set user_id [im_sysadmin_user_default]
    set peeraddr "0.0.0.0"
    }

    set payment_id [db_nextval "im_payments_id_seq"]
    
    if {$received_date eq ""} {
        set received_date [db_string today "select to_char(sysdate, 'YYYY-MM-DD') from dual"]
    }
    
    #if {$payment_currency eq ""} {set payment_currency "EUR"}
    # --------------------------
    # Get the values from cost item
    # --------------------------
    
    db_1row cost_info "select cost_type_id, customer_id, provider_id, (amount * (1 + coalesce(vat,0)/100 + coalesce(tax,0)/100)) as amount, currency from im_costs where cost_id = :cost_id"
    if {$actual_amount eq ""} {
        set actual_amount $amount
    }

    if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
        set company_id $customer_id
        set provider_id [im_company_internal]
    } else {
        set company_id $provider_id
        set customer_id [im_company_internal]
    }

    if {$payment_type_id eq ""} {
        set payment_method_id [db_string payment_method "select payment_method_id from im_invoices where invoice_id = :cost_id" -default ""]
        if {$payment_method_id eq ""} {
            set payment_type_id [db_string default_payment_method "select default_payment_method_id from im_companies where company_id = :customer_id" -default 0]
        } else {
            set payment_type_id $payment_method_id
        }
    }
    
     if {$payment_currency eq ""} {set payment_currency $currency}
    
    db_dml new_payment_insert "
    insert into im_payments ( 
        payment_id, 
        cost_id,
        company_id,
        provider_id,
        amount, 
        currency,
        received_date,
        payment_type_id,
        note, 
        last_modified, 
        last_modifying_user, 
        modified_ip_address
    ) values ( 
        :payment_id, 
        :cost_id,
        :customer_id,
        :provider_id,
        :actual_amount, 
        :payment_currency,
        :received_date,
        :payment_type_id,
        :note, 
        (select sysdate from dual), 
        :user_id, 
        :peeraddr
    )" 


    # ---------------------------------------------------------------
    # Update Cost Items
    # ---------------------------------------------------------------
    
    # Update paid_amount
    im_cost_update_payments $cost_id 

    # ---------------------------------------------------------------
    # Mark invoice as paid
    # ---------------------------------------------------------------
    if {$currency eq $payment_currency} {
     set paid_amount [db_string paid_amount "select sum(amount) as paid_amount from im_payments where cost_id = :cost_id" -default 0]
    }
    set gap [db_string gap "select amount - paid_amount from im_costs where cost_id = :cost_id"]
    
    if {$gap <= "5" } {
        set cost_status_id [im_cost_status_paid]
    } else {
        set cost_status_id [im_cost_status_partially_paid]
    } 

    
    if { $cost_status_id ne [im_cost_status_cancelled] || $cost_status_id ne [im_cost_status_replaced] } {
    db_dml mark_invoice_as_paid "
        update im_costs set
            cost_status_id = :cost_status_id
            where cost_id = :cost_id
        "
    }
    # Record the payment
    callback im_payment_after_create -payment_id $payment_id -payment_method_id $payment_type_id
    
    return $payment_id
}


#------------------------------------------------------------------------
# get agency from template name
#------------------------------------------------------------------------

ad_proc -public etm_split_template {
    {-template ""}
} {
    split template into significant parts and return as array 
    - only works for template structure like agency_ctORfl_costtype-paymode-corr.lang_Region.ext
     as in fud_ct_invoice-ap-corr.de_EN.fodt
} {

if {$template == "" } {	
	return
	ad_script_abort
	}

set part [split $template "."]
set firstpart [split [lindex $part 0] "_"]
set costpart [split [lindex $firstpart 2]  "-"]


set ag [lindex $firstpart 0]
set ctfl [lindex $firstpart 1]
set costtype [lindex $costpart 0]
set paymode [lindex $costpart 1]
set cor [lindex $costpart 2]

set lang [lindex $part 1] 
set ext [lindex $part 2]

set all [dict create agency $ag ctfl $ctfl cost_type $costtype paymode $paymode cor $cor lang $lang ext $ext]


    return $all
}

#------------------------------------------------------------------------
# get template_id for corresponding correction invoice template
#------------------------------------------------------------------------



ad_proc -public etm_find_correctioninvoice_template {
    {-template ""}
} {
	finds template_id for corresponding correction invoice template based on a dict parted template uses etm_split_template   
	- only works for template structure like agency_ctORfl_costtype-paymode-corr.lang_Region.ext as in 
} {

set tparts [etm_split_template -template $template]

foreach k {agency ctfl cost_type paymode cor lang ext} {
        if {[dict exists $tparts $k]} {
            set $k [dict get $tparts $k]
        } else {set $k ""}
}

set cor "-cor"
if {$paymode != ""} {set paymode "-${paymode}"}

#new template
set new_template_odt "${agency}_${ctfl}_$cost_type$paymode${cor}.${lang}.odt"
set new_template_fodt "${agency}_${ctfl}_$cost_type$paymode${cor}.${lang}.fodt"

#set new_template id

set new_template_id [im_category_from_category -category $new_template_fodt]

if {$new_template_id == "" || $new_template_id == 0} {
	set new_template_id [im_category_from_category -category $new_template_odt]
	}
 return $new_template_id
}


ad_proc -public etm_move_payment {
 {-payment_id:required}
 {-newcost_id:required}
} {move a sole payment to new invoice} {


set oldcost_id [db_string oc "select cost_id from im_payments where payment_id = :payment_id" -default 0]
set oldproject_id [db_string opc "select project_id from im_costs where cost_id = :oldcost_id" -default 0]

set project_id [db_string opc "select project_id from im_costs where cost_id = :newcost_id" -default 0]

if { $project_id ne 0 && $newcost_id != "" } {
  db_dml upd "update im_payments set cost_id = :newcost_id where payment_id = :payment_id" 
}



# update cost payments
im_cost_update_payments $newcost_id
im_cost_update_payments $oldcost_id

#update project costs
im_cost_update_project_cost_cache $project_id
im_cost_update_project_cost_cache $oldproject_id 



     set paid_amount [db_string paid_amount "select sum(amount) as paid_amount from im_payments where cost_id = :newcost_id" -default 0]

    set gap [db_string gap "select amount - paid_amount from im_costs where cost_id = :newcost_id"]
    
    if {$gap <= "5" } {
        set cost_status_id [im_cost_status_paid]
    } else {
        set cost_status_id [im_cost_status_partially_paid]
    } 


    db_dml mark_invoice_as_paid "
        update im_costs set
            cost_status_id = :cost_status_id
            where cost_id = :newcost_id
            and cost_status_id <> [im_cost_status_cancelled]
        "
    set oldcost_status_id [im_cost_status_replaced]
    db_dml mark_oldinvoice_as_replaced "update im_costs 
            set cost_status_id = :oldcost_status_id
            where cost_id = :oldcost_id
        "

## emd move payment #######################

}


# check if invoice name exists
ad_proc -public etm_invoice_exists_p {
  {-invoice_nr:required}
} {check if invoice name already exists} {
  set invoice_name_exists [db_string invexists "select 1 from im_invoices where invoice_nr = :invoice_nr" -default 0]
  return $invoice_name_exists

}


ad_proc etm_cost_template_select { 
    select_name 
    { default "" }
    { cost_type_id ""}
    { cost_center_id ""}
} {
    Returns an html select box named $select_name and defaulted to $default 
    with a list of all the partner statuses in the system
} {
  if {$cost_center_id ne 0 && $cost_center_id ne ""} {
      set cost_center [string tolower [im_cost_center_name $cost_center_id]]
      if {$cost_center_id eq "12375"} {set cost_center "fud"}
    }

  if {$cost_type_id eq ""} {
	      return [im_category_select -translate_p 0 "Intranet Cost Template" $select_name $default]
  } else {
	set template_options [db_list_of_lists templates "
	    select category,category_id 
	    from im_categories 
	    where category_type = 'Intranet Cost Template' 
	      and aux_int1=:cost_type_id 
	      and enabled_p = 't' 
	      and category like ('${cost_center}%')
	    order by sort_order"]
	if {![db_string default_included "select 1 from im_categories where category_id = :default and category_type = 'Intranet Cost Template' and aux_int1 = :cost_type_id" -default 0]} {
		lappend template_options [list [im_category_from_id $default] $default]
	}
	return [im_options_to_select_box $select_name $template_options $default]
  }
}


# get locale form etm invoices

ad_proc etm_locale_from_invoice {
  { -invoice_id ""}
} { extract locale from template by invoice_id if in correct format i.e. template.en_US.ext } {
  db_1row invtempl "select im_name_from_id(template_id) as template from im_costs where cost_id = :invoice_id"
  set locale [string range $template [string last "." $template]-5 [string last "." $template]-1]
  return $locale
}




# rewrite quote items for etm quotes



ad_proc etm_quote_items {
  { -invoice_id "" }
} {
  change quote items to list first all tasks and then all language pairs
} {

ns_log Notice "get etm quote items for $invoice_id"


   db_1row invinfo "select project_id, currency,customer_id, im_cost_center__name(cost_center_id) as cost_center, invoice_office_id from im_costs c, im_invoices i where cost_id = invoice_id and cost_id = :invoice_id"

################################## temporary FUD only
 if {$cost_center ne ""} {
   
   set main_office_id [db_string mo "select main_office_id from im_companies where company_id = :customer_id" -default ""] 
   if { $invoice_office_id eq "" } { set invoice_office_id $main_office_id }
   if { $invoice_office_id ne "" } { set country_code [db_string cc "select upper(address_country_code) from im_offices where office_id = :invoice_office_id" -default ""] }


   
    set item_list [list] 

    set trans_cert_type_id "2505"
    set cert_p [db_string cert "select 1 from im_trans_tasks where project_id = :project_id and task_type_id = :trans_cert_type_id limit 1" -default "0"]


    set locale [etm_locale_from_invoice -invoice_id $invoice_id]
    if { $locale eq ""} { set locale [lang::system::locale -site_wide] }

    set source_language_id [db_string sl "
    select distinct on (source_language_id) source_language_id
    from  im_trans_tasks t 
    where t.project_id = :project_id
    limit 1 " ]
    
    set source_language_code [im_category_from_id $source_language_id]
    set source_lang [lang::message::lookup $locale  intranet-core.string1_$source_language_id $source_language_code]



# add tasks to item list

    set task_list [db_list_of_lists tasks "
	      select distinct on (task_name) task_name, 
		  case when (billable_units + 0) = 0 then 1
                       else coalesce(billable_units,1)  
                  end as billable_units,
                  case when task_uom_id = [im_uom_page] then task_uom_id
		       when coalesce(billable_units,1) > 1 then task_uom_id
		      else [im_uom_unit] 
		  end as task_uom_id,
		  task_type_id
	  from  im_trans_tasks t 
	  where t.project_id = :project_id 
	  order by task_name"]

    foreach task $task_list {
      lappend item_list $task
    }


# add language pairs to item list
    
    set target_list [db_list targets "select distinct on (target_language_id) target_language_id 
                                        from im_trans_tasks 
                                        where project_id = :project_id
                                        order by target_language_id"]

#     foreach target $target_list {	    
#      set tl [im_category_from_id $target]
# 	  set target_lang [lang::message::lookup "" intranet-core.string1_$target $tl]
# 	  lappend item_list "{$source_lang > $target_lang} 1 322 1"
#    }
    set lastline ""
    
    switch $cost_center {
      
	"FUD"	{
		
		foreach target $target_list {	    
		  set tl [im_category_from_id $target]
		  set target_lang [lang::message::lookup "" intranet-core.string1_$target $tl]
		  lappend item_list "{$source_lang > $target_lang} 1 322 1"
		}
		
		if {$locale == "de_DE"} {
		  if {$cert_p} {set lastline [lang::message::lookup "de_DE" intranet-invoices.cert_deliv_item_fud  "Beglaubigte Übersetzung + Postversand"]
		  } else { set lastline  [lang::message::lookup "de_DE" intranet-invoices.trans_deliv_item_fud "Fachübersetzung + digitaler Versand"]
		  }
		} else { 
		  if {$cert_p} {set lastline [lang::message::lookup "en_US" intranet-invoices.cert_deliv_item_fud  "certified  + delivery"]
		  } else { set lastline  [lang::message::lookup "en_US" intranet-invoices.trans_deliv_item_fud " + delivery"]
		  }
		} 
		if {$country_code == "CH"} {set currency "CHF"}
	
	      }
	"ZIS"	{
		
		foreach target $target_list {	    
		  set tl [im_category_from_id $target]
		  set target_lang [lang::message::lookup "" intranet-core.string1_$target $tl]
		  lappend item_list "{$source_lang > $target_lang} 1 322 1"
		}
		
		if {$locale == "de_DE"} {
		  if {$cert_p} {set lastline [lang::message::lookup "de_DE" intranet-invoices.cert_deliv_item_zis "Beglaubigte Übersetzung inkl. Postversand"]
		  } else { set lastline  [lang::message::lookup "de_DE" intranet-invoices.trans_deliv_item_zis "Fachübersetzung inkl. digitaler Versand"]
		  }
		} else { 
		  if {$cert_p} {set lastline [lang::message::lookup "en_US" intranet-invoices.cert_deliv_item_zis  "certified translation"]
		  } else { set lastline  [lang::message::lookup "en_US" intranet-invoices.trans_deliv_item_zis "translation"]
		  }
		} 
	
	      }

	default { set lastline "" }
	      
    }

    if {$lastline ne "" } {lappend item_list "{$lastline} 1 322 1"}

    
    set material_id ""
    set rate "0.00"
    
 

    db_dml delete_invoice_items "
	    DELETE from im_invoice_items
	    WHERE invoice_id=:invoice_id
    "


    set sortorder 1
    foreach item $item_list {
	  set name [lindex $item 0]
	  set units [lindex $item 1]
	  set uom_id [lindex $item 2]
	  set type_id [lindex $item 3]
	  set item_id [db_nextval "im_invoice_items_seq"]
	  set material_id ""
	  set sort_order $sortorder
	  set type_id ""
    
    

	  set insert_invoice_items_sql "
	  INSERT INTO im_invoice_items (
		  item_id, item_name, 
		  project_id, invoice_id, 
		  item_units, item_uom_id, item_material_id,
		  price_per_unit, currency, 
		  sort_order, item_type_id, 
		  item_status_id, description
	  ) VALUES (
		  :item_id, :name, 
		  :project_id, :invoice_id, 
		  :units, :uom_id, :material_id,
		  :rate, :currency, 
		  :sort_order, :type_id, 
		  null, ''
	  )"

	  db_dml insert_invoice_items $insert_invoice_items_sql
	  
	  incr sortorder 
    } 
   } 
    
}


ad_proc etm_paymterms_by_customer_id {
  -company_id:required
} {
  payment term for customer
} {
  
  set payment_term_id 80100
  
  if { [im_column_exists im_companies  payment_term_id ]} {set extra_select ", payment_term_id paymterm_id"} else {set extra_select ""}

  db_1row comp_info "select company_id, company_type_id,main_office_id $extra_select from im_companies where company_id = :company_id"
  set country_code [db_string cc "select lower(address_country_code) as country_code from im_offices where office_id = :main_office_id" -default ""]

  if {$payment_term_id eq 80100} {
    #only business customers from DE or CH or customers are allowed to pay on account
    if {$company_type_id eq [etm_company_business]} { 
      switch $country_code {
	"de" - "ch" {if {![etm_newcustomer_p -customer_id $company_id]} {set payment_term_id "80110"}}

      }
    } 
  }
 
  return $payment_term_id 
}




ad_proc etm_get_cost_template_from_project_id {
  { -project_id "" }
  { -type_id "" }
} {
  get template_id for invoice type for a project
} {

# project infos and company infos
db_1row invoice_info "select p.company_id, 
			    comp.main_office_id,
			    comp.company_type_id,
			    p.project_cost_center_id, p.interco_company_id
		      from im_offices o, im_companies comp, im_projects p
		      where p.company_id = comp.company_id
			and comp.main_office_id = o.office_id
			and p.project_id = :project_id
			
"

set agency [string tolower [etm_dept_from_project_id -project_id $project_id]]



# adv
set adv ""
set payment_term_id [etm_paymterms_by_customer_id -company_id $company_id]
if {$payment_term_id < 80101 || $payment_term_id eq ""} {set adv "-adv"}


# cor
set cor ""
if {$type_id eq [im_cost_type_correction_invoice]} {set cor "-cor"}


# locale 

set locale "de_DE"

set country_code [db_string cc "select lower(address_country_code) as country_code from im_offices where office_id = :main_office_id" -default ""]
if {![etm_country_is_dach_p -country_code $country_code]} {set locale "en_US"}


# get it all together and return the id
set template_name ${agency}_ct_invoice${adv}${cor}.${locale}.odt

set template_id [db_string template_id "select category_id from im_categories where category = :template_name" -default ""]


return $template_id

}


ad_proc etm_get_correction_invoice_template {
  { -source_invoice_id "" }
  { -target_type_id "" }
} {
  get template_id for invoice type based on a source invoice
} {

if {$target_type_id eq "3725"} {

  # project infos and company infos
    db_1row invoice_info "select cost_type_id as source_cost_type_id, 
				im_name_from_id(template_id) as template
			  from im_costs
			  where cost_id = :source_invoice_id
			    
    "
    set cor "-cor"

    if {$source_cost_type_id eq "3725"} { set cor "2"}


    set templatelength [string length $template]
    set template_trail_length "11"
    set templ_base [string range $template 0 [expr [string length $template] - $template_trail_length]]
    set templ_trail [string range $template [expr $templatelength - $template_trail_length +1 ] [string length $template] ] 
    set newtemplate "${templ_base}${cor}${templ_trail}"
    set new_template_id [im_category_from_category -category $newtemplate]


    return $new_template_id
  }
}

ad_proc etm_po2billadd {
  { -poids "" }
  { -bill_id "" }
} {
  add purchase orders to bill 
} {
    foreach po_id $poids {

  
  db_exec_plsql create_invoice_rel "      select acs_rel__new (
	      null,             -- rel_id
	      'im_invoice_invoice_rel',   -- rel_type
	      :po_id,      -- object_id_one
	      :bill_id,      -- object_id_two
	      null,             -- context_id
	      null,             -- creation_user
	      null             -- creation_ip
	    )"

  db_dml upd "update im_costs set cost_status_id = 3810 where cost_id = :po_id"


  #acs_rel project and bill

  set project_id [db_string project_id "select project_id from im_costs where cost_id = :po_id"]
  if {"" != $project_id} {
  #change bill if necessary ...probably not ...
  #set bill_id 2890726

  db_exec_plsql create_projinv_rel "      select acs_rel__new (
	      null,             -- rel_id
	      'relationship',   -- rel_type
	      :project_id,      -- object_id_one
	      :bill_id,      -- object_id_two
	      null,             -- context_id
	      null,             -- creation_user
	      null             -- creation_ip
	    )"
  }
	    
	    

  # add po amount as new item to new bill


  # get po name and amount
  set po_name [db_string po_name "select cost_name as po_name from im_costs where cost_id = :po_id" -default "po_$po_id"]

  db_1row po_sum " select round(sum(item_units * price_per_unit),3) as po_sum, currency
	  from im_invoice_items
	  where invoice_id = :po_id
	  group by currency
  "

  # add new invoice item for new cost invoice
  #check if already exists to not break the proc 
  if { ![db_string po_name "select 1 from im_invoice_items where invoice_id = :bill_id and item_name = :po_name" -default "0"]} {

	set item_id [db_nextval "im_invoice_items_seq"]
	set sort_order [db_string sort_order "select count(item_id) + 1 as sort_order from im_invoice_items 
		  where invoice_id = :bill_id " -default "1"]

	set insert_invoice_items_sql "
	INSERT INTO im_invoice_items (
	    item_id, item_name,
	    project_id, invoice_id,
	    item_units, item_uom_id,
	    price_per_unit, currency,
	    sort_order,
	    item_source_invoice_id
	) VALUES (
	    :item_id, :po_name,
	    :project_id, :bill_id,
	    1, 322,
	    :po_sum, :currency,
	    :sort_order,
	    :bill_id
	)" 


	db_dml insert_invoice_items $insert_invoice_items_sql
  }                
		  
    db_1row ii "
		    select round(sum(item_units * price_per_unit),3) as items_sum
		    from im_invoice_items
		    where invoice_id = :bill_id
	    "
    
    # change cost amount
	    db_dml upd_cost_amount "update im_costs 
			    set amount = :items_sum 
			    where cost_id = :bill_id
	    "


  }

}


ad_proc -public etm_find_invoice_pdf {
    {-invoice_id:required}
} {
    return pdf from invoice direcotry 
    @param invoice_id invoice for which to find the PDF file

} {

    set find_cmd [im_filestorage_find_cmd]
    set invpath [im_filestorage_cost_path $invoice_id]
    set invoice_pdf [exec $find_cmd $invpath -noleaf -type f -name *.pdf | head -n 1]
    
    return $invoice_pdf

} 
