# 

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {
    
    FUD callback procs
    
    @author <yourname> (<your email>)
    @creation-date 2012-03-11
    @cvs-id $Id$
}


ad_proc -public -callback intranet-invoices::reminder_send -impl fud_invoice_reminder {
	-invoice_id:required
} {
	Store E-Mails send in the cost folder when the invoice reminder is send
} {
	
	fud_invoice_reminder_proc -invoice_id $invoice_id
}

ad_proc -public fud_invoice_reminder_proc {
	-invoice_id:required
} {
	Record the send reminder in the inkasso folder along with the send documents
} {
	set recipient_id [db_string recipient "select company_contact_id from im_costs, im_invoices where cost_id = :invoice_id and cost_id = invoice_id"]
	set recipient [party::name -party_id $recipient_id]
	
	#set recipient_id 182862
	
	ds_comment "Recipient:: $recipient_id"
	db_1row logfile "select message_id, l.log_id, from_addr, sender_id, subject, body, sent_date
from 
	acs_mail_log l, acs_mail_log_recipient_map lrm
where l.log_id = lrm.log_id 
and lrm.type = 'to'
and recipient_id = :recipient_id
and l.context_id = :invoice_id
order by sent_date limit 1
"
	# Create the inkasso path	
	set cost_path [im_filestorage_cost_path $invoice_id]
	set cost_name [im_name_from_id $invoice_id]
	set inkasso_path "${cost_path}/${cost_name}_inkasso"
	catch {file mkdir $inkasso_path}

	if {[exists_and_not_null sender_id]} {
		set sender [party::name -party_id $sender_id]
	} else {
		set sender "$from_addr"
	}
	
	set download_files [list]
	db_foreach files {
		select cr.title, cr.revision_id as version_id from acs_mail_log_attachment_map lam, cr_revisions cr
		where log_id = :log_id
		and cr.revision_id = lam.file_id
	} {
		set revision_path [content::revision::get_cr_file_path -revision_id $version_id]
		file copy -force $revision_path "${inkasso_path}/$title"
		append download_files "$title <br />"
	}
	
	set sent_date_pretty [lc_time_fmt $sent_date %c]
	set invoice_log_html "<html><body>
	<table>
	<tr><td>[_ intranet-mail.Sender]:</td><td>$sender</tr>
	<tr><td>[_ intranet-mail.Recipient]:</td><td>$recipient</td></tr>
	<tr><td>[_ intranet-mail.Subject]:</td><td>$subject</td></tr>
	<tr><td>[_ intranet-mail.Attachments]:</td><td>$download_files</td></tr>
	<tr><td>[_ intranet-mail.Sent_Date]:</td><td>$sent_date_pretty</td></tr>
	<tr><td>[_ intranet-mail.Body]:</td><td>$body</td></tr>
	</table>
	</div>
</body></html>
	"
	
	set file [open "${inkasso_path}/${cost_name}_reminder.html" w]
	fconfigure $file -encoding "utf-8"
	puts $file $invoice_log_html
	flush $file
	close $file
	
	#acs_mail_lite::send_immediately -from_addr malte.sussdorff@cognovis.de -to_addr malte.sussdorff@cognovis.de -subject "LOGGIN for FUD" -body "$invoice_log_html" -mime_type "text/html"
	
	return "$invoice_log_html"
}


ad_proc -public -callback im_company_after_create -impl zzz_etm_company_defaults {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set the default templates correctly
} {
    #set default_invoice_template_id [im_etm_invoicetemplate_default]
   # set default_quote_template_id [im_etm_quotetemplate_default]
    set default_invoice_template_id ""
    set default_quote_template_id ""
    set default_payment_method_id [im_payment_method_undefined]
    set interco_company_co_id ""
    set manager_id [ad_conn user_id]
    set company_status_id [im_company_status_potential]
    set vat ""
    db_dml update_company "update im_companies 
					    set   company_name = trim(leading from company_name),
						  default_invoice_template_id = null,
						  default_quote_template_id = null,
						  interco_company_co_id = null,
						  manager_id = :manager_id,
						  company_status_id = :company_status_id,
						  default_vat = null,
						  vat_type_id = null
					   where company_id = :object_id
			  "
}
 

 
ad_proc -public -callback im_company_after_update -impl zzz_etm_companies_clean_vat {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    If a company is saved automatically clean default and overwrite system callback 
} {
    set vat ""
    db_dml update_company "update im_companies set default_vat = null, vat_type_id = null where company_id = :object_id"
}
 
 

#### COMPANY OFFICE CALLBACKs #######
ad_proc -public -callback im_office_after_create -impl etm_office_postalcode_adapt_cre {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    adapt postal code for office to add country code prefix to postal code 
} { 
 

db_1row officeinfos "select upper(address_country_code) as country_code, replace(address_postal_code, ' ', '') as plz from im_offices where office_id = :object_id"

if {![regexp {^([A-Z].*)} $plz ]} {

    switch $country_code {
        "AT"    {  
                   if {[regexp {[0-9]{4}} $plz match]} {set plz "A-$plz"} else { set plz $plz }
                } 
        "CH"    {  
                   if {[regexp {[0-9]{4}} $plz match]} {set plz "CH-$plz"} else { set plz $plz }
                }  
        "DE"    {  
                   if {[regexp {[0-9]{5}} $plz match]} {set plz "D-$plz"} else { set plz $plz }
                }

         default { set plz $plz }    
      }
} else { set plz $plz }

db_dml upd_postalcode "update im_offices set address_postal_code = :plz where office_id = :object_id"

}




ad_proc -public -callback im_office_after_update -impl etm_office_postalcode_adapt_upd {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    adapt postal code for office to add country code prefix to postal code 
} { 
 

db_1row officeinfos "select upper(address_country_code) as country_code, replace(address_postal_code, ' ', '') as plz from im_offices where office_id = :object_id"

if {![regexp {^([A-Z].*)} $plz ]} {

    switch $country_code {
        "AT"    {  
                   if {[regexp {[0-9]{4}} $plz match]} {set plz "A-$plz"} else { set plz $plz }
                } 
        "CH"    {  
                   if {[regexp {[0-9]{4}} $plz match]} {set plz "CH-$plz"} else { set plz $plz }
                }  
        "DE"    {  
                   if {[regexp {[0-9]{5}} $plz match]} {set plz "D-$plz"} else { set plz $plz }
                }

         default { set plz $plz }    
      }
} else { set plz $plz }

db_dml upd_postalcode "update im_offices set address_postal_code = :plz where office_id = :object_id"

}
 

ad_proc -public -callback im_office_after_create -impl etm_office_del_leadingspaces {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    delete leading spaces in offices
} { 
 
db_dml del_spaces "update im_offices set office_name = trim(leading from office_name),
					 address_line1 = trim(leading from address_line1),
					 address_city = trim(leading from address_city)
		   where office_id = :object_id"
}
 
 
 
 
# ---------------------------------------------------------------
# PROJECT CALLBACKs
# ---------------------------------------------------------------

ad_proc -public -callback im_project_after_create -impl fud_project_cost_center {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set the cost_center_id of the project based on the interco_company_id
} {
    set interco_company_id [db_string interco "select interco_company_id from im_projects where project_id = :object_id" -default ""]
    if {$interco_company_id eq ""} {
	set interco_company_id [etm_agency_by_employee_dept]
	set cost_center_id [fud_cost_center_from_interco -interco_company_id $interco_company_id]
	db_dml update_cost_center "update im_projects set project_cost_center_id = :cost_center_id, interco_company_id = :interco_company_id where project_id = :object_id"
    } else {
	set cost_center_id [fud_cost_center_from_interco -interco_company_id $interco_company_id]
	db_dml update_cost_center "update im_projects set project_cost_center_id = :cost_center_id where project_id = :object_id"
    }
}

ad_proc -public -callback im_project_after_update -impl fud_project_cost_center_upd {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set the cost_center_id of the project based on the interco_company_id
} {
    set interco_company_id [db_string interco "select interco_company_id from im_projects where project_id = :object_id" -default ""]
    if {$interco_company_id eq ""} {
	set interco_company_id [etm_agency_by_employee_dept]
	set cost_center_id [fud_cost_center_from_interco -interco_company_id $interco_company_id]
	db_dml update_cost_center "update im_projects set project_cost_center_id = :cost_center_id, interco_company_id = :interco_company_id where project_id = :object_id"
    } else {
	set cost_center_id [fud_cost_center_from_interco -interco_company_id $interco_company_id]
	db_dml update_cost_center "update im_projects set project_cost_center_id = :cost_center_id where project_id = :object_id"
    }
}



ad_proc -public -callback im_project_after_create -impl fud_project_certified {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set the certified_translation based on skill

    This might no longer be necessary with the new certification support.

} {
        ns_log Notice "start callback fud_project_certified"

    set certified_p [db_string select_certified "select 1 from im_object_freelance_skill_map where object_id = :object_id and skill_id = 2204" -default 0]
    if {$certified_p} {
	db_dml update_certified_projects "update im_projects set certified_translation = 't' where project_id = :object_id"
    }
}




ad_proc -public -callback im_project_after_create -impl etm_project_defaults {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set  project defaults for etm projects: project_nr check, add project_lead, set quote_nr

} {

    ns_log Notice "start callback etm_project_defaults"

    
    #check projectnr
    set last_quote_nr [db_string lpn "select max(quote_nr) from im_projects where quote_nr like '2%'" -default 0]
    set last_quote_nr_ymd [string range $last_quote_nr 0 5]
    set today_ymd [clock format [clock seconds] -format "%y%m%d"]
    set next_quote_nr "${today_ymd}001"
    
    if {[regexp {^2.*[0-9]} $last_quote_nr ]} {
      if {$today_ymd > $last_quote_nr_ymd} {set next_quote_nr $next_quote_nr
	} else {  
	      incr last_quote_nr 
	      set next_quote_nr $last_quote_nr
	      }

      while {[db_string quote_exists_p "select 1 from im_projects where quote_nr = :next_quote_nr" -default 0]} {
	  incr next_quote_nr}
    }
   
    set project_lead_id [ad_conn user_id]
    db_1row project_infos "select * from im_projects where project_id = :object_id"
    ns_log Notice "before project update $project_lead_id | $next_quote_nr | $quote_date"
    
    if {$project_nr != $next_quote_nr} {
      set company_dir [im_filestorage_company_path $company_id]
      set old_path "$company_dir/$project_path"
      set dir_quotenr "$company_dir/$next_quote_nr"
  
      set err_msg "old_path $old_path not found"
      if {[catch {
	     if {[file exists $old_path]} {
		ns_log Notice "file rename $old_path $dir_quotenr"
		file rename $old_path $dir_quotenr
		}
      } err_msg]} { return $err_msg }
    }

    util_memoize_flush [list im_filestorage_base_path_helper project $project_id]
    util_memoize_flush [list im_filestorage_project_path_helper $project_id]

    db_dml update_projects_defaults "update im_projects 
					  set project_lead_id = :project_lead_id, 
					  project_nr = :next_quote_nr,
					  project_name = :next_quote_nr,
					   project_path = :next_quote_nr,
					  quote_nr = :next_quote_nr,
					  quote_date = :start_date
					 where project_id = :object_id"
					 
    # re-check if directories are missing and create if so
    set fs_dir_create_proc [parameter::get -package_id [im_package_translation_id] -parameter "FilestorageCreateFolderProc"]
    $fs_dir_create_proc $project_id
    

    
}




ad_proc -public -callback im_project_after_quote2order -impl fud_rename_project {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    change the project name based on the interco of the company
} {

    db_1row customer_and_project_info "select certified_translation, now() as effective_date, 
									p.project_type_id, 
									project_nr as original_project_nr, 
									c.company_id, 
									p.start_date, 
									p.end_date, 
									p.project_status_id,
									p.interco_company_id, 
									coalesce(p.processing_time,0) as processing_time, 
									to_char(now(),'YYYY-MM-DD') as new_start_date, 
									to_char(end_date, 'YYYY-MM-DD') as new_end_date, 
									company_status_id,
									project_cost_center_id as cost_center_id 
								from im_projects p, im_companies c 
								where p.company_id = c.company_id and p.project_id = :project_id"

    
    if {$status_id eq "" && project_status_id ne ""} {set status_id $project_status_id} 
    if {$status_id eq "71"} {
    
    set old_project_path [im_filestorage_project_path $project_id]

    # Define the new project_number
    switch $interco_company_id {
	28022 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like '9%'"]
	    incr project_nr
	    set cost_status_id 11000162
	}
	552735 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like '3%'"]
	    incr project_nr
	    set cost_status_id 11000162
	}
	279215 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like 'Z%'"]
	    set project_nr [string trim $project_nr "Z"]
	    incr project_nr
	    set project_nr "Z$project_nr"
	    set cost_status_id 3804
	}
	default {
	    # Leave the project_nr
	    set project_nr $original_project_nr
	    set cost_status_id 3804
	}
    }

    db_dml update_project_nr "update im_projects set project_nr = :project_nr, project_path = :project_nr, project_name = :project_nr where project_id = :project_id"
    
    # Create a new invoice.
    # If new customer create a new invoice with the effective_date of now,
    # else project_end_date

 set invcount [db_string invct " select count(cost_id) invs 
                                    from im_costs 
                                    where customer_id = :company_id 
                                          and cost_type_id = '3700'
                                          and cost_status_id not in ([etm_coststatus_collectionagency],
                                                                     [etm_coststatus_collectionagencyprep],
                                                                     [etm_coststatus_reminder1],
                                                                     [etm_coststatus_writeoff],
                                                                     [etm_coststatus_writtenoff])
                                          
                                          " -default "0"]

    if {$invcount > 0 } {set effective_date $new_end_date } 
     else {set effective_date $new_start_date}
    
    
    
#  ---  companies are set to active before this so all companies get the end date as invoice date
#     if {[im_company_status_active] != $company_status_id} {
# 	set effective_date $new_start_date
#     } else {
# 	set effective_date $new_end_date
#     }




    
    set quote_id [db_string quote "select cost_id from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote] order by cost_id desc limit 1" -default ""]
    set quote_nr [db_string quote "select cost_name from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote] order by cost_id desc limit 1" -default ""]

    if {"" != $quote_id} {
        # Copy the invoice
        set invoice_id [im_invoice_copy_new -source_invoice_ids $quote_id -target_cost_type_id [im_cost_type_invoice]]
        set invoice_nr [fud_next_invoice_nr -cost_type_id [im_cost_type_invoice] -cost_center_id $cost_center_id -company_id $company_id -cost_id $invoice_id]

        db_dml update_effective_date "update im_costs set cost_name = :invoice_nr, effective_date = :effective_date, delivery_date = :new_end_date, cost_status_id= :cost_status_id where cost_id = :invoice_id"
        db_dml update_invoice_nr "update im_invoices set invoice_nr = :invoice_nr where invoice_id = :invoice_id"
    }

    # Change the template of the Offer and change the name.

    db_dml update_quote "update im_costs set effective_date = now(), delivery_date = :new_end_date where cost_id = :quote_id"


    # Rename the file path
    if {[file exists $old_project_path]} {
    	set new_project_path [im_filestorage_project_path_helper $project_id]
    	if {$new_project_path ne $old_project_path} {
            file rename $old_project_path $new_project_path
        }
    }

    util_memoize_flush [list im_filestorage_base_path_helper project $project_id]
    util_memoize_flush [list im_filestorage_project_path_helper $project_id]

     #add folder for translation memories and termbases
	set tbtmx_dir 9_tbtmx
	set project_dir [im_filestorage_project_path $project_id]
	set dir_tbtmx "$project_dir/$tbtmx_dir"
	
	if {[catch {
	    if {![file exists $dir_tbtmx]} {
		ns_log Debug "exec /bin/mkdir -p $dir_tbtmx"
		exec /bin/mkdir -p $dir_tbtmx
		ns_log Debug "exec /bin/chmod ug+w $dir_tbtmx"
		exec /bin/chmod ug+rw $dir_tbtmx
	    }
	} err_msg]} { return $err_msg }
    
    

    #add folder "Zusicherung" if order includes a certified translation
    if { [lsearch [im_sub_categories 2505] $project_type_id] >=0 } {
	set dir_name Zusicherung
	set project_dir [im_filestorage_project_path $project_id]
	set zusicherung_dir "$project_dir/$dir_name"
	
	if {[catch {
	    if {![file exists $zusicherung_dir]} {
		ns_log Debug "exec /bin/mkdir -p $zusicherung_dir"
		exec /bin/mkdir -p $zusicherung_dir
		ns_log Debug "exec /bin/chmod ug+w $zusicherung_dir"
		exec /bin/chmod ug+rw $zusicherung_dir
	    }
	} err_msg]} { return $err_msg }
    }

  }
}


# ---------------------------------------------------------------
# INVOICE Callbacks
# ---------------------------------------------------------------

ad_proc -public -callback im_invoice_after_create -impl aa_fud_store_invoice {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Store the invoice after creation
} {
    # Check if this is an invoice in active status
    if {$status_id ne "3812" } { 
      if {$type_id eq "3700" || $type_id eq "3725"} {
         # Create the invoice
         fud_create_invoice_pdf -invoice_id $object_id
         ns_log Notice "fud_create_invoice_pdf $object_id"
         } 
    }
   
}

ad_proc -public -callback im_invoice_after_update -impl aa_fud_store_invoice {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Store the invoice after change
} {
    # Check if this is an invoice in active status
    if {$status_id ne "3812" } { 
      if {$type_id eq "3700" || $type_id eq "3725"} {
         # Create the invoice
         fud_create_invoice_pdf -invoice_id $object_id
         } 
    }
   
}



ad_proc -public -callback im_invoice_after_create -impl fud_provider_po_renamer {
    -object_id:required
    {-status_id ""}
    {-type_id ""}
} {
    Change the name of the invoice_nr for Provider POs
} {
    if {$type_id eq 3706} {
		set invoice_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :object_id" -default ""]
		db_1row provider_and_project_info  "
			select project_nr, end_date - interval '1 day' as delivery_date,  im_name_from_id(provider_id) as company_name 
			from im_projects p, im_costs c
			where cost_id = :object_id and p.project_id = c.project_id" 
	     
		regsub -all "\u00e4" $company_name "ae" company_name
		regsub -all "\u00C4" $company_name "Ae" company_name
		regsub -all "\u00FC" $company_name "ue" company_name 	
		regsub -all "\u00DC" $company_name "Ue" company_name 	 	
		regsub -all "\u00F6" $company_name "oe" company_name 
		regsub -all "\u00D6" $company_name "Oe" company_name 
		regsub -all "\u00ED" $company_name "i" company_name 	
		regsub -all "\u00CD" $company_name "I" company_name 
		regsub -all "\u00FA" $company_name "u" company_name 	
		regsub -all "\u00DA" $company_name "U" company_name 	
		regsub -all "\u00E1" $company_name "a" company_name 	
		regsub -all "\u00C1" $company_name "A" company_name 
	     
	    	regsub -all {[^a-zA-Z0-9]} $company_name "" company_name
		set extension [string range $company_name 0 1]
				
		set template "11000259"
		set vat_type "11000424"
		set cost_center_id "85004"
			
		set invoice_nr "P${project_nr}_$extension"
		#check if already exists and add an index if so
		
		set invoice_exists_p [db_string inv_exists "select count(*) from im_invoices where invoice_nr = :invoice_nr" -default 0]
		set i 2
		while {$invoice_exists_p != 0} {
			
               set invoice_nr [string trimright $invoice_nr _$extension]
		     set invoice_nr "${invoice_nr}-${i}_$extension"
			set i [expr {$i + 1}]
			set invoice_exists_p [db_string inv_exists "select count(*) from im_invoices where invoice_nr = :invoice_nr" -default 0]
		}
		db_dml update_cost_nr "update im_costs set cost_name = :invoice_nr, cost_center_id = 85004, 
							delivery_date = :delivery_date, vat_type_id = :vat_type, template_id = :template 
						   where cost_id = :object_id"
	    db_dml update_invoice_nr "update im_invoices set invoice_nr = :invoice_nr where invoice_id = :object_id"
    }
}











ad_proc -public -callback im_invoice_after_create -impl fud_merge_provider_bill {
    -object_id:required
    {-status_id ""}
    {-type_id ""}
} {
    Change the name of the invoice_nr for Provider bills
} {
    if {$status_id eq 3804 && $type_id eq 3704} {
        set invoice_id $object_id
	db_1row invoice_infos "select invoice_nr , payment_method_id,provider_id, im_name_from_id(c.provider_id) as company_name,bank_iban
			      from im_invoices i,im_costs c, im_companies comp 
			      where c.cost_id = :object_id 
			      and i.invoice_id = c.cost_id
			      and comp.company_id = c.provider_id"
										  
	set currency [db_string currency "select currency from im_invoice_items where invoice_id = :object_id limit 1" -default "EUR"]
	
	if {$invoice_nr ne "" && $company_name ne "" } {
	   
	# Add any regsubs you want to do with the invoice_nr here
	    regsub -all "\u00e4" $invoice_nr "ae" invoice_nr
	    regsub -all "\u00C4" $invoice_nr "Ae" invoice_nr
	    regsub -all "\u00FC" $invoice_nr "ue" invoice_nr 	
	    regsub -all "\u00DC" $invoice_nr "Ue" invoice_nr 	 	
	    regsub -all "\u00F6" $invoice_nr "oe" invoice_nr 
	    regsub -all "\u00D6" $invoice_nr "Oe" invoice_nr 
	    regsub -all "\u00ED" $invoice_nr "i" invoice_nr 	
	    regsub -all "\u00CD" $invoice_nr "I" invoice_nr 
	    regsub -all "\u00FA" $invoice_nr "u" invoice_nr 	
	    regsub -all "\u00DA" $invoice_nr "U" invoice_nr 	
	    regsub -all "\u00E1" $invoice_nr "a" invoice_nr 	
	    regsub -all "\u00C1" $invoice_nr "A" invoice_nr 
	    
	    regsub -all "\u00e4" $company_name "ae" company_name
	    regsub -all "\u00C4" $company_name "Ae" company_name
	    regsub -all "\u00FC" $company_name "ue" company_name 	
	    regsub -all "\u00DC" $company_name "Ue" company_name 	 	
	    regsub -all "\u00F6" $company_name "oe" company_name 
	    regsub -all "\u00D6" $company_name "Oe" company_name 
	    regsub -all "\u00ED" $company_name "i" company_name 	
	    regsub -all "\u00CD" $company_name "I" company_name 
	    regsub -all "\u00FA" $company_name "u" company_name 	
	    regsub -all "\u00DA" $company_name "U" company_name 	
	    regsub -all "\u00E1" $company_name "a" company_name 	
	    regsub -all "\u00C1" $company_name "A" company_name 
	}
		
	    #check for already existing extension and trim it
	    set underl_lastpos [expr {[string length $invoice_nr] - [string last _ $invoice_nr]}]

	    if {$underl_lastpos == 4 || $underl_lastpos == 3} {
		    set extension [string range $invoice_nr [string last _ $invoice_nr] [string length $invoice_nr] ]
		    if { [regexp {^_([a-zA-Z]{2,3})$} $extension] } {
			    set invoice_nr [string trimright $invoice_nr $extension]
		    }
	    }
	    #trim B from name and clean additional characters not needed
	    set invoice_nr [string trimleft $invoice_nr "B"]
	    regsub -all {(0{3,})} $invoice_nr "0" invoice_nr
	    regsub -all {[R|r]echnung} $invoice_nr "" invoice_nr
	    regsub -all {[I|i]nvoice} $invoice_nr "" invoice_nr
	    regsub -all -nocase {fud} $invoice_nr "" invoice_nr
	    regsub -all -nocase {fued} $invoice_nr "" invoice_nr	
	    regsub -all -nocase {FachuebersetzungsdienstGmbH} $invoice_nr "" invoice_nr	
	    regsub -all -nocase {Fachuebersetzungsdienst} $invoice_nr "" invoice_nr
    	    regsub -all -nocase {FACHUEBERSETZUNGSDIENST} $invoice_nr "" invoice_nr
	    regsub -all {[^a-zA-Z0-9]} $invoice_nr "" invoice_nr

	    
	    # format comp_extension
	    regsub -all {[^a-zA-Z0-9]} $company_name "" company_name
	    set comp_extension [string range $company_name 0 1]
	    
	    #if bill number is too simple  just add the invoice year to get a unique bill number
	    set inv_len [string length $invoice_nr]

	    if {$inv_len < 4} {
		    set inv_year [db_string inv_year "select to_char(effective_date, 'YYYY') as inv_year from im_costs where cost_id = :object_id" -default ""]
		    set invoice_nr "B$inv_year${invoice_nr}_${comp_extension}"	
	    } else { set invoice_nr "B${invoice_nr}_${comp_extension}" }
	    
	    while {[etm_invoice_exists_p -invoice_nr $invoice_nr]} {
	      set invoice_nr [string trimleft $invoice_nr "B"]
	      set invoice_nr "B0${invoice_nr}"
	    }
	    
	    #check payment method 
	    # 11000204 sepa
	    # 11000205 non-sepa
	    set iban_code ""
	    if {$bank_iban ne ""} {
	      regsub -all " " $bank_iban "" bank_iban
	      set iban_code  [string range $bank_iban 0 1]
	    }

	    switch $payment_method_id {
	      11000204 { if {$currency ne "EUR"} {set payment_method_id "11000205"}
			 if {$iban_code ne ""} { 
			    if {![etm_iban_is_sepa_p -country_code $iban_code]} {set payment_method_id "11000205" }
			    }
		      }
	      11000205 { if {$iban_code ne ""} { 
			    if {[etm_iban_is_sepa_p -country_code $iban_code] && $currency eq "EUR"} {set payment_method_id "11000204" }
			}
	      
		      }	      
	    
	    
	    }
	    
	    db_dml update_cost_nr "update im_costs set  cost_name = :invoice_nr,cost_center_id =  85004  where cost_id = :object_id"
	    db_dml update_invoice_nr "update im_invoices set invoice_nr = :invoice_nr, payment_method_id = :payment_method_id where invoice_id = :object_id"
      }
}




ad_proc -public -callback im_invoice_after_update -impl fud_merge_provider_bill_upd {
    -object_id:required
    {-status_id ""}
    {-type_id ""}
} {
    Change the name of the invoice_nr for Provider bills
} {
    if {$status_id eq 3804 && $type_id eq 3704} {
        set invoice_id $object_id
	#set invoice_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :object_id" -default ""]
	#set company_name [db_string company_name "select im_name_from_id(provider_id) from im_costs where cost_id = :object_id" -default ""]

	db_1row billinfo "select to_char(effective_date, 'YYYYMMDD') as effective_date, 
							 im_name_from_id(provider_id) as company_name, 
							 cost_name as invoice_nr
					  from im_costs 
					  where cost_id = :object_id"



	if {$invoice_nr ne "" && $company_name ne "" } {
	   
		    # Add any regsubs you want to do with the invoice_nr here
	    regsub -all "\u00e4" $invoice_nr "ae" invoice_nr
	    regsub -all "\u00C4" $invoice_nr "Ae" invoice_nr
	    regsub -all "\u00FC" $invoice_nr "ue" invoice_nr 	
	    regsub -all "\u00DC" $invoice_nr "Ue" invoice_nr 	 	
	    regsub -all "\u00F6" $invoice_nr "oe" invoice_nr 
	    regsub -all "\u00D6" $invoice_nr "Oe" invoice_nr 
	    regsub -all "\u00ED" $invoice_nr "i" invoice_nr 	
	    regsub -all "\u00CD" $invoice_nr "I" invoice_nr 
	    regsub -all "\u00FA" $invoice_nr "u" invoice_nr 	
	    regsub -all "\u00DA" $invoice_nr "U" invoice_nr 	
	    regsub -all "\u00E1" $invoice_nr "a" invoice_nr 	
	    regsub -all "\u00C1" $invoice_nr "A" invoice_nr 
	    
	    regsub -all "\u00e4" $company_name "ae" company_name
	    regsub -all "\u00C4" $company_name "Ae" company_name
	    regsub -all "\u00FC" $company_name "ue" company_name 	
	    regsub -all "\u00DC" $company_name "Ue" company_name 	 	
	    regsub -all "\u00F6" $company_name "oe" company_name 
	    regsub -all "\u00D6" $company_name "Oe" company_name 
	    regsub -all "\u00ED" $company_name "i" company_name 	
	    regsub -all "\u00CD" $company_name "I" company_name 
	    regsub -all "\u00FA" $company_name "u" company_name 	
	    regsub -all "\u00DA" $company_name "U" company_name 	
	    regsub -all "\u00E1" $company_name "a" company_name 	
	    regsub -all "\u00C1" $company_name "A" company_name 
	}
		
	  #check for already existing extension and trim it
	  set underl_lastpos [expr {[string length $invoice_nr] - [string last _ $invoice_nr]}]

	  if {$underl_lastpos == 4 || $underl_lastpos == 3} {
		  set extension [string range $invoice_nr [string last _ $invoice_nr] [string length $invoice_nr] ]
		  if { [regexp {^_([a-zA-Z]{2,3})$} $extension] } {
			  set invoice_nr [string trimright $invoice_nr $extension]
		  }
	  }
	  #trim B from name and clean additional characters not needed
	  set invoice_nr [string trimleft $invoice_nr "B"]
	  regsub -all {(0{3,})} $invoice_nr "0" invoice_nr
	  regsub -all -nocase {rechnung} $invoice_nr "" invoice_nr
	  regsub -all -nocase {invoice} $invoice_nr "" invoice_nr
	  regsub -all -nocase {fud} $invoice_nr "" invoice_nr
	  regsub -all -nocase {fued} $invoice_nr "" invoice_nr
	  regsub -all -nocase {FachuebersetzungsdienstGmbH} $invoice_nr "" invoice_nr	
	  regsub -all -nocase {Fachuebersetzungsdienst} $invoice_nr "" invoice_nr
	  regsub -all -nocase {FACHUEBERSETZUNGSDIENST} $invoice_nr "" invoice_nr
	  regsub -all {[^a-zA-Z0-9]} $invoice_nr "" invoice_nr

	  
	  # format comp_extension
	  regsub -all {[^a-zA-Z0-9]} $company_name "" company_name
	  set comp_extension [string range $company_name 0 1]
	  
	  #if bill number is too simple  just add the invoice year to get a unique bill number
	  set inv_len [string length $invoice_nr]

	  if {$inv_len < 4} {
		  set inv_year [db_string inv_year "select to_char(effective_date, 'YYYY') as inv_year from im_costs where cost_id = :object_id" -default ""]
		  set invoice_nr "B$inv_year${invoice_nr}_${comp_extension}"	
	  } else { set invoice_nr "B${invoice_nr}_${comp_extension}" }
	  
	  set invoice_exists_p [db_string invoice_exists "select 1 from im_invoices where invoice_nr = :invoice_nr and invoice_id != :object_id" -default "0"]
	  
	  while {$invoice_exists_p} {
	    set invoice_nr [string trimleft $invoice_nr "B"]
	    set invoice_nr "B0${invoice_nr}"
	  }
	  # all bills cost center is FUD
	  set cost_center_id 85004
	  db_dml update_cost_nr "update im_costs set  cost_name = :invoice_nr,cost_center_id =  :cost_center_id  where cost_id = :object_id"
	  db_dml update_invoice_nr "update im_invoices set invoice_nr = :invoice_nr where invoice_id = :object_id"

### update uploaded file since renaming only works for new bills


	  set company_shortname [string range $company_name 0 19]

	  #set billpdf "[im_cost_center_name $cost_center_id]_fl_${company_name}_${invoice_nr}_${effective_date}.pdf"
	  set billpdf "[im_cost_center_name $cost_center_id]_fl_${company_shortname}_${invoice_nr}_${effective_date}.pdf"
	  ns_log Notice "Callback fud_merge_provider_bill_upd  $billpdf"


	  set costpath [im_filestorage_cost_path $object_id]
	  set find_cmd [im_filestorage_find_cmd]
	  #set file_list [exec $find_cmd $costpath -noleaf -type f | wc -l]
	  set file [glob -nocomplain -directory ${costpath} -- *.pdf ]
	  if {[llength $file] == 1} {
	  exec /bin/mv "$file" $costpath/$billpdf
	  }
    }
}




ad_proc -public -callback intranet-invoices::merge_filename_change -impl fud_rename_filename {
	-invoice_id:required
	-file_extension:required
} {
	Change the filename
	
	CostCenter_fl_ProviderName_BillNr_EffectiveDateYYYYMMDD.pdf 
} { 
	if {$file_extension eq "pdf"} {
		db_1row invoice_info "select company_path, company_name,cost_center_id, 
							to_char(effective_date,'YYYYMMDD') as effective_date, 
							invoice_nr 
						from im_invoices i, im_costs c, im_companies co 
						where co.company_id = c.provider_id 
							and i.invoice_id = c.cost_id 
							and c.cost_id = :invoice_id"
		
		
		regsub -all "\u00e4" $company_name "ae" company_name
		regsub -all "\u00C4" $company_name "Ae" company_name
		regsub -all "\u00FC" $company_name "ue" company_name 	
		regsub -all "\u00DC" $company_name "Ue" company_name 	 	
		regsub -all "\u00F6" $company_name "oe" company_name 
		regsub -all "\u00D6" $company_name "Oe" company_name 
		regsub -all "\u00ED" $company_name "i" company_name 	
		regsub -all "\u00CD" $company_name "I" company_name 
		regsub -all "\u00FA" $company_name "u" company_name 	
		regsub -all "\u00DA" $company_name "U" company_name 	
		regsub -all "\u00E1" $company_name "a" company_name 	
		regsub -all "\u00C1" $company_name "A" company_name 
		
		
		
		regsub -all {[^a-zA-Z0-9]} $company_name "" company_name
		set company_name [string range $company_name 0 19]
		regsub -all {0{3,}} $invoice_nr "0" invoice_nr
		
		upvar client_filename client_filename			
		set client_filename "[im_cost_center_name $cost_center_id]_fl_${company_name}_${invoice_nr}_${effective_date}.pdf"
	}
}






ad_proc -public -callback im_invoices_def_comp_template -impl cust_cost_center_from_user {
    -cost_type_id
    -company_id
    {-cost_center_id ""}
} {
    Return the default invoice template id for currently logged user
} {
  
  if {$cost_type_id eq 3702} {
   if {$company_id ne [im_company_internal]} {
    upvar template_id template_id
    set user_id [ad_conn user_id]
    if { $cost_center_id ne 0 && "" != $cost_center_id } {
      set cost_center_id [fud_cost_center_from_interco -interco_company_id [etm_agency_by_employee_dept]]
    }
    
      set template_id [im_invoices_default_company_template -no_callback $cost_type_id $company_id $cost_center_id]
    } 
   }
     
}



ad_proc -public -callback im_invoice_after_create -impl zzz_etm_projectquote_default {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set etm findocs default overwriting im_invoices_default_company_template ...needs some rewriting 
} {

   ns_log Notice "Callback implementation zzz_etm_projectquote_default  for object_id $object_id status $status_id cost_type_id $type_id"


#set template_id [im_etm_quotetemplate_default]
    if {$type_id eq "" } {
	set type_id [db_string type_id "select cost_type_id as type_id from im_costs where cost_id = :object_id" -default ""]
    }
    
    if {$type_id eq 3702} {
	# set quote default
	set project_id [db_string project_id "select project_id from im_costs where cost_id = :object_id" -default ""]
	if {$project_id ne ""} {set template_id [etm_quote_project_default -project_id $project_id]}
    
	if {$template_id ne ""} { db_dml qttempl "update im_costs set template_id = :template_id where cost_id = :object_id" }
	
	etm_quote_items -invoice_id $object_id
	
    }
    
    
    
    
}

ad_proc -public -callback im_invoice_after_create -impl fud_payment_move {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Move payment(s) for correction invoices
} {
   ns_log Notice "Callback implementation fud_payment_move  for object_id $object_id status $status_id cost_type_id $type_id"

    set invoice_id $object_id
    
    if {$type_id eq [im_cost_type_correction_invoice]  || $type_id eq [im_cost_type_cancellation_invoice]} {
	set linked_invoice_ids [relation::get_objects -object_id_two $invoice_id -rel_type "im_invoice_invoice_rel"]
	set linked_invoice_ids [concat [relation::get_objects -object_id_one $invoice_id -rel_type "im_invoice_invoice_rel"] $linked_invoice_ids]

	    set linked_list_sql "
select
        invoice_id as linked_invoice_id,
        invoice_nr as linked_invoice_nr,
        effective_date as linked_effective_date,
        cost_type_id as linked_cost_type_id,
        cost_status_id as linked_cost_status_id
from
        im_invoices, im_costs
where
        invoice_id in ([template::util::tcl_to_sql_list $linked_invoice_ids])
        and cost_id = invoice_id
"

   if {$linked_list_sql ne ""} { 
     set linked_ctr 0
	db_foreach linked_list $linked_list_sql {
	    if {$linked_cost_type_id eq [im_cost_type_invoice] || $linked_cost_type_id eq [im_cost_type_correction_invoice] || $linked_cost_type_id eq [im_cost_type_cancellation_invoice]} {
		# Update the payments, move them to the correction invoice
		db_dml update_payments "update im_payments set cost_id = :invoice_id where cost_id = :linked_invoice_id"

		# Update paid_amount value for correction invoice and clear in source invoice
		set transfer_paid_amount [ db_string pa_old "select paid_amount from im_costs where cost_id = :linked_invoice_id" -default ""]
		set transfer_paid_currency [ db_string pc_old "select paid_currency from im_costs where cost_id = :linked_invoice_id" -default ""]

		if {$transfer_paid_amount ne ""} {
		    db_dml transfer_cached_paymentamount "update im_costs set paid_amount = :transfer_paid_amount, 
												paid_currency = :transfer_paid_currency 
										  where cost_id = :object_id"
		    
		    db_dml clear_old_cached_paymentamount "update im_costs set paid_amount = null , paid_currency = null where cost_id =  :linked_invoice_id"
		}
	    }
	  }
    }   
   }
}

ad_proc -public -callback im_invoice_after_create -impl zzz_etm_cancellation_invoice_adapt {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	change some etm-stuff for cancellation invoices 
} {

   ns_log Notice "Callback implementation zzz_etm_cancellation_invoice_adapt for object_id $object_id status $status_id cost_type_id $type_id"


	if {$type_id eq "3740"} {
		# collect infos for cancellation invoice and original invoice
		db_0or1row invoice_infos "select cost_name, project_id,(amount * -1) as amount, im_name_from_id(template_id) as template
		from im_costs where cost_id = :object_id
		"
		db_0or1row orig_cost_id "select object_id_one as orig_cost_id,im_name_from_id(object_id_one) as orig_cost_name
			from  acs_rels 
			where rel_type = 'im_invoice_invoice_rel'
			and object_id_two = :object_id
			order by rel_id desc
			limit 1
		"
		set invoice_currency [db_string cur "select distinct on (invoice_id)
				currency as invoice_currency
			from	im_invoice_items 
			where	invoice_id = :object_id
			limit 1" -default "EUR"]
	
	     
		
		# change status and cost type 
		db_dml upd "update im_costs 
					set cost_status_id = [im_cost_status_cancelled],
					cost_type_id = [im_cost_type_correction_invoice] 
					where cost_id = :object_id"
                
         #change status of original invoice from cancelled to im_cost_status_replaced
          db_dml origstatus_replaced "update im_costs 
                                        set cost_status_id = [im_cost_status_replaced]
                                      where cost_id = :orig_cost_id" 

          #add orig_cost_id and orig_cost_name
		db_dml upd "update im_invoices 
			set 	changed_invoice_id = :orig_cost_id,
				changed_invoice_nr = :orig_cost_name 
			where invoice_id = :object_id"                                      
                                                
		#add new invoice item for cancellation
		set sort_order [db_string sort_order "select max(sort_order) +  1 from im_invoice_items where invoice_id = :object_id" -default ""]

		if {[regexp {(.*)\.de.*\.([a-zA-Z]{3,4})} $template]} {
                    set cancellation "Storno"
                } else {set cancellation "Cancellation"}


                set item_id [db_nextval "im_invoice_items_seq"]

                set insert_invoice_items_sql "
                INSERT INTO im_invoice_items (
                    item_id, item_name,
                    project_id, invoice_id,
                    item_units, item_uom_id,
                    price_per_unit, currency,
                    sort_order,
                    item_source_invoice_id
                ) VALUES (
                    :item_id, :cancellation,
                    :project_id, :object_id,
                    1, 322,
                    :amount, :invoice_currency,
                    :sort_order,
                    :orig_cost_id
                )" 


                db_dml insert_invoice_items $insert_invoice_items_sql
                
                
                db_1row ii "
				select round(sum(item_units * price_per_unit),3) as items_sum
				from im_invoice_items
				where invoice_id = :object_id
			"
               
               # change cost amount
			db_dml upd_cost_amount "update im_costs 
					set amount = :items_sum 
					where cost_id = :object_id
			"
                
			 
        } 

}


ad_proc -public -callback im_invoice_after_create -impl etm_quote_adapt {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	for all existing quote items with units 0 set units to 1 and uom to unit
} {
   ns_log Notice "Callback implementation etm_quote_adapt for object_id $object_id status $status_id cost_type_id $type_id"

	
	if {$type_id eq "" } { set type_id [db_string type "select cost_type_id as type_id from im_costs where cost_id = :object_id" -default 0] } 

	if {$type_id eq 3702} {

		set invoice_item_ids [db_list iii "
		select item_id 
		from im_invoice_items
		where invoice_id = :object_id 
			and item_units = 0

		"]

		if {$invoice_item_ids ne ""} {
		set invoice_item_ids [template::util::tcl_to_sql_list $invoice_item_ids]
		db_dml upd_units  "update im_invoice_items set item_units = 1, item_uom_id = [im_uom_unit] where item_id in ($invoice_item_ids)"
		
		} 
		
		
		#set vat
		 set vat_type_id [etm_vatypebyinvoicedept_default -invoice_id $object_id]
		 if {$vat_type_id ne ""} { db_dml qtvattype "update im_costs set  vat_type_id = :vat_type_id where cost_id = :object_id" }

		 set vat [db_string vat "select aux_num1 from im_categories where category_id = :vat_type_id" -default ""]
		 if {$vat ne ""} { db_dml qtvat "update im_costs set vat = :vat where cost_id = :object_id" }
		 ns_log Notice "Callback etm_quote_adapt invoice_id $object_id vat_id to $vat_type_id and vat to $vat"
	}
}


ad_proc -public -callback im_invoice_after_create -impl zzz_etm_costvat_default {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    set etm findocs default vat overwriting im_invoices_default_company_template 
} {
   ns_log Notice "Callback implementation zzz_etm_costvat_default for object_id $object_id status $status_id cost_type_id $type_id"


#set template_id [im_etm_quotetemplate_default]
    if {$type_id eq "" } {
	set type_id [db_string type_id "select cost_type_id as type_id from im_costs where cost_id = :object_id" -default ""]
    }
    
    switch $type_id {	
    "3700" - "3725" - "3702" {
        set vat_type_id [etm_vatypebyinvoicedept_default -invoice_id $object_id]
        if {$vat_type_id ne ""} { db_dml qtvattype "update im_costs set  vat_type_id = :vat_type_id where cost_id = :object_id" }

        set vat [db_string vat "select aux_num1 from im_categories where category_id = :vat_type_id" -default ""]
	if {$vat ne ""} { db_dml qtvat "update im_costs set vat = :vat where cost_id = :object_id" }
       ns_log Notice "Callback zzz_etm_costvat_default invoice_id $object_id vat_id to $vat_type_id and vat to $vat"

    }
   } 
    
}

ad_proc -public -callback im_invoice_after_create -impl zzz_etm_cost_save_vatamount {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    If a cost is created automatically save the vat_amount
} {
   set vat [db_string category "select aux_int1 from im_costs c, im_categories ca where ca.category_id = c.vat_type_id and c.cost_id = :object_id" -default ""]
   set amount [db_string invamount "select sum(item_units * price_per_unit) from im_invoice_items where invoice_id = :object_id" -default "0"]
    if {$amount ne "" && $vat ne ""} {
      set vat_amount [db_string invamount "select vat_amount from im_costs where cost_id = :object_id" -default "0"]
      set vat_amount [expr ($vat+0) * $amount / 100]
      db_dml update_cost "update im_costs set vat_amount = :vat_amount where cost_id = :object_id"
      }
}



ad_proc -public -callback im_invoice_after_update -impl zzz_etm_cost_save_vatamount_upd {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    If a cost is updated automatically save the vat_amount
} {
   set vat [db_string category "select aux_int1 from im_costs c, im_categories ca where ca.category_id = c.vat_type_id and c.cost_id = :object_id" -default ""]
   set amount [db_string invamount "select sum(item_units * price_per_unit) from im_invoice_items where invoice_id = :object_id" -default "0"]
    if {$amount ne "" && $vat ne ""} {
      set vat_amount [db_string invamount "select vat_amount from im_costs where cost_id = :object_id" -default "0"]
      set vat_amount [expr ($vat+0) * $amount / 100]
      db_dml update_cost "update im_costs set vat_amount = :vat_amount where cost_id = :object_id"
      }
}