

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    FUD custom procs 
    
    @author <yourname> (<your email>)
    @creation-date 2012-03-11
    @cvs-id $Id$
}

ad_proc -public fud_status_id {
    -project_status_id
} {
    if {"" eq $project_status_id} {set project_status_id 0}
    return $project_status_id
}

ad_proc -public fud_int2_id {
    -category_id
} {
    Return the profile_id to a role
} {
    return
}

ad_proc -public fud_member_list {
    -project_id
    -object_role_id
} {
    Return the Names of the PMS
} {
    return [util_memoize [list fud_member_list_helper -project_id $project_id -object_role_id $object_role_id] 3600]
}

ad_proc -public fud_member_list_helper {
    -project_id
    -object_role_id
} {
    Return the Names of the PMS
} {
    set pm_list [list]
    set sql "
	select im_name_from_id(object_id_two) as pm_name,object_id_two as pm_id
	from
	       acs_rels r, im_biz_object_members bo
	where
               r.object_id_one = :project_id and
	       r.rel_id = bo.rel_id and
               bo.object_role_id = :object_role_id
    "
    db_foreach pm $sql {
	lappend pm_list "<A HREF=/intranet/users/view?user_id=$pm_id>$pm_name</A>"
    }

    
    if {0 == [llength $pm_list]} {

	set user_id [ad_conn user_id]
	set profile_id [util_memoize [list db_string profile_id "select aux_int2 from im_categories where category_id = $object_role_id"]]

	# Check if the user is in the correct group
	if {[im_profile::member_p -profile_id $profile_id -user_id $user_id]} {
	    set assign_url [export_vars -base "/intranet/member-add-2" -url {{user_id_from_search "$user_id"} {object_id $project_id} {role_id $object_role_id} {return_url "[util_get_current_url]"}}]
	    return "<a href=\"$assign_url\">Assign me</a>"
	} else {
	    return "Assign me"
	}
    } else {
	return [join $pm_list "<br />"]
    }
}


#procs update
ad_proc -public fud_update_old_projects {
    
} {
    change quote project status after 30 days without answer
} {
    db_dml update_kv_status {
	UPDATE im_projects  SET project_status_id = 11000007
	WHERE project_status_id = 71
	AND start_date < current_date - 30
	AND project_nr LIKE '2%'
    }
}



ad_proc -public etm_update_wrong_order_status {
    
} {
    change invoice status to outstanding if set to created
} {
    db_dml update_kv_status {
      update im_costs set cost_status_id = 3804
      where cost_type_id = 3700
      and cost_status_id = 3802
    }
}



#find dept by user_id
ad_proc -public etm_dept_from_employee_id {
    { -user_id "" }
} { 
    dept by user_id
} { 
        if { $user_id eq ""} {set user_id [ad_conn user_id]}

    set dept [db_string dept "select im_name_from_id(department_id)
    from im_employees 
    where employee_id = :user_id" -default ""]
    return $dept
}

#agency by employee mail-address
ad_proc -public im_etm_agency_by_cm_default {} {
	set agency_default ""
	set user_id [ad_conn user_id]
    if {[im_user_is_employee_p $user_id]} {
	   switch -glob [im_email_from_user_id $user_id] {
	       *@fachuebersetzungsdienst.com {set agency_default 28022}
	       *@panoramalanguages.com {set agency_default 552735}
	       *@fachuebersetzungsagentur.com {set agency_default 279215}
	       *@fachuebersetzungsservice.com {set agency_default 279215}
	       *@uebersetzungsagentur24.com {set agency_default 279215}
	       *@dialecta.com {set agency_default 279215}
	   }
	   return $agency_default
	}
}

#agency by employee dept
ad_proc -public etm_agency_by_employee_dept {
    { -user_id "" }
} { interco_comp by employees dept 
} {
    set agency_default ""
    if {$user_id eq ""} {set user_id [ad_conn user_id]}
    if {[im_user_is_employee_p $user_id]} {
       switch  [etm_dept_from_employee_id -user_id $user_id] {
           FUD {set agency_default 28022}
           PAN {set agency_default 552735}
           ZIS {set agency_default 279215}
           DIA {set agency_default 279215}
           default {set agency_default ""}
       }
       return $agency_default
    }
}





# agency by project agent
ad_proc -public etm_agent_from_project_id {
    { -project_id "" }
} { interco_comp by project id 
} {
	set project_agent [db_string project_agent "select im_name_from_id(interco_company_id) as project_agent 
						    from im_projects 
						    where project_id = :project_id " -default "" ]
    	return $project_agent
}

 
 
 
# agency id by project agent
ad_proc -public etm_agent_id_from_project_id {
    { -project_id "" }
} { interco_comp id by project id 
} {
 	set project_agent_id [db_string project_dept "select interco_company_id as project_agent_id 
									 from im_projects 
									 where project_id = :project_id " -default "" ]
	return $project_agent_id
} 
 
 
# dept by project dept
ad_proc -public etm_dept_from_project_id {
    { -project_id "" }
} { cost center (dept) by project id
} {
	set project_dept [db_string project_dept "select im_name_from_id(project_cost_center_id) as project_dept 
									 from im_projects 
									 where project_id = :project_id " -default "" ]
	return $project_dept
}
 
 
#dept id by project 
ad_proc -public etm_dept_id_from_project_id {
    { -project_id "" }
} { cost_center_id by project id
} {
    	set project_dept_id [db_string project_dept "select project_cost_center_id as project_dept_id 
									 from im_projects 
									 where project_id = :project_id " -default "" ]
	return $project_dept_id
} 
 
 
 
 
 
ad_proc -public fud_active_customers {
    {-days_ago "365"}
} {
        Set all customers which did receive an offer or invoice within  the last 365  (default) days to "Active".
} {

    set company_type_ids [im_sub_categories [im_company_type_customer]]

    # Get all inactive customers
    set active_customer_ids [db_list inaktive "select company_id 
        from im_companies
        where company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])
        and company_status_id = [im_company_status_inactive]
        and company_id in (select customer_id from im_costs c where effective_date > now() - interval '$days_ago days' and cost_type_id in (3700, 3725))"]


    foreach customer_id $active_customer_ids {
	ns_log Notice "Activating [im_name_from_id $customer_id]"
	db_dml update_company "update im_companies set company_status_id = [im_company_status_active] where company_id = :customer_id"
    }

}

ad_proc -public fud_inactive_customers {
    {-days_ago "365"}
} {
        Set all customers which did not receive an offer within  the last 365 (default) days to "inactive".
} {
    # Exklude inaktive
    set exclude_status_ids [im_sub_categories [im_company_status_inactive]]
    lappend exclude_status_ids [im_company_status_potential]
    lappend exclude_status_ids [im_company_status_inquiries]
    lappend exclude_status_ids [im_company_status_qualifying]

    set company_type_ids [im_sub_categories [im_company_type_customer]]

    # Get all inactive customers
    set inactive_customer_ids [db_list inaktive "select company_id from im_companies
        where company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])
        and company_status_id = [im_company_status_active]
        and company_id not in (select customer_id from im_costs c where effective_date > now() - interval '$days_ago days' and cost_type_id in (3700, 3725))"]


    if {$inactive_customer_ids ne ""} {
	ns_log Notice "Inactive customers: $inactive_customer_ids"
	db_dml update_companies "update im_companies set company_status_id = [im_company_status_inactive] where company_id in ([template::util::tcl_to_sql_list $inactive_customer_ids])"
    }
}


ad_proc -public etm_inactive_customers_byquote {
    {-days_ago "365"}
} {
        Set all customers which did not receive an offer within  the last 365 (default) days to "inactive".
} {
    # Exklude inaktive
    set exclude_status_ids [im_sub_categories [im_company_status_inactive]]
#    lappend exclude_status_ids [im_company_status_potential]
    lappend exclude_status_ids [im_company_status_inquiries]
    lappend exclude_status_ids [im_company_status_qualifying]

    set company_type_ids [im_sub_categories [im_company_type_customer]]

    # Get all inactive customers
    set inactive_customer_ids [db_list inaktive "select company_id from im_companies
        where company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])
        and company_status_id in ([im_company_status_active],[im_company_status_potential])
        and company_id not in (select customer_id from im_costs c where effective_date > now() - interval '$days_ago days' and cost_type_id = 3702)"]


    if {$inactive_customer_ids ne ""} {
	ns_log Notice "Inactive customers: $inactive_customer_ids"
	db_dml update_companies "update im_companies 
	                         set company_status_id = [im_company_status_inactive] 
	                         where company_id in ([template::util::tcl_to_sql_list $inactive_customer_ids])"
    }
}







ad_proc etm_newcustomer_p {
  -customer_id:required
} {
    returns 1 if customer had had no invoice in the last 720 days otherwise returns 0
} {

 set invcount [db_string invct " select count(cost_id) invs 
                                    from im_costs 
                                    where customer_id = :customer_id 
                                          and cost_type_id = '3700'
                                          and effective_date > now() - interval '720 days'
                                          and cost_status_id not in ([etm_coststatus_collectionagency],
                                                                     [etm_coststatus_collectionagencyprep],
                                                                     [etm_coststatus_reminder1],
                                                                     [etm_coststatus_write],
                                                                     [etm_coststatus_writtenoff])
                                          
                                          " -default "0"]

if {$invcount > 0 } {set newcomp 0 } else {set newcomp 1}

return $newcomp


}



ad_proc -public etm_allcmids_by_cmid {
        -user_id:required
} {

    Returns list of all user_ids for the same CM
} {

    if {[im_user_is_pm_p $user_id]} {
    set sql_user_ids $user_id 
    set usersql  "select * 
        from persons, group_member_map gmm
        where 
        regexp_replace(lower(first_names || last_name), ' ...', '') = 
            (select regexp_replace(lower(first_names || last_name), ' ...', '')
            from persons
            where person_id = :user_id)
        and gmm.member_id = person_id
        and group_id = 467"

        set user_ids [db_list userids "$usersql"]
        if { "" != $user_ids } {
        set sql_user_ids [template::util::tcl_to_sql_list $user_ids]
        }  
     return $sql_user_ids
   }
  

}





ad_proc fud_trans_project_component {
	-project_id:required
} {
	Returns a formatted HTML table representing the status of the translation project
} {
	if {![im_project_has_type $project_id "Translation Project"]} { return "" }

	im_project_permissions [ad_get_user_id] $project_id view read write admin
	if {!$write} { return "" }

	set params [list \
			[list project_id $project_id] \
	]

	set result ""
	if {[catch {} err_msg]} {
		set result "Error in Translation Project Wizard:<p><pre>$err_msg</pre>"
	}

	set result [ad_parse_template -params $params "/packages/intranet-cust-fud/lib/trans-project-wizard"]

	return $result

}

ad_proc -public fud_invoice_permissions {
    user_id invoice_id view_var read_var write_var admin_var
} {
    Fill the "by-reference" variables read, write and admin
    with the permissions of $user_id on $invoice_id.<br>

} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

    im_cost_permissions $user_id $invoice_id view read write admin
    
    db_1row invoice_info "select cost_type_id, invoice_nr from im_costs, im_invoices where invoice_id = cost_id and cost_id = :invoice_id"
    
    # Remove write permissions for non site wide admins if we have a PDF generated
    if {![acs_user::site_wide_admin_p] &[im_cost_type_is_invoice_or_bill_p $cost_type_id]} {
        set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]
        set invoice_revision_id [content::item::get_live_revision -item_id $invoice_item_id]
        if {$invoice_revision_id ne ""} {
            # We have a live PDF for this invoice. Prohibit changes
            set write 0
        }
    }

    ds_comment "Testing... SWA [acs_user::site_wide_admin_p]"
    # Overwrite for SWA
    if {[acs_user::site_wide_admin_p]} {
	set read 1
	set write 1
	set admin 1
    }
}



ad_proc -public etm_searchname_bh {
    -company_id:required
} {
    
set searchname [db_string sn "
  select 
    case when company_type_id = 11000011 then (substring(company_path,1,3))
    else lower(substring(last_name,1,3)) end as searchname  
  from acs_rels acs, persons p, im_companies comp 
  where comp.company_id = acs.object_id_one 
    and object_id_one = :company_id
    and person_id = object_id_two
    and rel_type = 'im_company_employee_rel'
  limit 1" -default ""
]

    return $searchname
}



# procs for return of category_ids 
ad_proc -public etm_coststatus_collectionagency {} { return 11000325 }
ad_proc -public etm_coststatus_collectionagencyprep {} { return 11000426 }
ad_proc -public etm_coststatus_reminder1 {} { return 11000201 }
ad_proc -public etm_coststatus_write {} { return 11000503 }
ad_proc -public etm_coststatus_writeoff {} { return 11000503 }
ad_proc -public etm_coststatus_writtenoff {} { return 11000349 }
ad_proc -public etm_coststatus_prepaid {} { return 11000162 }

ad_proc -public etm_company_private {} {return 11000010}
ad_proc -public etm_company_business {} {return 11000011}

# missing in sys but used in some parts so just to prevent errors in log
ad_proc -public im_project_status_inquiring {} { return 72 }


# country categorization

ad_proc -public etm_country_is_dach_p {
 -country_code:required
} {
returns 1 if country is DACH (Germany / Austria / Switzerland)
} {
	set country_code [string tolower $country_code]
	switch $country_code {
		de - at - ch { set dach "1" }
		default { set dach "0" }
	} 

}



ad_proc -public etm_country_is_eu_p {
 -country_code:required
} {
returns 1 if country is EU member 
} {
	set country_code [string toupper $country_code]
	switch $country_code {
		BE 	-
		BG 	-
		CZ 	-
		DK 	-
		DE 	-
		EE 	-
		IE 	-
		EL 	-
		ES 	-
		FR 	-
		HR 	-
		IT 	-
		CY 	-
		LV 	-
		LT 	-
		LU 	-
		HU 	-
		MT 	-
		NL 	-
		AT 	-
		PL 	-
		PT 	-
		RO 	-
		SI 	-
		SK 	-
		FI 	-
		SE     	{ set eu "1" }
		default { set eu "0" }
	} 

}

ad_proc -public etm_iban_is_sepa_p {
 -country_code:required
} {
returns 1 if iban is available for sepa transfers using first 2 characters from a giving iban
} {
	set country_code [string toupper $country_code]
	switch $country_code {
	      BE	-
	      BG	-
	      DK	-
	      DE	-
	      EE	-
	      FI	-
	      FR	-
	      GR	-
	      GB	-
	      IE	-
	      IS	-
	      IT	-
	      HR	-
	      LV	-
	      LI	-
	      LT	-
	      LU	-
	      MT	-
	      MC	-
	      NL	-
	      NO	-
	      AT	-
	      PL	-
	      PT	-
	      RO	-
	      SM	-
	      CH	-
	      SE	-
	      ES	-
	      SI	-
	      SK	-
	      CZ	-
	      HU	-
	      CY	{ set sepa "1" }
	      default 	{ set sepa "0" }
	} 

}

ad_proc -public etm_next_project_nr_from_dept_id {
    -dept_id:required
} {

     switch $dept_id {
	85004 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like '9%'"]
	    incr project_nr
	    
	}
	554725 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like '3%'"]
	    incr project_nr
	    
	}
	304791 {
	    set project_nr [db_string fud_project "select max(project_nr) from im_projects where project_nr like 'Z%'"]
	    set project_nr [string trim $project_nr "Z"]
	    incr project_nr
	    set project_nr "Z$project_nr"
	    	    
	}
    }
 return $project_nr
}



  #######################################################################
## proc to remove leading windows like path (backslashs) of task

ad_proc -public etm_remove_path_from_task {
    -project_id:required
} { proc to remove leading windows-like path (backslashs) of task
} {

 

  set tasks [db_list_of_lists slash_tasks "select project_id,task_id, task_name
  from im_trans_tasks
  where task_name like ('%\%')
  and project_id = :project_id
  "]

  set slash "\\"
  #set new_task_list []

  foreach task $tasks {
    set task_name [lindex $task 2]
    set pos [expr [string last $slash $task_name] +  1]
    set new_task_name [string range $task_name $pos end]
    set task_id [lindex $task 1] 
    set idx 1
    while  {[db_string taskexists "select count(*) from im_trans_tasks where project_id = :project_id and task_name = :new_task_name"]} {
		set new_task_name "${new_task_name}_$idx" 
		incr idx
	  }
 

  
    if {$task_id ne "" && $new_task_name ne ""} {
	db_dml upd "update im_trans_tasks 
		    set task_name = :new_task_name, task_filename = :new_task_name
		    where task_id = :task_id"
      }
  }
}


ad_proc -public etm_sencha_project_member_component {
  -project_id
  -current_user_id
} {
  Procedure to return the project_members
} {

  set name_order [parameter::get -package_id [apm_package_id_from_key intranet-core] -parameter "NameOrder" -default 1]
  set group_l10n [lang::message::lookup "" intranet-core.Group "Group"]
  set object_id $project_id

  set return_url [export_vars -base "/sencha-assignment/project-manager-app" -url {project_id}]
  set sql_query "
    select
      rels.object_id_two as user_id, 
      rels.object_id_two as party_id, 
      im_email_from_user_id(rels.object_id_two) as email,
      coalesce(
        im_name_from_user_id(rels.object_id_two, $name_order), 
        :group_l10n || ': ' || acs_object__name(rels.object_id_two)
      ) as name,
      im_category_from_id(c.category_id) as member_role,
      c.category_gif as role_gif,
      c.category_description as role_description
    from
      acs_rels rels
      LEFT OUTER JOIN im_biz_object_members bo_rels ON (rels.rel_id = bo_rels.rel_id)
      LEFT OUTER JOIN im_categories c ON (c.category_id = bo_rels.object_role_id)
    where
      rels.object_id_one = :project_id and
      rels.object_id_two in (select party_id from parties) and
      rels.object_id_two not in (
        -- Exclude banned or deleted users
        select  m.member_id
        from  group_member_map m,
          membership_rels mr
        where m.rel_id = mr.rel_id and
          m.group_id = acs__magic_object_id('registered_users') and
          m.container_id = m.group_id and
          mr.member_state != 'approved'
      )
    order by 
      name  
  "

  set add_admin_links 0
  set project_lead_id [db_string project_lead "select project_lead_id from im_projects where project_id = :project_id" -default ""]

  if {[im_user_is_employee_p $current_user_id]} {
    set add_admin_links 1
  }
 
   if {[im_user_is_admin_p $current_user_id]} {
    set add_admin_links 1
  }
 

 
  # ------------------ Format the table header ------------------------
  set colspan 1
  set header_html "
      <tr> 
  <td class=rowtitle align=middle>[_ intranet-core.Name]</td>
  "
  

  if {$add_admin_links} {
    incr colspan
    append header_html "<td class=rowtitle align=middle><input type='checkbox' name='_dummy' onclick=\"acs_ListCheckAll('delete_user',this.checked)\"></td>"
  }
  append header_html "
  </tr>"

  # ------------------ Format the table body ----------------
  set td_class(0) "class=roweven"
  set td_class(1) "class=rowodd"
  set found 0
  set count 0
  set body_html ""
  set output_hidden_vars ""
  db_foreach users_in_group $sql_query {

    # Make up a GIF with ALT text to explain the role (Member, Key 
    # Account, ...
    set descr $role_description
    if {"" == $descr} { set descr $member_role }

    # Allow for object type specific localization of GIF and comment
    set member_role_key [lang::util::suggest_key $member_role]
    set descr_otype_key "intranet-core.Role_im_project_$member_role_key"
    set descr [lang::message::lookup "" $descr_otype_key $descr]
    set role_gif_key "intranet-core.Role_GIF_[lang::util::suggest_key $role_gif]"
    set role_gif [lang::message::lookup "" $role_gif_key $role_gif]
  
    set profile_gif [im_gif -translate_p 0 $role_gif $descr]

    incr count
    if { $current_user_id == $user_id } { set found 1 }

    # determine how to show the user: 
    # -1: Show name only, 0: don't show, 1:Show link
    set show_user [im_show_user_style $user_id $current_user_id $project_id]
    if {$show_user == 0} { continue }

    append output_hidden_vars "<input type=hidden name=member_id value=$user_id>"
    append body_html "
      <tr $td_class([expr $count % 2])>
        <td>
    "

    if {$show_user > 0} {
      append body_html "<A HREF=/intranet/users/view?user_id=$user_id>$name</A>"
    } else {
      append body_html $name
    }

    append body_html "$profile_gif</td>"
  
    if {$add_admin_links} {
      append body_html "
        <td align=middle>
          <input type='checkbox' name='delete_user' id='delete_user,$user_id' value='$user_id'>
 
        </td>
      "
    }
    append body_html "</tr>"
  }

  if { [empty_string_p $body_html] } {
    set body_html "<tr><td colspan=$colspan><i>[_ intranet-core.none]</i></td></tr>\n"
  }

  # ------------------ Format the table footer with buttons ------------
  set footer_html ""
  if {$add_admin_links} {
    append footer_html "
      <tr>
        <td align=left>
    <ul>
    <li><A HREF=\"[export_vars -base "/intranet/member-add" -url {object_id return_url}]\">[_ intranet-core.Add_member]</A>
    </ul>
        </td>
    "

    append footer_html "
      <tr>
        <td align=right colspan=$colspan>
    <select name=action>
    "

    append footer_html "
      <option value=del_members>[_ intranet-core.Delete_members]</option>
      </select>
      <input type=submit value='[_ intranet-core.Apply]' name=submit_apply></td>
        </td>
      </tr>
    "
  }

  # ------------------ Join table header, body and footer ----------------
  set html "
    <form method=POST action=/intranet/member-update>
    $output_hidden_vars
    [export_form_vars object_id return_url]
      <table bgcolor=white cellpadding=1 cellspacing=1 border=0>
        $header_html
        $body_html
        $footer_html
      </table>
  </form>
    "
  return $html
}


ad_proc -public etm_office_company_component { user_id company_id } {
    Creates a HTML table showing the table of offices related to the
    specified company.
} {
    set bgcolor(0) " class=roweven"
    set bgcolor(1) " class=rowodd"
    set office_view_page "/intranet/offices/view"

    set sql "
	select
		o.*,
		im_category_from_id(o.office_type_id) as office_type
	from
		im_offices o,
		im_categories c
	where
		o.company_id = :company_id
		and o.office_status_id = c.category_id
		and lower(c.category) not in ('inactive')
    "

    set component_html "
	<table cellspacing=1 cellpadding=1>
	<tr class=rowtitle>
	  <td class=rowtitle>[_ intranet-core.Office]</td>
	  <td class=rowtitle>[_ intranet-core.Tel]</td>
	</tr>
    "

    set ctr 1
    db_foreach office_list $sql {
	    set return_url [export_vars -base "/intranet/companies/view" -url {company_id}]
	    set office_url [export_vars -base "$office_view_page" -url {office_id return_url}]
	append component_html "
		<tr$bgcolor([expr $ctr % 2])>
		  <td>
		    <A href=\"$office_url\">$office_name</A>
		  </td>
		  <td>
		    $phone
		  </td>
		</tr>
        "
	incr ctr
    }
    if {$ctr == 1} {
	append component_html "<tr><td colspan=2>[_ intranet-core.No_offices_found]</td></tr>\n"
    }

    append component_html "
	<tr>
	  <td colspan=99 align=right>
	    <A href=/intranet-cust-fud/etm_office_new?office_type_id=11000005&company_id=$company_id>Neue Adresse</a>
	  </td>
	</tr>
	</table>
    "

    return $component_html
}


