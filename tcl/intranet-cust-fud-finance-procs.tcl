# packages/intranet-hmd/tcl/intranet-hmd-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {

	procs etm for financial purposes

	@author Malte Sussdorf
	@author etm
	@creation-date 2018-09-04
	@cvs-id $Id$
}

namespace eval intranet_cust_fud {}
package require csv

ad_proc -public intranet_cust_fud::inkasso_csv {
	-invoice_id
} {
    Generate a CSV Line for inkasso export
    @param invoice_id inkasso export
} {
    
    set today [db_string get_today_date "select sysdate from dual"]

    # Get all the invoice information

    if {![db_0or1row invoice_data {
       select customer_id,
            comp.company_id,
            cost_name,invoice_nr, to_char(effective_date, 'DD.MM.YYYY') as invoice_date,
            comp.company_name,
            first_names as contact_first_names,
            last_name as contact_last_name,
            im_email_from_user_id(company_contact_id) as email,
            o.address_line1,
            o.address_postal_code,
            o.address_city,
            o.address_country_code as country_code,
            o.phone,
            o.fax,
            comp.company_type_id,
            im_name_from_id(comp.company_type_id) as company_type,
            --pa.email,
            round((coalesce((c.amount * c.vat/100),0) + c.amount),2) as hauptforderung,
            c.currency ,
            round(((coalesce((c.amount * c.vat/100),0) + c.amount)*0.05),2) as nebenforderung,
            'letzte Zahlungserinnerung: ' || to_char(m.sent_date, 'DD.MM.YYYY') as bemerkung
	  from im_invoices i, 
		  persons pe, 
		  -- parties pa, 
		  im_offices o,
		  im_companies comp,
		  im_costs c left outer join
            (SELECT
                distinct on (context_id) im_name_from_id(acs_mail_log.context_id) as mcostname,
                context_id as mcost_id,
                sent_date
            FROM  acs_mail_log
            ORDER BY context_id  desc, sent_date desc
                ) m on (c.cost_id = m.mcost_id)
        
        where 
             c.cost_id = invoice_id
             and c.customer_id = comp.company_id
           -- and c.cost_id = m.mcost_id
            and i.company_contact_id = pe.person_id
            --and i.company_contact_id = party_id
            and i.invoice_office_id = o.office_id
            and i.invoice_id = :invoice_id
    }]} {
        ns_log Error "Could not get information for invoice_id $invoice_id"
        return ""
        ad_script_abort
    } 
    

        set hauptforderung [lc_numeric [expr $hauptforderung + 0] "%.2f" "de_DE"]
        
        #nebenforderung auf 5 eur begrenzen
        set nebenforderung 5.00
        set nebenforderung [lc_numeric [expr $nebenforderung + 0] "%.2f" "de_DE"]
        
       
    
      
	   ns_log Notice "$email"
        ns_log Notice "Generating inkasso invoice CSV line for $invoice_id without line items"
        
        


        set csv_values [list $company_id]; # 0 Kunden-ID
        lappend csv_values "$invoice_nr" ; # 1 Belegnummer
        lappend csv_values "$invoice_date" ; # 2 Rechnungs-Datum
        lappend csv_values "" ; # 3 Anrede        
        lappend csv_values "$contact_first_names" ; # 4 Vorname        
        lappend csv_values "$company_name" ; # 5 Nachname oder Firmenname
        lappend csv_values "" ; # 6 Geburtsdatum
        lappend csv_values "" ; # 7 Anrede
        lappend csv_values "$contact_first_names" ; # 8 Ansprechpartner-Vorname
        lappend csv_values "$contact_last_name" ; # 9 Ansprechpartner-Nachname
        lappend csv_values "$address_line1" ; # 10 Strasse
        lappend csv_values "" ; # 11 LKZ
        lappend csv_values "$address_postal_code" ; # 12 PLZ
        lappend csv_values "$address_city" ; # 13 Ort
        lappend csv_values "$country_code" ; # 14 Land
        lappend csv_values "$phone" ; # 15 Telefon
        lappend csv_values "$fax" ; # 16 Fax
        lappend csv_values "" ; # 17 Mobiltelefon
        lappend csv_values "$email" ; # 18 Email-Adresse
        lappend csv_values "$hauptforderung" ; # 19 Hauptforderung
        lappend csv_values "$currency" ; # 20 Währung
        lappend csv_values "$nebenforderung" ; # 21 Nebenforderung
        lappend csv_values "$bemerkung" ; # 22 Bemerkung
        

        set csv_line [::csv::join $csv_values ";"]
    
        return "$csv_line"
   
}

