# /packages/intranet-trans-project-wizard/www/trans-project-wizard.tcl
#
# Copyright (c) 2003-2007 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

if {![info exists project_id]} {
    ad_page_contract {
	Show the status and a description of a number of steps to
	execute a translation project
	@author frank.bergmann@project-open.com
    } {
	project_id
    }
}

# ---------------------------------------------------------------------
# Permissions & Defaults
# ---------------------------------------------------------------------
set fud_package_url [apm_package_url_from_key "intranet-cust-fud"]

if {![info exists project_id]} {
    ad_return_complaint 1 "Trans-Project-Wizard: No project_id specified"
}

set user_id [ad_maybe_redirect_for_registration]
im_project_permissions $user_id $project_id view read write admin
if {!$read} {
    ad_return_complaint 1 "<li>[_ intranet-core.lt_You_have_insufficient_6]"
    ad_script_abort
}

set bgcolor(0) " class=roweven"
set bgcolor(1) " class=rowodd"

set table_width 500

set return_url [im_url_with_query]
set project_url "/intranet/projects/view"
set help_gif_url "/intranet/images/help.gif"
set progress_url "/intranet/images/progress_greygreen"

set status_display(0) "<img src=$progress_url.0.gif>"
set status_display(1) "<img src=$progress_url.1.gif>"
set status_display(2) "<img src=$progress_url.2.gif>"
set status_display(3) "<img src=$progress_url.3.gif>"
set status_display(4) "<img src=$progress_url.4.gif>"
set status_display(5) "<img src=$progress_url.5.gif>"
set status_display(6) "<img src=$progress_url.6.gif>"
set status_display(7) "<img src=$progress_url.7.gif>"
set status_display(8) "<img src=$progress_url.8.gif>"
set status_display(9) "<img src=$progress_url.9.gif>"
set status_display(10) "<img src=$progress_url.10.gif>"

set freelance_invoices_installed_p [util_memoize [list db_string freelance_inv_exists "select count(*) from apm_packages where package_key = 'intranet-freelance-invoices'"]]

set project_closed_p [db_string project_open_p "
	select	1
	from	im_projects
	where	project_id = :project_id
	and project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_closed]]])
" -default 0]

# ---------------------------------------------------------------------
# Setup Multirow to store data
# ---------------------------------------------------------------------


multirow create call_to_quote status value url name description class
multirow create execution status value url name description class
multirow create invoicing status value url name description class

set multi_row_count 0

# ------------------------------------------------------------------------------------------------
# Call-To-Quote Worklfow
# ------------------------------------------------------------------------------------------------

set call_to_quote_header [lang::message::lookup "" intranet-trans-project-wizard.Call_to_Quote_header "From Call to Quote"]
set call_to_quote_description [lang::message::lookup "" intranet-trans-project-wizard.Call_to_Quote_Workflow_descr "
The 'Call to Quote' process leads you through the definition and setup of a new project
after a customer has contacted you.
The average duration is 2.5 minutes plus TM analysis time.
"]

# ---------------------------------------------------------------------
# Project Base Data
# ---------------------------------------------------------------------

# Show status "done" = 10, as the base data must have been entered to get
# to this page...

db_1row project_info "select
	p.project_nr,
	p.project_path,
	p.project_name,
	c.company_path,
	p.company_id
from
	im_projects p,
	im_companies c
where
	p.project_id=:project_id
	and p.company_id=c.company_id"

incr multi_row_count
multirow append call_to_quote \
    $status_display(10) \
    $project_nr \
    [export_vars -base "/intranet/projects/new" {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Define_Project_Base_Data_name "Define Project Base Data"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Project_Base_Data_descr "
	Base data include project name, project number, start and end date."] \
    $bgcolor([expr $multi_row_count % 2])


# ---------------------------------------------------------------------
# Source + Target Language
# ---------------------------------------------------------------------

set source_language [db_string source_language_status "
	select	im_category_from_id(source_language_id)
	from	im_projects
	where	project_id = :project_id
"]
if {"" == $source_language} { set source_language_status 0 } else { set source_language_status 10 }
if {0 == $source_language} { set source_language "-" }


set target_languages [db_list target_language_status "
	select	im_category_from_id(language_id)
	from	im_target_languages
	where	project_id = :project_id
"]
if {[llength $target_languages] > 0} { set target_language_status 10 } else { set target_language_status 0 }
if {0 == $target_languages} { set target_languages "-" }

set status [expr round(0.5*$source_language_status + 0.5*$target_language_status)]

incr multi_row_count
multirow append call_to_quote \
    $status_display($status) \
    "$source_language -&gt; [join $target_languages ", "]" \
    [export_vars -base "/intranet-translation/projects/edit-trans-data" {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Define_Source_and_Target_Language_name "Define Source and Target Languages"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Source_and_Target_Language_descr "
	You have to set the source and target languages of the project."] \
    $bgcolor([expr $multi_row_count % 2])



# ---------------------------------------------------------------------
# Translation Tasks
# ---------------------------------------------------------------------

set trans_tasks [db_string trans_tasks_status "
        select  count(*)
        from    im_trans_tasks
        where   project_id = :project_id
"]
if {$trans_tasks > 0} {
	set trans_tasks_status 10
} else {
	set trans_tasks_status 0
}
	
incr multi_row_count

	multirow append call_to_quote \
		$status_display($trans_tasks_status) \
		"$trans_tasks [lang::message::lookup "" intranet-trans-project-wizard.Trans_Tasks "Task(s)"]" \
		[export_vars -base "/intranet-translation/trans-tasks/task-list?view_name=trans_tasks" {project_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Trans_Tasks_name "Define Translation Tasks"] \
		[lang::message::lookup "" intranet-trans-project-wizard.Trans_Tasks_descr "Setup the information about the file to be translated."] \
		$bgcolor([expr $multi_row_count % 2])






# ---------------------------------------------------------------------
# Quotes available?
# ---------------------------------------------------------------------

set quotes [db_string quotes "
        select  count(*)
        from    im_costs c
        where   c.cost_type_id = [im_cost_type_quote]
		and (
			c.project_id = :project_id
		    OR
			c.cost_id in (
				select	object_id_two
				from	acs_rels
				where	object_id_one = :project_id
			)
		)
"]
if {$quotes > 0} { set quotes_status 10} else { set quotes_status 0}

#set quote_url "/intranet-trans-invoics/invoices/new?target_cost_type_id=3702"
set quote_url  "/intranet-translation/trans-tasks/task-action?action=replace_quote"
incr multi_row_count
multirow append call_to_quote \
    $status_display($quotes_status) \
    "$quotes [lang::message::lookup "" intranet-trans-project-wizard.Quote_s_ "Quote(s)"]" \
    [export_vars -base $quote_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Quotes_name "Write Quotes"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Quotes_descr "
	Create a quote by applying the customer's price list of the translation tasks."] \
    $bgcolor([expr $multi_row_count % 2])

# ---------------------------------------------------------------
# Warning if customer data is not filled
# ---------------------------------------------------------------
db_1row customer_info "select company_name, address_line1, address_postal_code, address_country_code, vat_number
	from im_companies c, im_offices o
	where c.main_office_id = o.office_id
	and c.company_id = :company_id"

if {$company_name eq "" || $address_line1 eq "" || $address_postal_code eq "" || $address_country_code eq ""} {
	incr multi_row_count
	multirow append call_to_quote \
		$status_display(0) \
		"[lang::message::lookup "" intranet-trans-project-wizard.Company_info_incom "Company Info is incomplete"]" \
		[export_vars -base "/intranet/companies/view" {company_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Update_company "Update Company"] \
		[lang::message::lookup "" intranet-trans-project-wizard.update_comp_descr "
		Review the company and provide full address information so we can create a quote."] \
		$bgcolor([expr $multi_row_count % 2])
}


if {$address_country_code ne "" && $address_country_code ne "de" && $vat_number eq ""} {
	incr multi_row_count
	multirow append call_to_quote \
		$status_display(0) \
		"[lang::message::lookup "" intranet-trans-project-wizard.Company_Vat "Company VAT is missing"]" \
		[export_vars -base "/intranet/companies/view" {company_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Update_company "Update Company"] \
		[lang::message::lookup "" intranet-trans-project-wizard.update_comp_descr "
		Review the company and provide the correct VAT number"] \
		$bgcolor([expr $multi_row_count % 2])
}

# ---------------------------------------------------------------------
# Hours Logged?
# ---------------------------------------------------------------------

set first_invoice_date [db_string first_invoice "
	select	min(creation_date)
	from	(
			select	o.creation_date + '30 minutes'::interval as creation_date
			from	im_costs c,
				acs_objects o
			where	c.cost_id = o.object_id and
				c.project_id = :project_id and
				cost_type_id in ([im_cost_type_invoice], [im_cost_type_quote])
		    UNION
			select	now() as creation_date
		) t
"]

set hours1 [db_string hours1 "
        select  sum(h.hours)
        from    im_hours h
        where   h.project_id = :project_id and
		h.user_id = :user_id and
		h.day < :first_invoice_date
"]

if {"" == $hours1} { set hours1 0 }
if {$hours1 > 0} { set hours1_status 10} else { set hours1_status 0}

set hours1_url "/intranet-timesheet2/hours/new"

incr multi_row_count
multirow append call_to_quote \
    $status_display($hours1_status) \
    "$hours1 [lang::message::lookup "" intranet-trans-project-wizard.Hours "Hours(s)"]" \
    [export_vars -base $hours1_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Hours1_name "Log Your Hours for Creating This Quote"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Hours1_descr "
	Please log your hours that you've spend to create the quote.
	This display only counts hours that you have logged until 30 minutes after creating
        your first quote for this project.
    "] \
    $bgcolor([expr $multi_row_count % 2])

# ---------------------------------------------------------------
# Set the project to open and download new tm/tb if necessary
# ---------------------------------------------------------------


set project_open_p [db_string project_open_p "
	select	1
	from	im_projects
	where	project_id = :project_id
	and project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_open]]])
" -default 0]

set quote_id [db_string quote "select cost_id from im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id and c.cost_id = o.object_id order by o.last_modified desc limit 1" -default ""]

if {!$project_open_p && $quote_id ne ""} {

	incr multi_row_count
	multirow append call_to_quote \
		"" \
		"" \
		[export_vars -base "$fud_package_url/quote2order" {project_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Open_Project "Set the project to OPEN."] \
		[lang::message::lookup "" intranet-trans-project-wizard.Open_Project_descr "
		After the customer accepted the latest quote, set the project to status open. If a new TB / TM is available
		you will be able to download the updated files.
		"] \
		$bgcolor([expr $multi_row_count % 2])
}


# ------------------------------------------------------------------------------------------------
# Execution Workflow
# ------------------------------------------------------------------------------------------------

set execution_header [lang::message::lookup "" intranet-trans-project-wizard.Execution_header "From Quote to Deliverable"]
set execution_description [lang::message::lookup "" intranet-trans-project-wizard.Execution_Workflow_descr "
The 'Quote to Deliverable' process covers the staffing, assignation and execution of project tasks.
The execution itself is driven by translators down- and uploading translation files.
"]




# ---------------------------------------------------------------------
# All files uploaded?
# ---------------------------------------------------------------------

set file_upload_status 0
set missing_task_list {}
set upload_files_url ""



if {[db_string memoq "select memoq_guid from im_projects where project_id = :project_id" -default ""] eq ""} {
    incr multi_row_count
    multirow append execution \
	$status_display($file_upload_status) \
	"[llength $missing_task_list] [lang::message::lookup "" intranet-trans-memoq.Present_files "File(s) founde"]" \
        [export_vars -base "/intranet-trans-memoq/create-and-analyse" -url {project_id}] \
	[lang::message::lookup "" intranet-trans-memoq.Create_and_upload "Create MemoQ Project and upload files"] \
	[lang::message::lookup "" intranet-trans-memoq.Create_and_upload_desc "
        Make sure all files have been analyzed."] \
	$bgcolor([expr $multi_row_count % 2])
} else {

    incr multi_row_count
    multirow append execution \
	$status_display($file_upload_status) \
	"[llength $missing_task_list] [lang::message::lookup "" intranet-trans-memoq.Present_files "File(s) founde"]" \
        [export_vars -base "/intranet-trans-memoq/analyse" -url {project_id}] \
	[lang::message::lookup "" intranet-trans-memoq.run_analysis "Run MemoQ analysis"] \
	[lang::message::lookup "" intranet-trans-memoq.run_analysis_desc "
        Run analysis without creating new project."] \
	$bgcolor([expr $multi_row_count % 2])

    incr multi_row_count
    multirow append execution \
	$status_display($file_upload_status) \
	"[llength $missing_task_list] [lang::message::lookup "" intranet-trans-memoq.Present_files "File(s) founde"]" \
        [export_vars -base "/intranet-trans-memoq/download-analysis" -url {project_id}] \
	[lang::message::lookup "" intranet-trans-memoq.download_analysis "Download MemoQ analysis"] \
	[lang::message::lookup "" intranet-trans-memoq.download_analysis_desc "
        Download CSV of the analysis in MemoQ Format."] \
	$bgcolor([expr $multi_row_count % 2])
}


incr multi_row_count
multirow append execution \
    $status_display($file_upload_status) \
    "[llength $missing_task_list] [lang::message::lookup "" intranet-trans-project-wizard.Missing_Files "Missing File(s)"]" \
    $upload_files_url \
    [lang::message::lookup "" intranet-trans-project-wizard.Upload_Missing_Files "Upload Missing File(s)"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Upload_Missing_Files_descr "
        Make sure all task files have been uploaded."] \
    $bgcolor([expr $multi_row_count % 2])


	set pdf_generated_status 0
	set pdf_generated_nr 0
	set pdf_dir "[im_filestorage_project_path $project_id]/PDF"
	
	if {[file exists $pdf_dir]} {
		set pdf_generated_status 10
		if {[catch {glob -directory "$pdf_dir" "*.pdf"} pdf_file_list]} {
			set pdf_file_list ""
		}
		set pdf_generated_nr [llength $pdf_file_list]
	}
	
	incr multi_row_count
	multirow append execution \
		$status_display($pdf_generated_status) \
		"$pdf_generated_nr PDFs generated" \
		[export_vars -base "/sencha-freelance-translation/download-pdfs" {project_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Download_PDFs "Download Source PDFs"] \
		[lang::message::lookup "" intranet-trans-project-wizard.Download_PDFs_descr "
			Generate PDFs for all source files if possible and download the result."] \
		$bgcolor([expr $multi_row_count % 2])
	

# ---------------------------------------------------------------------
# Freelancers defined?
# ---------------------------------------------------------------------

# Get the total number of steps across all tasks
set total_num_of_steps 0
db_foreach step "select task_id, aux_string1 from im_trans_tasks tt, im_categories c
	where c.category_id = tt.task_type_id
	and tt.project_id = :project_id" {
		set total_num_of_steps [expr $total_num_of_steps + [llength [split $aux_string1 " "]]]
	}

# Get the num of assigned tasks (where we have an assignment)
set assignment_list [db_list assignments "select distinct trans_task_id || '-' || package_type_id
	from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa
	where fp.freelance_package_id = fptt.freelance_package_id
	and fa.freelance_package_id = fp.freelance_package_id
	and fp.project_id = :project_id"]
set total_num_of_assigned_steps [llength $assignment_list]

set all_translators [db_string num_assignees "select count(distinct assignee_id)
	from im_freelance_assignments fa, im_freelance_packages fp
	where fa.freelance_package_id = fp.freelance_package_id
	and fp.project_id = :project_id" -default 0]

if {$total_num_of_steps >0} {
	set freelancers_status [expr 10 * $total_num_of_assigned_steps / $total_num_of_steps]
} else {
	set freelancers_status 0
}

set member_add_url "/sencha-assignment/freelancers-select"

incr multi_row_count
multirow append execution \
    $status_display($freelancers_status) \
    "$all_translators [lang::message::lookup "" intranet-trans-project-wizard.Translators "Freelancer(s)"]" \
    [export_vars -base $member_add_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Freelancers_name "Select Freelancers"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Freelancers_descr "
	Select a number of translators, editors and other resources to execute your project."] \
    $bgcolor([expr $multi_row_count % 2])

set accepted_list [db_list assignments "select distinct trans_task_id || '-' || package_type_id
	from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa
	where fp.freelance_package_id = fptt.freelance_package_id
	and fp.freelance_package_id = fa.freelance_package_id
	and fa.assignment_status_id in (4222,4224,4225,4226,4228)
	and fp.project_id = :project_id"]
	
set total_num_of_accepted_steps [llength $accepted_list]
if {$total_num_of_steps >0} {
	set freelancers_status [expr 10 * $total_num_of_accepted_steps / $total_num_of_steps]
} else {
	set freelancers_status 0
}

if {$total_num_of_assigned_steps >0} {
	
	incr multi_row_count
	multirow append execution \
		$status_display($freelancers_status) \
		"$total_num_of_accepted_steps / $total_num_of_steps [lang::message::lookup "" sencha-freelance-translation.accepted "accepted"]" \
		[export_vars -base "/sencha-freelance-translation/assignments" {project_id {display_only_p 1} return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.View_assignments "View Assignments"] \
		[lang::message::lookup "" intranet-trans-project-wizard.View_assignments_descr "
		View the assignments currently in the system"] \
		$bgcolor([expr $multi_row_count % 2])
}



# ---------------------------------------------------------------------
# Assignations
# ---------------------------------------------------------------------

set assigned_tasks [db_string assigned_tasks "
        select  count(*)
        from    im_trans_tasks
        where   (trans_id is not NULL or
		 edit_id is not NULL or
		 proof_id is not NULL or
		 other_id is not NULL
		)
		and project_id = :project_id
"]
if {0 != $trans_tasks} {
    set assignations_status [expr 10 * $assigned_tasks / $trans_tasks]
} else {
    set assignations_status 0
}
if {$assignations_status > 10} { set assignations_status 10 }

set assign_tasks_url "/intranet-translation/trans-tasks/task-assignments"
if {0} {
incr multi_row_count
multirow append execution \
    $status_display($assignations_status) \
    "$assigned_tasks [lang::message::lookup "" intranet-trans-project-wizard.Assigned_Tasks "Assigned Task(s)"]" \
    [export_vars -base $assign_tasks_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Assignations_name "Assign Translators to Tasks"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Assignations_descr "
	Assign translators to the project tasks to determine who should do what."] \
    $bgcolor([expr $multi_row_count % 2])

}


# ---------------------------------------------------------------------
# Purchase_Orders written
# ---------------------------------------------------------------------

if {$freelance_invoices_installed_p} {
	
	# Get the number of assignments for freelancers
	set num_assignments [db_string num_assignments "select count(assignment_id)
		from im_freelance_assignments fa, im_freelance_packages fp, group_distinct_member_map m
		where fa.freelance_package_id = fp.freelance_package_id
		and m.member_id = fa.assignee_id
		and fa.assignment_status_id in (4222,4224,4225,4226,4228)
		and m.group_id =  [im_freelance_group_id]
		and fp.project_id = :project_id" -default 0]
		
	set num_assignments_with_po [db_string num_assignments "select count(assignment_id)
	from im_freelance_assignments fa, im_freelance_packages fp, group_distinct_member_map m
	where fa.freelance_package_id = fp.freelance_package_id
	and m.member_id = fa.assignee_id
	and m.group_id =  [im_freelance_group_id]
	and fa.purchase_order_id is not null
	and fp.project_id = :project_id" -default 0]

	if {$num_assignments >0} {
		set purchase_orders_status [expr 10 * $num_assignments_with_po / $num_assignments]
	} else {
		set purchase_orders_status 0
	}
    if {$purchase_orders_status > 10} { set purchase_orders_status 10 }

	set missing_po_freelancer_ids [db_list missing_po "select distinct assignee_id
		from im_freelance_assignments fa, im_freelance_packages fp, group_distinct_member_map m
		where fa.freelance_package_id = fp.freelance_package_id
		and m.member_id = fa.assignee_id
		and m.group_id =  [im_freelance_group_id]
		and fa.assignment_status_id in (4222,4224,4225,4226,4228)
		and fa.purchase_order_id is null
		and fp.project_id = :project_id"]

	set freelancer_list [join $missing_po_freelancer_ids ","]
    set write_po_url [export_vars -base "/sencha-freelance-translation/assignments" {project_id {freelancer_ids $freelancer_list}}]
    
    incr multi_row_count
    multirow append execution \
	$status_display($purchase_orders_status) \
	"$num_assignments_with_po [lang::message::lookup "" intranet-trans-project-wizard.POs "PO(s)"]" \
	$write_po_url \
	[lang::message::lookup "" intranet-trans-project-wizard.POs_name "Write Purchase Orders"] \
	[lang::message::lookup "" intranet-trans-project-wizard.POs_descr "
	Apply the translator's price list to project tasks to generate purchase orders."] \
	$bgcolor([expr $multi_row_count % 2])

}


# ------------------------------------------------------------------------------------------------
# Post-Delivery Workflow
# ------------------------------------------------------------------------------------------------

set invoicing_header [lang::message::lookup "" intranet-trans-project-wizard.Invoicing_header "From Deliverable to Cash"]
set invoicing_description [lang::message::lookup "" intranet-trans-project-wizard.Invoicing_Workflow_descr "
The invoicing workflow leads you
"]



# ---------------------------------------------------------------------
# Provider Bills
# ---------------------------------------------------------------------

set bills [db_string bills "
        select  count(*)
        from    im_costs
        where   project_id = :project_id
                and cost_type_id = [im_cost_type_bill]
"]

if {0 != $num_assignments_with_po} {
    set bills_status [expr round(10 * $bills / $num_assignments_with_po)]
} else {
    set bills_status 0
}
if {$bills_status > 10} { set bills_status 10}

set bill_from_po_url "/intranet-invoices/new-copy-invoiceselect?source_cost_type_id=3706&target_cost_type_id=3704"

incr multi_row_count
multirow append invoicing \
    $status_display($bills_status) \
    "$bills [lang::message::lookup "" intranet-trans-project-wizard.Bills "Bills"]" \
    [export_vars -base $bill_from_po_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Bills_name "Write Bill"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Bills_descr "
	Each Purchase Order should be folled by a 'Provider Bill' for the same provider."] \
    $bgcolor([expr $multi_row_count % 2])




# ---------------------------------------------------------------------
# Invoices
# ---------------------------------------------------------------------

set invoices [db_string invoices "
        select  count(*)
        from    im_costs
        where   cost_type_id = [im_cost_type_invoice]
		and (
			project_id = :project_id
		   OR
			cost_id in (
				select	object_id_two
				from	acs_rels
				where	object_id_one = :project_id
			)
		)

"]

if {0 != $quotes} {
    set invoices_status [expr round(10 * $invoices / $quotes)]
} else {
    set invoices_status 0
}
if {$invoices_status > 10} { set invoices_status 10}

set write_invoices_url "/intranet-invoices/new-copy-invoiceselect?source_cost_type_id=3702&target_cost_type_id=3700"

incr multi_row_count
multirow append invoicing \
    $status_display($invoices_status) \
    "$invoices [lang::message::lookup "" intranet-trans-project-wizard.Invoices "Invoices"]" \
    [export_vars -base $write_invoices_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Invoices_name "Write Invoice"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Invoices_descr "
	Each Quote should be folled by an Invoice to the customer."] \
    $bgcolor([expr $multi_row_count % 2])



# ---------------------------------------------------------------------
# Bills Payment
# ---------------------------------------------------------------------

set paid_bills [db_string paid_bills "
        select  count(*)
        from    im_costs
        where   project_id = :project_id
                and cost_type_id = [im_cost_type_bill]
		and paid_amount > 0
"]

if {0 != $bills} {
    set paid_bills_status [expr round(10 * $paid_bills / $bills)]
} else {
    set paid_bills_status 0
}
if {$paid_bills_status > 10} { set paid_bills_status 10}

set pay_bills_url "/intranet-invoices/list?cost_type_id=3704"

incr multi_row_count
multirow append invoicing \
    $status_display($paid_bills_status) \
    "$paid_bills [lang::message::lookup "" intranet-trans-project-wizard.Paid_Bills "Paid Bills"]" \
    [export_vars -base $pay_bills_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Paid_Bills_name "Pay Providers"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Paid_Bills_descr "
	Each Provider Bill should register a payment."] \
    $bgcolor([expr $multi_row_count % 2])


# ---------------------------------------------------------------------
# Invoices Payment
# ---------------------------------------------------------------------

set paid_invoices [db_string paid_invoices "
        select  count(*)
        from    im_costs
        where   project_id = :project_id
                and cost_type_id = [im_cost_type_invoice]
		and paid_amount > 0
"]

if {0 != $invoices} {
    set paid_invoices_status [expr round(10.0 * $paid_invoices / $invoices)]
} else {
    set paid_invoices_status 0
}
if {$paid_invoices_status > 10} { set paid_invoices_status 10}

set pay_invoices_url "/intranet-invoices/list?cost_type_id=3700"

incr multi_row_count
multirow append invoicing \
    $status_display($paid_invoices_status) \
    "$paid_invoices [lang::message::lookup "" intranet-trans-project-wizard.Paid_Invoices "Paid Invoices"]" \
    [export_vars -base $pay_invoices_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Paid_Invoices_name "Receive Payments"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Paid_Invoices_descr "
	Each Invoice should register a payment."] \
    $bgcolor([expr $multi_row_count % 2])

# ---------------------------------------------------------------------
# Hours Logged?
# ---------------------------------------------------------------------

set first_invoice_date [db_string first_invoice "
	select	min(creation_date)
	from	(
			select	o.creation_date as creation_date
			from	im_costs c,
				acs_objects o
			where	c.cost_id = o.object_id and
				c.project_id = :project_id and
				cost_type_id in ([im_cost_type_invoice], [im_cost_type_quote])
			UNION
			select	now() as creation_date
		) t
"]

set hours2 [db_string hours2 "
		select  sum(h.hours)
		from    im_hours h
		where   h.project_id = :project_id and
		h.user_id = :user_id and
		h.day > :first_invoice_date
"]

if {"" == $hours2} { set hours2 0 }
if {$hours2 > 0} { set hours2_status 10} else { set hours2_status 0}

set hours2_url "/intranet-timesheet2/hours/new"

incr multi_row_count
multirow append invoicing \
	$status_display($hours2_status) \
	"$hours2 [lang::message::lookup "" intranet-trans-project-wizard.Hours "Hours(s)"]" \
	[export_vars -base $hours2_url {project_id return_url}] \
	[lang::message::lookup "" intranet-cust-fud.hours2_name "Log Your Hours for Delivering this Project"] \
	[lang::message::lookup "" intranet-cust-fud.hours2_descr "
	Please log your hours that you've spend to deliver the project
	This display only counts hours that you have logged after creating
		your first quote for this project.
	"] \
	$bgcolor([expr $multi_row_count % 2])


# ---------------------------------------------------------------
# Close the project (if hours are logged)
# ---------------------------------------------------------------

if {!$project_closed_p} {
	incr multi_row_count
	
	multirow append invoicing \
		"" \
		"" \
		[export_vars -base "$fud_package_url/close-project" {project_id return_url}] \
		[lang::message::lookup "" intranet-cust-fud.Close_Project "Close Project"] \
		[lang::message::lookup "" intranet-cust-fud.Close_Project_descr "Close the project and run all final tasks."] \
		$bgcolor([expr $multi_row_count % 2])
}
	



