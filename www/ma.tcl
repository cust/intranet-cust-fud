# packages/intranet-cust-fud/www/ma.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	Prepare an invoice for "Mahnung"
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-11-29
    @cvs-id $Id$
} {
    invoice_id:notnull
} 


db_1row invoice_info "select cost_name, company_id,company_name,im_cost_center_name_from_id(cost_center_id) as agent from im_costs,im_companies where cost_id = :invoice_id and customer_id = company_id"

regsub -all {[^a-zA-Z0-9_]} $company_name "" ma_company_name
set ma_company_name [string range $ma_company_name 0 19]
set cost_folder [im_filestorage_cost_path $invoice_id]
set ma_name "MA_${agent}_${ma_company_name}-${company_id}-${cost_name}"
set ma_dir "${cost_folder}/$ma_name"
file mkdir $ma_dir

set ma_html [im_filestorage_base_component [ad_conn user_id] $invoice_id $ma_name $ma_dir "cost"]