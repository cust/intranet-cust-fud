# packages/intranet-mail/www/reply.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Allow to reply to write a new E-Mail
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-04-27
    @cvs-id $Id$
     @author ETM
} {
    invoice_id:notnull
    {return_url ""}
    {subject ""}
    {body ""}
    {recipient_id ""}
    {mail_agent ""}
} 

set page_title "[_ intranet-invoices.Invoice_Mail]"
set user_id [ad_conn user_id]

db_1row invoice_info "select invoice_nr,last_modified from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"

set invoice_url "/intranet-invoices/view?invoice_id="
set cost_id $invoice_id

set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]

if {"" == $invoice_item_id} {
    set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
} else {
    set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]

    # Check if we need to create a new revision
    if {[db_string date_check "select 1 from acs_objects where object_id = :invoice_revision_id and last_modified < :last_modified" -default 0]} {
        set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
    }
}

set user_id [ad_conn user_id]

if {![db_0or1row related_projects_sql "
        select distinct
        r.object_id_one as project_id,
        p.project_name,
        project_lead_id,
        customer_id as company_id,
        im_name_from_id(project_lead_id) as project_manager,
        im_email_from_user_id(project_lead_id) as project_manager_mail,
        p.project_nr,
        p.parent_id,
        p.description,
        trim(both p.company_project_nr) as customer_project_nr,
        pe.signature,
        c.cost_id,
        lower(substr(im_name_from_id(c.template_id),1,3)) as mail_template_check,
        c.effective_date,
        c.effective_date::date + c.payment_days as due_date, 
        now()::date - interval '10 days' as payment_date,
	   now()::date + interval '10 days' as reminder_date,
	   now()::date - effective_date::date - payment_days as overdue_days 

    from
        acs_rels r,
        im_projects p,
        im_costs c,
        persons pe
    where
        r.object_id_one = p.project_id
        and r.object_id_two = :invoice_id
        and c.cost_id = :invoice_id
        and p.project_lead_id = pe.person_id
        order by project_id desc
        limit 1
"]} {
    set project_name ""
    set project_manager [im_name_from_user_id $user_id]
    set project_lead_id $user_id
    set project_manager_mail [im_email_from_user_id $user_id]
    set customer_project_nr ""
    set project_nr ""
    set cost_id ""
}



#for reminders
set kundenkontobuchpruefung "Durchsicht Ihres Kundenkontos" 
# template-mail data for editing
set recipient_locale "de_DE"	
set effective_date_pretty [lc_time_fmt $effective_date %q $recipient_locale]
set due_date_pretty [lc_time_fmt $due_date %q $recipient_locale]
set payment_date_pretty [lc_time_fmt $payment_date %q $recipient_locale]

set reminder_date_pretty [lc_time_fmt $reminder_date %q $recipient_locale]

# Transform the signature
set signature [template::util::richtext::get_property html_value $signature]

		
set email $project_manager_mail

#790807  - accountancy@panoramalanguages.com
#2490696 - buchhaltung@fachuebersetzungsagentur.com
#788458  - accountancy@fachuebersetzungsdienst.com





if {"" == $mail_agent } {set mail_agent [db_string userdept "select lower(im_name_from_id(department_id)) as cm_agent from im_employees  where  employee_id = :user_id" -default ""] }

if { $mail_agent == $mail_template_check } {
switch -glob $mail_agent {

	pan {	set cc "790807"
			set bcc "$email"
			set name ""
			
			set subject "Offene Rg $invoice_nr Zahlungserinnerung"
			set body "
			<strong>
			Zahlungserinnerung
			<br />
			Rechnungsnummer: $invoice_nr
			<br />
			Kundennummer: $company_id
			</strong>
			<br />
			<br />							
			
			Sehr geehrte Damen und Herren,
			<br />
			<br />
			eine $kundenkontobuchpruefung hat ergeben, dass zu oben genannter Rechnung bis zum heutigen Tag kein Zahlungseingang festzustellen ist, 
			obwohl die Rechnung am $due_date_pretty fällig war. Wir möchten Sie daher bitten, den offenen Betrag innerhalb der nächsten 10 Tage zu begleichen, um mögliche Mahngebühren zu vermeiden.
			<br />
			Sollten Sie den Rechnungsbetrag entgegen dieser Aufstellung bereits zum Ausgleich gebracht haben, übermitteln Sie uns doch bitte den zugehörigen Zahlungsbeleg, um jenen zuordnen zu können.
			
			<br /> <br />Mit freundlichen Grüßen<br /><br />$signature"
			}
	zis {	set cc "2490696"
			set bcc "$email"
			set name ""
			set subject "Rg $invoice_nr Erinnerung"
			set body "Sehr geehrte Damen und Herren,
			<br /> <br />
			eine $kundenkontobuchpruefung hat gezeigt, dass die angehangene Rechnung <strong> $invoice_nr </strong> zum Fälligkeitstermin nicht beglichen wurde. 
			Unter Berücksichtigung Ihrer Zahlungseingänge bis zum $payment_date_pretty erinnern wir Sie daher heute an die Begleichung. Bitte führen Sie diese bis zum <strong>$reminder_date_pretty</strong> aus.
			<br /> Besten Dank.
			<br /> <br />Mit freundlichen Grüßen<br /><br />$signature"
			
			}
	fud  {  	set cc "788458"
			set bcc "$email"
			set name ""
			set subject "Zahlungserinnerung $invoice_nr"
			set body "Sehr geehrte Damen und Herren,
				\<br \/\> \<br \/\>
				
				eine  $kundenkontobuchpruefung hat ergeben, dass die Zahlung für die Rechnung $invoice_nr vom $effective_date_pretty noch nicht bei uns eingegangen und seit $overdue_days Tagen überfällig ist. 
				Hierbei wurden Zahlungseingänge bis zum $payment_date_pretty berücksichtigt.
				<br /><br />Sollten Sie den fälligen Betrag inzwischen bezahlt haben, möchten wir Sie bitten, uns kurz das Datum der Zahlung mitzuteilen, da unter Umständen die Zahlung nicht zuzuordnen war. 
				Andernfalls überweisen Sie bitte den Rechnungsbetrag innerhalb der nächsten 10 Tage, also bis zum <strong>$reminder_date_pretty</strong> auf eines der angegebenen Konten. 
				<br /> Bitte geben Sie bei Rückfragen und Zahlungen immer Rechnungs- und Kundennummer an:
				<br /> <br /> 
	
				<li><strong>Rechnungs-Nr: $invoice_nr </strong></li>
					<li> <strong>Kunden-Nr: $company_id</strong></li> 
				<br /> <br /> Mit freundlichen Grüßen <br /> <br /> $signature"
		}
	}
} else {
			
			set email "webmin@fachuebersetzungsdienst.com"
			set cc ""
			set bcc ""
			set name ""
			set subject "Error Reminder $invoice_nr ($cost_id) - no cost_center $mail_agent <> $mail_template_check"
			set body "<a href=www.etranslationmanagement.com/$invoice_url$cost_id>$invoice_nr</a>"
}

db_0or1row user_info "select first_names, last_name from persons where person_id = :recipient_id"

# Get the type information so we can get the strings
set invoice_type_id [db_string type "select cost_type_id from im_costs where cost_id = :invoice_id"]

set recipient_locale [lang::user::locale -user_id $recipient_id]

if {$recipient_id != "" } { set recipient_id $recipient_id 
	} else {set recipient_id [db_string company_contact_id "select company_contact_id from im_invoices where invoice_id = :invoice_id" -default $user_id] }



if {$subject != "" } { set subject $subject 
	} else {set subject [lang::util::localize "#intranet-invoices.invoice_email_subject_${invoice_type_id}#" $recipient_locale] }

if {$body != "" } { set body $body
	} else {set body [lang::util::localize "#intranet-invoices.invoice_email_body_${invoice_type_id}#" $recipient_locale]}
	
if {$cc != "" } { 
	set cc_id $cc
	set cc_ids $cc
   } else {
	set cc_id ""
	set cc_ids ""
   }
	
#set cc_id ""	
	
if {![ad_looks_like_html_p $body]} {
    set body [ad_text_to_html $body]
}
