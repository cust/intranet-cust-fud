# /packages/intranet-cust-fud/www/reports/etm_bh_FLR-fll-vll.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/ for licensing details.


ad_page_contract {
	testing reports	
    @param start_year Year to start the report
    @param start_unit Month or week to start within the start_year
} {
    { start_date "" }
    { end_date "" }
    { level_of_detail 1 }
	 { agent "fud" }
    { output_format "html" }
    { number_locale "" }
    { customer_id:integer 0}
}

# ------------------------------------------------------------
# Security

# Label: Provides the security context for this report
# because it identifies unquely the report's Menu and
# its permissions.
set current_user_id [ad_maybe_redirect_for_registration]
set menu_label "etm_bh_FLR-fll"

set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

if {![string equal "t" $read_p]} {
    ad_return_complaint 1 "<li>
[lang::message::lookup "" intranet-reporting.You_dont_have_permissions "You don't have the necessary permissions to view this page"]"
    return
}


# ------------------------------------------------------------
# Defaults

set rowclass(0) "roweven"
set rowclass(1) "rowodd"

set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
set cur_format [im_l10n_sql_currency_format]
set date_format [im_l10n_sql_date_format]
set locale [lang::user::locale]
if {"" == $number_locale} { set number_locale $locale  }

set company_url "/intranet/companies/view?company_id="
set invoice_url "/intranet-invoices/view?invoice_id="
set user_url "/intranet/users/view?user_id="
set this_url [export_vars -base "/intranet-cust-fud/reports/etm_bh_inkassoTransfer" {start_date end_date} ]


# Deal with invoices related to multiple projects
#im_invoices_check_for_multi_project_invoices


# ------------------------------------------------------------
# Constants
#



# Show all details for this report (no grouping)
set level_of_detail 1

# ETM: agent und VLL oder FLL
#set agentauswahl {fud_ford "FUD Fordg" fud_verb "FUD Verbkt" pan_ford "PAN Fordg" pan_verb "PAN Verbkt" zis_ford "ZIS Fordg" zis_verb "ZIS Verbkt"} 
#set agentauswahl {zis_ford "ZIS Fordg" zis_verb "ZIS Verbkt"} 
set agentauswahl {fud "FUD" pan "PAN" zis "ZIS" } 
# ------------------------------------------------------------
# Argument Checking

# Check that Start & End-Date have correct format
if {"" != $start_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
}

if {"" != $end_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $end_date]} {
    ad_return_complaint 1 "End Date doesn't have the right format.<br>
    Current value: '$end_date'<br>
    Expected format: 'YYYY-MM-DD'"
}

# ------------------------------------------------------------
# Page Settings

set page_title "agents inkasso transfer"
set context_bar [im_context_bar $page_title]
set context ""

#set help_text "
#<strong>Forderungen oder Verbindlichkeiten innerhalb start-end nach Belegdatum<br><br>
#<table><nobr>
#<tr><td><nobr><strong>ZIS</strong></td><td><nobr><strong>Fremdleistungen</strong></td><td><nobr><strong>Standardkonten</strong></td></tr>
#<tr><td><nobr>ZIS: privat   | de 		-> 4400 </td><td><nobr>Projektaufwand	5201 </td><td><nobr>Werbung	6600</td></tr>
#<tr><td><nobr>ZIS: privat 	 | eu 		-> 4400 </td><td><nobr>Projektmanagement	5210 </td><td><nobr>Telefon	6805</td></tr>
#<tr><td><nobr>ZIS: privat 	 | noneu 	-> 4338 </td><td><nobr>Domainverwaltung	5901 </td><td><nobr>Telefax und Internet	6810</td></tr>
#<tr><td><nobr>ZIS: business | de 		-> 4400 </td><td><nobr>Virtuelles Office	5902 </td><td><nobr>Bürobedarf	6815</td></tr>
#<tr><td><nobr>ZIS: business | eu 		-> 4336 </td><td><nobr>Programmierung	5903 </td><td><nobr>Fortbildungskosten	6821</td></tr>
#<tr><td><nobr>ZIS: business | noneu 	-> 4338 </td><td><nobr>Verwaltung	5904 </td><td><nobr>Abschluss- und Prüfungskosten	6827</td></tr>
#<tr><td><nobr></td><td>Verwaltung System	5905 </td><td><nobr>Buchführungskosten	6830</td></tr>
#<tr><td><nobr></td><td>Betreuung Website	5907 </td><td><nobr>Mieten für Einrichtungen (bewegliche WG)	6835</td></tr>
#<tr><td><nobr></td><td></td><td><nobr>Nebenkosten Geldverkehr	6855</td></tr>
#</nobr>
#</table>
#"

set help_text ""


# ------------------------------------------------------------
# Set the defaults


db_1row start_date "
select
    date_trunc('WEEK', current_date - 7)::date as last_week_monday
from dual
"
# if {"" == $start_date} { 
#     set start_date "$last_week_monday"
# }

if {"" == $start_date} { 
    set start_date "2014-01-01"
}
db_1row end_date "
select
	(date_trunc('WEEK', current_date - 7)::date) + 6 as last_week_sunday
from dual
"
if {"" == $end_date} { 
    set end_date "$last_week_sunday"
}


# eu-laender
set eu "'AT','BE','BG','CY','CZ','DE','DK','EE','EL','ES','FI','FR','HR','HU','IE','IT','LT','LU','LV','MT','NL','PL','PT','RO','SE','SI','SK','UK'"
set noneu ""


## cost-centers
##525;"The Company"
##12368;"System Administration"
##526;"Administration"
##529;"Marketing"
##530;"Operations"
##12364;"Software Development"
##12388;"Project Management"
##304791;"ZIS"
##85004;"FUD"
##53182;"ProjMan"
##554725;"PAN"


# ETM: setzen der agentur und FLL/VLL


##if {[regexp {[fud_*]$} $agent]} {
##							set buchungstextpre "'PA '"
##							set buchungstextpost "ci.cost_name"
##							set cust_or_prov "ci.provider_id"
##							set cost_type "'3704'"
##							set belegnummerauswahl "ci.note"
##						}

set nointernals_where "\nAND comp.company_type_id not in (53,11000000)"

if { $agent == "fud" } {	set agent_where "and im_name_from_id(cost_center_id) = 'FUD'"							
   }	elseif { $agent == "pan" }  {set agent_where "and im_name_from_id(cost_center_id) = 'PAN'"
   }	elseif { $agent == "zis" } { set agent_where "and im_name_from_id(cost_center_id) = 'ZIS'"
   } 
	

set today [dt_sysdate]


### ------------------------------------------------------------
### Conditional SQL Where-Clause

##set criteria [list]

##if {0 != $customer_id} {
##    lappend criteria "cust.company_id = :customer_id"
##}

##set where_clause [join $criteria " and\n            "]
##if { ![empty_string_p $where_clause] } {
##    set where_clause " and $where_clause"
##}



if { ![empty_string_p $agent_where] } {
    set agent_where "$agent_where"
}

# if { ![empty_string_p $buchungstextpre] } {
# 	 set buchungstextpre "$buchungstextpre"
# }


# ------------------------------------------------------------
# Define the report - SQL, counters, headers and footers 
#



set sql "

select distinct on (cost_id) 
	c.customer_id,
	c.cost_name,
	to_char(c.effective_date, 'DD.MM.YYYY') as cost_date,
	Null as anrede,
	case 	when company_type_id = '11000010' then pers.first_names
		when company_type_id = '11000011' then Null 
	end as firstname,
	case 	when company_type_id = '11000010' then pers.last_name
		when company_type_id = '11000011' then comp.company_name 
	end as company_name,
	Null as birthdate,
	Null as contact_title,
	pers.first_names as contact_firstname,
	pers.last_name as contact_lastname,
	o.address_line1 as street,
	Null as lkz,
	o.address_postal_code,
	o.address_city,
	o.address_country_code,
	o.phone,
	o.fax,
	Null as mobilephone,
	part.email,
	round((coalesce((c.amount * c.vat/100),0) + c.amount),2) as hauptforderung,
        c.currency,
	round((coalesce((c.amount * c.vat/100),0) + c.amount)*0.05,2) as nebenforderung,
	'letzte Zahlungserinnerung: ' || to_char(m.sent_date, 'DD.MM.YYYY') as bemerkung,
	im_category_from_id(comp.company_type_id) companytype,
     c.cost_id,
     im_name_from_id(cost_status_id) cost_status
        
from 
(
	SELECT 
	  im_name_from_id(acs_mail_log.context_id) as mcostname, 
	  context_id as mcost_id,
	  acs_mail_log.sent_date
	FROM 
	  public.acs_mail_log
	where acs_mail_log.sent_date < (current_date - integer '10' )
	) m, 
	im_costs c, im_invoices i, 
	persons pers , parties part, 
	im_offices o, im_companies comp
where 	c.cost_id = m.mcost_id
	and i.invoice_id = m.mcost_id
	and i.company_contact_id = pers.person_id
	and i.company_contact_id = part.party_id
	and i.invoice_office_id = o.office_id
	and comp.company_id = c.customer_id
	
	and (to_char(effective_date, 'YYYY')::integer  + 4) > extract(year from current_date)
	and cost_status_id = 11000426
     --and (noautoreminder is  null or noautoreminder <> 't')
	$agent_where

"



			set report_def [list \
				 group_by customer_id \
				 header {
						$customer_id
						"<a href=$invoice_url$cost_id>$cost_name</a>"
						$cost_date
						$anrede
						$firstname
						"<a href=$company_url$customer_id>$company_name</a>"
						$birthdate
						$contact_title					
						$contact_firstname
						$contact_lastname
						$street
						$lkz
						$address_postal_code
						$address_city
						$address_country_code
						$phone
						$fax
						$mobilephone
						$email
						$hauptforderung
						$currency
						$nebenforderung
						$bemerkung
						$companytype
						$cost_id
						$today
						$cost_status
						

						
				 } \
				 content {} \
				 footer {} \
			]




# Global header/footer
set header0 {
"Kunden-ID" 
"Rechnungs-Nummer" 
"Rechnungs-Datum" 
"Anrede" 
"Vorname" 
"Nachname oder Firmenname" 
"Geburtsdatum" 
"Ansprechpartner-Anrede" 
"Ansprechpartner-Vorname" 
"Ansprechpartner-Nachname" 
"Strasse" 
"LKZ" 
"PLZ" 
"Ort" 
"Land" 
"Telefon" 
"Fax" 
"Mobiltelefon" 
"Email-Adresse" 
"Hauptforderung" 
"Währung" 
"Nebenforderung" 
"Bemerkung" 
"Kundentyp" 
"cost_id"
"TransferDatum"
"Status"
}
set footer0 {}




# ------------------------------------------------------------
# Start formatting the page header
#

# Write out HTTP header, considering CSV/MS-Excel formatting
im_report_write_http_headers -output_format $output_format

# Add the HTML select box to the head of the page
switch $output_format {
    html {
	ns_write "
	[im_header]
	[im_navbar]
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	<td>
	<form>
		[export_form_vars customer_id]
		<table border=0 cellspacing=1 cellpadding=1>
		<tr>
		  <td class=form-label>Agentur</td>
		  <td class=form-widget>
		    [im_select -translate_p 0 agent $agentauswahl $agent]
		  </td>
		</tr>
		<tr>
		  <td class=form-label>Start Date</td>
		  <td class=form-widget>
		    <input type=textfield name=start_date value=$start_date>
		  </td>
		</tr>
		<tr>
		  <td class=form-label>End Date</td>
		  <td class=form-widget>
		    <input type=textfield name=end_date value=$end_date>
		  </td>
		</tr>
                <tr>
                  <td class=form-label>Format</td>
                  <td class=form-widget>
                    [im_report_output_format_select output_format "" $output_format]
                  </td>
                </tr>
                <tr>
                  <td class=form-label><nobr>Number Format</nobr></td>
                  <td class=form-widget>
                    [im_report_number_locale_select number_locale $number_locale]
                  </td>
                </tr>
		<tr>
		  <td class=form-label></td>
		  <td class=form-widget><input type=submit value=Submit></td>
		</tr>
		</table>
	</form>
	</td>
	<td>
		<table cellspacing=2 width=90%>
		<tr><td>$help_text</td></tr>
		</table>
	</td>
	</tr>
	</table>
	<table border=0 cellspacing=3 cellpadding=3>\n"
    }
}


# ------------------------------------------------------------
# Start formatting the report body
#

im_report_render_row \
    -output_format $output_format \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"


set footer_array_list [list]
set last_value_list [list]
set class "rowodd"

#ns_log Notice "intranet-reporting-finance/finance-income-statement: sql=\n$sql"

db_foreach sql $sql {



# original-betraege wenn nicht EUR
# 	if { "EUR" == $currency } {
# 		set buchungsbetrag_orig_net ""
# 		set buchungsbetrag_orig_net_pretty ""
# 		set currency ""
# 	} else { set buchungsbetrag_orig_net_pretty [im_report_format_number $buchungsbetrag_orig_net $output_format $number_locale] }

     
# 	set buchungsbetrag_netto_pretty [im_report_format_number $buchungsbetrag_netto $output_format $number_locale]
# 	set buchungsbetrag_steuer_pretty [im_report_format_number $buchungsbetrag_steuer $output_format $number_locale]
# 	set buchungsbetrag_brutto_pretty [im_report_format_number $buchungsbetrag_brutto $output_format $number_locale]
# 

set rounding_precision 2
#set nebenforderung [expr $hauptforderung * 0.05]
set nebenforderung [im_report_format_number $nebenforderung $output_format $number_locale $rounding_precision]
set hauptforderung [im_report_format_number $hauptforderung $output_format $number_locale $rounding_precision]
    
    im_report_display_footer \
	-output_format $output_format \
	-group_def $report_def \
	-footer_array_list $footer_array_list \
	-last_value_array_list $last_value_list \
	-level_of_detail $level_of_detail \
	-row_class $class \
	-cell_class $class

   
    
    set last_value_list [im_report_render_header \
	    -output_format $output_format \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
    ]

    set footer_array_list [im_report_render_footer \
	    -output_format $output_format \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
    ]
}

im_report_display_footer \
    -output_format $output_format \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -output_format $output_format \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1ü


switch $output_format {
    html { ns_write "</table>\n[im_footer]\n" }
}
