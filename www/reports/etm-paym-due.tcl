# /intranet-reporting-finance/etm-paym-due.tcl
#
# Copyright (c) 2003-2006 ]project-open[
#
# All rights reserved. 
# Please see http://www.project-open.com/ for licensing.



# ------------------------------------------------------------
# Page Contract 
#


ad_page_contract {
    Payments past due

    @param start_date Start date (YYYY-MM-DD format) 
    @param end_date End date (YYYY-MM-DD format) 
} {
    { start_date "" }
    { end_date "2099-12-31" }
    { level_of_detail:integer 3 }
    { customer_id:integer 0 }
}


# ------------------------------------------------------------
# Security (New!)
#
# The access permissions for the report are taken from the
# "im_menu" Menu Items in the "Reports" section that link 
# to this report. It's just a convenient way to handle 
# security, that avoids errors (different rights for the 
# Menu Item then for the report) and redundancy.

# What is the "label" of the Menu Item linking to this report?
set menu_label "etm-paym-due"

# Get the current user and make sure that he or she is
# logged in. Registration is handeled transparently - 
# the user is redirected to this URL after registration 
# if he wasn't logged in.
set current_user_id [ad_maybe_redirect_for_registration]
set user_id $current_user_id
# Determine whether the current_user has read permissions. 
# "db_string" takes a name as the first argument 
# ("report_perms") and then executes the SQL statement in 
# the second argument. 
# It returns an error if there is more then one result row.
# im_object_permission_p is a PlPg/SQL procedure that is 
# defined as part of ]project-open[.
set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

# For testing - set manually
#set read_p "t"

# Write out an error message if the current user doesn't
# have read permissions and abort the execution of the
# current screen.
if {![string equal "t" $read_p]} {
    set message "You don't have the necessary permissions to view this page"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
}

if {$start_date == "" } {
 set start_date [db_string startmax "select to_char(date('now') - interval '3 year', 'YYYY-MM-DD') as max_start " -default '2015-01-01']
}
# ------------------------------------------------------------
# Check Parameters (New!)
#
# Check that start_date and end_date have correct format.
# We are using a regular expression check here for convenience.

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $end_date]} {
    ad_return_complaint 1 "End Date doesn't have the right format.<br>
    Current value: '$end_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

# Maxlevel is 3. 
#if {$level_of_detail > 3} { set level_of_detail 3 }
set level_of_detail 4


# ------------------------------------------------------------
# Page Title, Bread Crums and Help
#
# We always need a "page_title".
# The "context_bar" defines the "bread crums" at the top of the
# page that allow a user to return to the home page and to
# navigate the site.
#

set page_title "Payment-Past-Due"
set context_bar [im_context_bar $page_title]


# ------------------------------------------------------------
# Default Values and Constants
#


# Default report line formatting - alternating between the
# CSS styles "roweven" (grey) and "rowodd" (lighter grey).
#
set rowclass(0) "roweven"
set rowclass(1) "rowodd"

 
#
set currency_format "999,999,999.09"
set date_format "DD-MM-YYYY"

# URLs 
#
set company_url "/intranet/companies/view?company_id="
set newcontact_url "/intranet/users/new?profile=461&also%5fadd%5fto%5fbiz%5fobject="
set project_url "/intranet/projects/view?view_name=finance&project_id="
set invoice_url "/intranet-invoices/view?invoice_id="
set user_url "/intranet/users/view?user_id="
set this_url [export_vars -base "/intranet-cust-fud/reports/etm-paym-due" {start_date end_date} ]
#set invoice_reminder_url "/intranet-cust-fud/invoice-reminder_1inv?return_url=$this_url&cost_id="
set invoice_reminder_url "/intranet-cust-fud/invoice-reminder?return_url=$this_url&cost="
set invoice_reminderEDIT_url "/intranet-cust-fud/etm_mail?return_url=$this_url&invoice_id="
set invoice_postreminder_url "/intranet-cust-fud/invoice-reminder?return_url=$this_url&post_reminder=1&cost="
set noautoinkasso_url "/intranet-cust-fud/company_changes?return_url=$this_url&company_status_id=&company_id=" 
set compstatus_url "/intranet-cust-fud/company_changes?return_url=$this_url&ft=&company_id=" 

set invpdf_url "/intranet-invoices/pdf?return_url=$this_url&invoice_id="
set reminderlog_mail_url "/intranet-mail/one-message?log_id="

# 
# Level of Details
#
set levels {2 "Customers" 3 "Customers+PM"} 



# ------------------------------------------------------------
# Report SQL

set customer_sql ""
if {0 != $customer_id} {
    set customer_sql "and p.company_id = :customer_id\n"
}

set useragency ""
set reminder ""
set reminder_letter ""
set reminder_log ""

set usermail [db_string dept "select email from parties where party_id = :user_id " -default '']
set useragency [db_string dept "select im_name_from_id(department_id) from im_employees where employee_id = :user_id" -default '']




#db_0or1row useragency "select im_name_from_id(department_id) from im_employees where employee_id = :user_id"

set where_in ""
set where_not_in ""



set where_in "and project_lead_id = :user_id"
set orderby "ORDER BY p.project_lead_id, c.cost_status_id,c.effective_date, (customer_name) ASC, project_name ASC"
set cost_status_where "  AND c.cost_status_id not in ( '3810', '3812','3813','11000197', '11000325', '11000349', '11000366', '11000391', '11000426')"

set user_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
if {$user_admin_p == 1 } { 
	set useragency "ALL"
	set orderby "ORDER BY  cost_status_id ASC,(customer_name) ASC, cm"
	set where_in "" 
	set cost_status_where "  AND c.cost_status_id not in ( '3810', '3812','3813','11000197', '11000349', '11000366', '11000391')"
 }



#if {$user_id eq 352568} {set useragency "FUD"}

# # FUD  28022 | PAN  552735 | ZIS  279215
# if {$cost_center_id eq 85004 } {
# 		set agentur "FUD"
# 		set agent_id "28022"
# 	} elseif { $cost_center_id eq 554725 }  {
# 		set agentur "PAN"
# 		set agent_id "552735"
# 	} elseif { $cost_center_id eq  304791 }  {
# 		set agentur "ZIS"
# 		set agent_id "279215"
# 
# 	} else { set where_in "" }

# marcus sieht alle von sich und von sarah
if {$user_id eq 298648 } {
		set where_in "AND p.project_lead_id in ('298648', '1306956')"
		set orderby "ORDER BY  (customer_name) ASC, project_name ASC ,  c.effective_date"
}



set report_sql "
SELECT 
  p.project_id as project_id,
  person__name(p.project_lead_id) as cm,
  p.project_lead_id,
  im_company__name(c.customer_id) as customer_name,
  im_name_from_id(i.company_contact_id) as contact,
  comp.company_status_id,
  c.customer_id,
  i.company_contact_id,
  p.project_nr as project_nr,
  c.cost_name as cost_name, 
  im_project__name(p.project_id) as project_name, 
  to_char(c.effective_date, :date_format) as invoice_date, 
  to_char(c.effective_date, 'YYYY') as effdate_year,
  c.payment_days as payment_days, 
  to_char(c.effective_date::date + c.payment_days, :date_format) as deadline, 
  (current_date - (c.effective_date::date + c.payment_days)) as days_past_due,
  to_char(c.amount, :currency_format) as amount_formatted,
  amount,
  c.currency as currency, 
  paid_amount,
  to_char(c.paid_amount, :currency_format) as paid_amount_formatted, 
  c.paid_currency as paid_currency, 
  --c.project_id, 
  cost_id,
  im_name_from_id(template_id) as template,
  c.cost_nr, 
  p.company_id, 
  im_category_from_id(p.project_status_id) as project_status,
  im_category_from_id(c.cost_status_id) as cost_status,
  cost_status_id,
  manager_id as keyacc_id,
  im_name_from_id(manager_id) as keyacc,
  comp.noautoreminder,
  invoice_nr,
  c.template_id,
  c.effective_date,
  c.effective_date::date + c.payment_days as due_date, 
  now()::date - interval '10 days' as payment_date,
  now()::date + interval '10 days' as reminder_date,
  pe.signature,
  now()::date - effective_date::date - payment_days as overdue_days

FROM 
  public.im_costs c,
  im_invoices i,
  public.im_projects p,
  persons pe,
  public.im_companies comp
WHERE 
  p.company_id = comp.company_id
  and c.cost_id = i.invoice_id
  AND c.project_id = p.project_id
  AND p.project_nr NOT LIKE ('1%')
  AND c.cost_type_id in ('3700', '3725')
  AND (c.paid_amount is NULL OR c.paid_amount < c.amount)
  AND c.cost_status_id not in ( '3810', '3812','3813','3819','11000197', '11000349', '11000366', '11000391')
  AND c.amount > 0
  AND (c.effective_date::date + c.payment_days) < current_date
  AND c.effective_date >= :start_date
  AND c.effective_date <= :end_date
  AND cost_center_id != '12375'
  and p.project_lead_id = pe.person_id
  $cost_status_where
  $where_in
  $orderby
"








# ------------------------------------------------------------
# Report Definition
#

if { $useragency eq "ZIS" }  {
			# Global Header Line
			set header0 { 
				"Cust /  Contact"
				"Invoice-No <br>(Project-No)" 
				"Invoice-Date <br>(Deadline)"
				"Paym days <br> (due)"
				"Amount"
				"Paid"
				"Project Status <br> Cost Status"
				"Send Reminder"
				"Reminder log"				
				"Inkasso-Transfer"
				
			}

			set report_def [list \
				group_by customer_id \
						header {	
								"<b><a href=$company_url$company_id>$customer_name</a></b><br>$contact<br><br>$new_contact"
							} \
						content [list \
							header { 	
								
							""
							"<a href=$invoice_url$cost_id>$cost_name</a> <br> <a href=$project_url$project_id>$project_nr</a> <br>
							<a href=$invpdf_url$cost_id&render_template_id=$template_id>PDF</a>"
							"$invoice_date <br> ($deadline)"
		 					"$payment_days <br> ($days_past_due)"
							"$amount_formatted $currency"
							"$paid_amount_formatted $paid_currency"
							"P: $project_status <br> <b> C: $cost_status"
							"$remindermail_contacts"
							"$reminder_log"


							$admin_links

								 
								} \
							content {} \
						
						] \
						footer {} \
					] \
			
	}  elseif {$user_admin_p == 1 } {
			# Global Header Line
			set header0 {
				"Cust" 
				"CM <br>(KeyAccMan)"
				"Invoice-No <br>(Project-No)" 
				"Invoice-Date <br>(Deadline)"
				"Paym days <br> (due)"
				"Amount"
				"Paid"
				"Project Status <br> Cost Status"
				"Send Reminder"
				"Reminder log"				
				"admin links"
				"admincomp links"
			}
			
			set report_def [list \
				group_by cost_status_id \
				header {
					"<b>$cost_status</b>"
				} \
					content [list \
						group_by customer_id \
						header {} \
					content [list \
						header {
							"<b><a href=$company_url$company_id>$customer_name</a></b><br>$company_id<br>$company_contact_id<br><br>$new_contact"
							"$cm <br> $keyacc "
							"<a href=$invoice_url$cost_id>$cost_name</a> <br> <a href=$project_url$project_id>$project_nr</a> <br>
							<a href=$invpdf_url$cost_id&render_template_id=$template_id>PDF</a>"
							"$invoice_date <br> $deadline"
		 					"$payment_days <br> $days_past_due "
							"$amount $currency"
							"$paid_amount $paid_currency"
							"P: $project_status <br> <b> C: $cost_status </b> <br>T: $template"
							
							"$remindermail_contacts"
							"$reminder_log"
							$admin_links
							$admincomp_links
							
							
						  } \
						content {} \
					] \
						footer {} \
				] \
				footer { 
				} \
			]	
			
		}	else {
			# all cm not ZIS nor admin
			set header0 {
				"CM" 
				"Cust"
				"Cust"
				"Invoice-No <br>(Project-No)" 
				"Invoice-Date <br>(Deadline)"
				"Paym days <br> (due)"
				"Amount"
				"Paid"
				"Project Status <br> Cost Status"
				"Send Reminder"
				"Reminder log"
				"Inkasso-Transfer"
				
			}
			
			set report_def [list \
				group_by cm \
					header {
						"<b>$cm</b>"
						$useragency
					} \
					content [list \
						group_by customer_id \
						header {	"" 
							"<b><a href=$company_url$company_id>$customer_name</a></b><br>$contact<br><br>$new_contact"
									
							} \
						content [list \
							header { 	
								""
								""
								""
							
							"<a href=$invoice_url$cost_id>$cost_name</a> <br> <a href=$project_url$project_id>$project_nr</a> <br>
							<a href=$invpdf_url$cost_id&render_template_id=$template_id>PDF</a>"
							"$invoice_date <br> ($deadline)"
		 					"$payment_days <br> ($days_past_due)"
							"$amount_formatted $currency"
							"$paid_amount_formatted $paid_currency"
							"P: $project_status <br> <b> C: $cost_status"
							
							"$remindermail_contacts"
							"$reminder_log"

							$admin_links

								} \
							content {} \
						
						] \
						footer {} \
					] \
					footer { } \
			]	
			
		}
















set ttt {
		"<nobr><i>$po_per_quote_perc</i></nobr>"
		"<nobr><i>$gross_profit</i></nobr>"
}

if {$user_admin_p == 1 } { 
# Global Footer Line
	set footer0 {
			""
			""
			""
			""
			""
			""
			""
			""
			"Total"
			"<b>$invoice_total</b>"  
			"" 
			"<b>$invoicepaid_total </b>"
			"" 
			""
			""
			""
	}
} else { set footer0 {} }


#
# Subtotal Counters (per customer)
#

set invoice_subtotal_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_subtotal \
        reset \$customer_id \
        expr "\$amount+0" \
]

set invoicepaid_subtotal_counter [list \
        pretty_name "Paid Amount" \
        var invoicepaid_subtotal \
        reset \$customer_id \
        expr "\$paid_amount+0" \
]



#
# Grand Total Counters
#
set invoice_grand_total_counter [list \
       pretty_name "Invoice Amount" \
       var invoice_total \
       reset 0 \
       expr "\$amount+0" \
]



set invoicepaid_grand_total_counter [list \
       pretty_name "Paid Amount" \
       var invoicepaid_total \
       reset 0 \
       expr "\$paid_amount+0" \
]






set counters [list \
$invoice_subtotal_counter \
$invoicepaid_subtotal_counter \
$invoice_grand_total_counter \
$invoicepaid_grand_total_counter \
]

# Set the values to 0 as default
set invoice_total 0
set invoicepaid_total 0



# ------------------------------------------------------------
# Start Formatting the HTML Page Contents
#

#
ad_return_top_of_page "
	[im_header]
	[im_navbar]
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	  <td width='30%'>
		<!-- 'Filters' - Show the Report parameters -->
		<form>
		<table cellspacing=2>
		<tr class=rowtitle>
		  <td class=rowtitle colspan=2 align=center>Filters</td>
		</tr>
		
		<!--
		<tr>
		  <td>Level of<br>Details</td>
		  <td>
		    [im_select -translate_p 0 level_of_detail $levels $level_of_detail]
		  </td>
		</tr>
		-->

		<tr>
		  <td><nobr>Start Date:</nobr></td>
		  <td><input type=text name=start_date value='$start_date'></td>
		</tr>
		<tr>
		  <td>End Date:</td>
		  <td><input type=text name=end_date value='$end_date'></td>
		</tr>
		<tr>
		  <td</td>
		  <td><input type=submit value='Submit'></td>
		</tr>
		</table>
		</form>
	  </td>
	</tr>
	</table>
	
	<!-- Here starts the main report table -->
	<table border=0 cellspacing=1 cellpadding=1>
"







set footer_array_list [list]
set last_value_list [list]

im_report_render_row \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"

set counter 0
set class ""
db_foreach sql $report_sql {

	# Select either "roweven" or "rowodd" from
	# a "hash", depending on the value of "counter".
	# You need explicite evaluation ("expre") in TCL
	# to calculate arithmetic expressions. 
	set class $rowclass([expr $counter % 2])
	
	
	# status change 
	set internal_reminder 11000395
	set outstanding 3804 
	set paid 3810
	set reminder1 11000201
	set outstanding_prep 11000162
	
	set comp_links ""
	
	
	
	
	
	set invoice_id $cost_id
	set coststatus2paid_url ""
	
	set sent_date ""
	set receiver ""
	set log_id ""
	
	set reminder_log [list] 
	db_foreach remindermail { 
		select aml.log_id,im_name_from_id(recipient_id) as receiver,to_char(sent_date, 'YYYY-MM-DD') sent_date, subject 
		from acs_mail_log aml left outer join acs_mail_log_recipient_map amm on (aml.log_id = amm.log_id) 
		where amm.type = 'to' and context_id = :cost_id
		order by sent_date DESC
		} {
			if { [regexp {PROBLEM.*} $subject]} { set subject "<font color=red>$subject</font>" }
			lappend reminder_log "<a href=$reminderlog_mail_url$log_id>$sent_date</a><br>An: $receiver<br>$subject<br>"
		}
		set reminder_log [join $reminder_log "<br>"]
		if { $cost_status_id != 3810 } {
		set coststatus2paid_url "/intranet-cust-fud/cost_status?return_url=$this_url&cost_status_id=3810&invoice_id="
	}
	
	
	# template-mail data for editing
		set recipient_locale "de_DE"	
		set effective_date_pretty [lc_time_fmt $effective_date %q $recipient_locale]
		set payment_date_pretty [lc_time_fmt $payment_date %q $recipient_locale]
		set due_date_pretty [lc_time_fmt $due_date %q $recipient_locale]
		set reminder_date_pretty [lc_time_fmt $reminder_date %q $recipient_locale]
		
		# Transform the signature
		set signature [template::util::richtext::get_property html_value $signature]

	
		if {$effdate_year > 2016} { set kundenkontobuchpruefung "Durchsicht Ihres Kundenkontos" } else {set kundenkontobuchpruefung "externe Buchprüfung"}
		#FUD 85004 | PAN 554725 | ZIS 304791
		set project_lead_email [im_email_from_user_id $user_id]
		set email "$project_lead_email"

		
				switch -glob $template {
				
					pan* {	set cc "accountancy@panoramalanguages.com"
							set bcc "$email"
							set mail_agent "pan"
							
							set subject "Offene Rg $invoice_nr Zahlungserinnerung"
							set body "
							<strong>
							Zahlungserinnerung
							<br />
							Rechnungsnummer: $invoice_nr
							<br />
							Kundennummer: $company_id
							</strong>
							<br />
							<br />							
							
							Sehr geehrte Damen und Herren,
							<br />
							<br />
							eine $kundenkontobuchpruefung hat ergeben, dass zu oben genannter Rechnung bis zum heutigen Tag kein Zahlungseingang festzustellen ist, 
							obwohl die Rechnung am $due_date_pretty fällig war. Wir möchten Sie daher bitten, den offenen Betrag innerhalb der nächsten 10 Tage zu begleichen, um mögliche Mahngebühren zu vermeiden.
							<br />
							Sollten Sie den Rechnungsbetrag entgegen dieser Aufstellung bereits zum Ausgleich gebracht haben, übermitteln Sie uns doch bitte den zugehörigen Zahlungsbeleg, um jenen zuordnen zu können.
							
							<br /> <br />Mit freundlichen Grüßen<br /><br />$signature"
							}
					zis* {	set cc "buchhaltung@fachuebersetzungsagentur.com"
							set bcc "$email"
							set mail_agent "zis"

							set subject "Rg $invoice_nr Erinnerung"
							set body "Sehr geehrte Damen und Herren,
							<br /> <br />
							eine $kundenkontobuchpruefung hat gezeigt, dass die angehangene Rechnung <strong> $invoice_nr </strong> zum Fälligkeitstermin nicht beglichen wurde. 
							Unter Berücksichtigung Ihrer Zahlungseingänge bis zum $payment_date_pretty erinnern wir Sie daher heute an die Begleichung. Bitte führen Sie diese bis zum <strong>$reminder_date_pretty</strong> aus.
							<br /> Besten Dank.
							<br /> <br />Mit freundlichen Grüßen<br /><br />$signature"
							
							}
					fud*  {  set cc "accountancy@fachuebersetzungsdienst.com"
							set bcc "$email"
							set mail_agent "fud"
							set subject "Zahlungserinnerung $invoice_nr"
							set body "Sehr geehrte Damen und Herren,
								\<br \/\> \<br \/\>
								
								eine  $kundenkontobuchpruefung hat ergeben, dass die Zahlung für die Rechnung $invoice_nr vom $effective_date_pretty noch nicht bei uns eingegangen und seit $overdue_days Tagen überfällig ist. 
								Hierbei wurden Zahlungseingänge bis zum $payment_date_pretty berücksichtigt.
								<br /><br />Sollten Sie den fälligen Betrag inzwischen bezahlt haben, möchten wir Sie bitten, uns kurz das Datum der Zahlung mitzuteilen, da unter Umständen die Zahlung nicht zuzuordnen war. 
								Andernfalls überweisen Sie bitte den Rechnungsbetrag innerhalb der nächsten 10 Tage, also bis zum <strong>$reminder_date_pretty</strong> auf eines der angegebenen Konten. 
								<br /> Bitte geben Sie bei Rückfragen und Zahlungen immer Rechnungs- und Kundennummer an:
								<br /> <br /> 
									<li><strong>Rechnungs-Nr: $invoice_nr </strong></li>
									<li> <strong>Kunden-Nr: $company_id</strong></li> 
								<br /> <br /> Mit freundlichen Grüßen <br /> <br /> $signature"
						}
					default {
							set email "webmin@fachuebersetzungsdienst.com"
							set cc ""
							set bcc ""
							set subject "Error Reminder $invoice_nr ($cost_id) - no cost_center"
							set body "<a href=www.etranslationmanagement.com/$invoice_url$cost_id>$invoice_nr</a>"
					}

					
				}



	
	#set mail_edit "$invoice_reminderEDIT_url$cost_id&recipient_id=$company_contact_id&body=$body&subject=$subject"
	
	
	## todo: add edit mail to lappend in loop
	#<a href=$invoice_reminderEDIT_url${cost_id}&recipient_id=${company_contact_id}&mail_agent=$mail_agent>(EditMail)<a/></li>
	
	set remindermail_contacts [list] 
	lappend remindermail_contacts "Erinnerung an"
	set contact_reminder_url "$invoice_reminder_url$cost_id&mail_to="
	db_foreach remindermailcontact_sq1 { 
		select  object_id_two as compcompany_contact_id , im_name_from_id(object_id_two) as contact
		from acs_rels ar
		where ar.object_id_one = :company_id
		and object_id_two <> '624'
		and ar.rel_type = 'im_company_employee_rel' } {
		 if {$compcompany_contact_id == $company_contact_id} {
		  lappend remindermail_contacts "<li><a href=$contact_reminder_url$compcompany_contact_id><strong>$contact</strong></a>
		  <a href=$invoice_reminderEDIT_url${cost_id}&recipient_id=${compcompany_contact_id}&mail_agent=$mail_agent><strong>(EditMail)</strong><a/></li>"  
		  } else {
		   lappend remindermail_contacts "<li><a href=$contact_reminder_url$compcompany_contact_id>$contact</a>
		   <a href=$invoice_reminderEDIT_url${cost_id}&recipient_id=${compcompany_contact_id}&mail_agent=$mail_agent>(EditMail)<a/></li>"  
		  }
	}
		 lappend remindermail_contacts "<li><a href=$invoice_reminderEDIT_url${cost_id}&recipient_id=$project_lead_id&mail_agent=$mail_agent>Mail2OtherContact<a/></li>"
		set remindermail_contacts [join $remindermail_contacts "<br>"]
		

	
	
	set cost_status_change_url "/intranet-cust-fud/cost_status?return_url=$this_url&invoice_id=$invoice_id&cost_status_id="
	#set coststatuslinks "<a href=$cost_status_change_url$internal_reminder>InternMahnen</a><br><a href=$cost_status_change_url$outstanding>Offen</a>"
	set coststatuslinks "<a href=$cost_status_change_url$outstanding>set2Outstanding</a>"
	
	
	
	if { $noautoreminder == "t" } { 
		set noautoinkasso "<font color=red>Kein autom. Inkasso</font><br>
						<a href=$noautoinkasso_url$company_id&ft=f>set2 AutoInkasso</a>" 
	} else {
		set noautoinkasso "AutoInkasso<br>
						<a href=$noautoinkasso_url$company_id&ft=t>set2 NoAutoInkasso</a>"
	}
	
	set new_contact "<a href=$newcontact_url${company_id}+1300  target=_blank>NewContact</a>"



	set reminder "<b><a href=$invoice_reminder_url$cost_id>email reminder</a></b>"
	set reminder_letter "<b><a href=$invoice_postreminder_url$cost_id>post reminder</a></b>"

	
	# 3804 			Outstanding
	# 3810 			Paid
	# 11000201		Reminder_1_reminderLetter
	# 11000395		INTERN_zuMahnen
# 	11000325 			CollectionAgency
# 	11000349 			written_off
# 	11000366			insolvency
	
	set user_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
	if {$user_admin_p == 1 } { 
		set coststatus "3804 3810 11000201 11000395 11000325 11000349 11000366 11000426 11000503"
	} else { set coststatus "11000426" }
	set admin_links [list]
	foreach opencoststatus_id $coststatus {
		if { $cost_status_id != $opencoststatus_id } {
			set adminstatus [im_category_from_id $opencoststatus_id]
			lappend admin_links "<a href=$cost_status_change_url$opencoststatus_id>$adminstatus</a>"
			}
	}
	
	
	
	set compstatus "46 48 11000346 11000347"
	set admincomp_links  [list]
	lappend admincomp_links [im_category_from_id $company_status_id]
	foreach compstatus_id $compstatus {
		if { $company_status_id != $compstatus_id } {
			set admincompstatus [im_category_from_id $compstatus_id]
			lappend admincomp_links "<a href=$compstatus_url$company_id&company_status_id=$compstatus_id>$admincompstatus</a>"
			}
	}
	
	
	
	switch $cost_status_id {
	3804		{
			set coststatuslinks "<a href=$cost_status_change_url$paid>set2Paid</a>"
			}
	11000395 {
			set cost_status "<font color=red>$cost_status</font>" 
			set coststatuslinks "<a href=$cost_status_change_url$outstanding>set2Outstanding</a>
						  <br><a href=$cost_status_change_url$paid>set2Paid</a>"
			}
	11000201 { set cost_status "<font color=goldenrod>$cost_status</font>" 
			set coststatuslinks "<a href=$cost_status_change_url$outstanding>set2Outstanding</a>
						  <br><a href=$cost_status_change_url$paid>set2Paid</a>"
			}
	
	}

	
	
	
	
	
	
	
	
	
	# 	if { $cost_status_id == 11000395 } {
# 			set cost_status "<font color=red>$cost_status</font>" 
# 			set coststatuslinks "InternMahnen<br><a href=$cost_status_change_url$outstanding>Offen</a>"
# 			}
#	if { $cost_status_id ==	3804 } { set coststatuslinks "<a href=$cost_status_change_url$internal_reminder>InternMahnen</a><br>Offen"	}
#	if { $cost_status_id ==	11000201 } { set cost_status "<font color=goldenrod>$cost_status</font>" }
	
	if { $keyacc_id ne $project_lead_id } {set keyacc "<font color=red>$keyacc</font>" } else {set keyacc ""}
	
	
	
	# Restrict the length of the project_name to max.
	# 40 characters. (New!)
	set project_name [string_truncate -len 40 $project_name]
	set customer_name_short [string_truncate -len 40 $customer_name]
	im_report_display_footer \
	    -group_def $report_def \
	    -footer_array_list $footer_array_list \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class

	im_report_update_counters -counters $counters

#	# Calculated Variables
#	set po_per_quote_perc "undef"
#	if {[expr $quote_subtotal+0] != 0} {
#	  set po_per_quote_perc [expr int(10000.0 * $po_subtotal / $quote_subtotal) / 100.0]
#	  set po_per_quote_perc "$po_per_quote_perc %"
#	}
#	set gross_profit [expr $invoice_subtotal - $bill_subtotal]

	set last_value_list [im_report_render_header \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	set footer_array_list [im_report_render_footer \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	incr counter
}

im_report_display_footer \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1


# Write out the HTMl to close the main report table
# and write out the page footer.
#
ns_write "
	</table>
	[im_footer]
"

