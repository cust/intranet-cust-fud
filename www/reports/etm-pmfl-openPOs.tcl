# ETM reports

# ETM FL-Bills doorpage
ad_page_contract {
    ETM - FL-Bills anlegen.


} {
	 { company_id:integer 0 }
}


# ------------------------------------------------------------
# Security 
#
# What is the "label" of the Menu Item linking to this report?
set menu_label "etm-pmfl-auswahlBilling"

# Get the current user and make sure that he or she is
# logged in. Registration is handeled transparently - 
# the user is redirected to this URL after registration 
# if he wasn't logged in.
set current_user_id [ad_maybe_redirect_for_registration]

# Determine whether the current_user has read permissions. 
set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

# For testing - set manually
set read_p "t"

# Error message
if {![string equal "t" $read_p]} {
    set message "You don't have the necessary permissions to view this page"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
}



# ------------------------------------------------------------
# Page Title, Bread Crums and Help
#
set page_title "FL - Bills Doorpage"
set context_bar [im_context_bar $page_title]
set help_text "Bitte mit Ctrl F Browsersuche nutzen und nicht scrollen :-)"

# ------------------------------------------------------------
# Default Values and Constants
#
# Level of Details
set level_of_detail 1

# Links
set company_url "/intranet/companies/view?company_id="
set company_edit_url "/intranet/companies/new?company_id="
set po2bill_source_url "/intranet-invoices/new-merge-invoiceselect?target_cost_type_id=3704&source_cost_type_id=3706&cost_status_id=3804&customer_id=8720&company_id="


# Report SQL
set report_sql "
select  prov.*,
	o.address_line1,
	o.address_line2,
	o.address_city,
	o.address_postal_code,
	o.address_country_code,
	cc.country_name
from
	im_companies prov,
	im_offices o,
	country_codes cc
	
where  
	prov.main_office_id = o.office_id
	and o.address_country_code = cc.iso
	and prov.company_type_id = '58'

order by 
	company_name asc
	
"





# Global Header Line
set header0 {
	"FL-Name" 
	"Adresse"
	"Bank"
	"FL-Rg eingeben"
}

set report_def [list \
    group_by company_id \
    header {
	"<b><a href=$company_url$company_id>$company_name</a>"
	 "$address_line2 <br> $address_line1 <br> $address_postal_code  $address_city <br> $country_name"
	"<strong>Bankdaten:</strong> <br> Bankname: $bank <br> IBAN: $bank_iban <br> Swift: $bank_swift <br> Bez. E-Mail: $payment_email <br> Acc-No: $bank_accno <br> Sort Code: $bank_sort_code "
	"<a href='$po2bill_source_url$company_id'><font color=red><strong>FL-Rg eingeben</strong></font></a>" 

    } \
    content {} \
    footer {} \
]

# Level of Details
# Will be used more extensively with groupings
#
set level_of_detail 1

# Diplay report lines using CSS style "rowodd"
set class "rowodd"


# ------------------------------------------------------------
# Counters
#
# Counters are used to present totals and subtotals.
# This report does not use counters, so the "counters"
# variable is set to an empty list.
#
set counters [list]

# Write out the report header
#
ad_return_top_of_page "
	<!-- Write out the logo stuff and the user info -->
	[im_header]
	<!-- Write out the main system navigation bar -->
	[im_navbar]
	<!-- Write out the help text. -->
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	  <td width='50%'>
		<!-- 'Filters' -->
	  </td>
	  <td align=center width='50%'>
		<!-- Help Text -->
		<table cellspacing=2 width='90%'>
		<tr>
		  <td>$help_text</td>
		</tr>
		</table>
	  </td>
	</tr>
	</table>
	
	<!-- Here starts the main report table -->
	<table border=0 cellspacing=1 cellpadding=1>
"


# ------------------------------------------------------------
# Render Report Body

# The following report loop is "magic", that means that 
# you don't have to fully understand what it does.
# It loops through all the lines of the SQL statement,
# calls the Report Engine library routines and formats 
# the report according to the report_def definition.

# Start <magic>

set footer_array_list [list]
set last_value_list [list]

im_report_render_row \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"

db_foreach sql $report_sql {
	im_report_display_footer \
	    -group_def $report_def \
	    -footer_array_list $footer_array_list \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	set last_value_list [im_report_render_header \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]
	set footer_array_list [im_report_render_footer \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]




# kontaktdaten pruefen ...weiter mit sql: office ueber rels oder cost.office_id danach $kontakt in bank einbaun
if { "" == $address_line1 } {
			set address_line1 "<a href='$company_edit_url$company_id'><font color=red><strong>Str fehlt</strong></font></a>"
		} 
if { "" == $address_city } {
			set address_city "<a href='$company_edit_url$company_id'><font color=red><strong>Stadt fehlt</strong></font></a>"
		} 

if { "" == $address_postal_code } {
			set address_postal_code "<a href='$company_edit_url$company_id'><font color=red><strong>PLZ fehlt</strong></font></a>"
		} 
if { "" == $address_country_code } {
			set country_name "<a href='$company_edit_url$company_id'><font color=red><strong>Land fehlt</strong></font></a>"
		} 


set address_block "<td row_class='roweven'>$address_line2 <br> $address_line1 <br> $address_postal_code  $address_city <br> $country_name</td> "


#bank-daten pruefen
	if { "" != $bank_iban && "" != $bank_swift || "" != $bank &&  "" != $bank_accno && "" != $bank_sort_code || "" != $bank && "" != $payment_email } {
			set bankblock "<strong>Bankdaten:</strong> <br> Bankname: $bank <br> IBAN: $bank_iban <br> Swift: $bank_swift <br> Bez. E-Mail: $payment_email <br> Acc-No: $bank_accno <br> Sort Code: $bank_sort_code "
		} else {
				set bankblock "<a href='$company_edit_url$company_id'><font color=red><strong>Bankdaten aktualiseren</strong></font></a>"
		  }


}

im_report_display_footer \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

# End </magic>


# ------------------------------------------------------------
# Render Report Footer

# Write out the HTMl to close the main report table
# and write out the page footer.
#
ns_write "
	</table>
	[im_footer]
"
