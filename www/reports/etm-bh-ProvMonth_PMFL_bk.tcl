# /packages/intranet-cust-fud/reports/etm-bh-ProvMonth_PMFL.tcl
#
# Copyright (c) 2003-2006 ]project-open[
#
# All rights reserved. 
# Please see http://www.project-open.com/ for licensing.



# ------------------------------------------------------------
# Page Contract 
#
# FUD PM-Umsatz-REPORT basiert auf report-tut05
#

ad_page_contract {
    FUD Monthly Report per PM
    This reports lists all projects in a time interval of months
    It is one of the easiest reports imaginable...

    @param year Year (YYYY format) 
    @param month Month (MM format)
    
} {
    { year "" }
    { month "" }
   
    { start_date "" }
    { end_date "" }
    { level_of_detail:integer 3 }
    { customer_id:integer 0 }
   
}



# Set "label" of the Menu Item linking to this report
set menu_label "etm-bh-ProvMonth_PMFL"


# Get the current user and make sure that he or she is
# logged in. 
set current_user_id [ad_maybe_redirect_for_registration]


# is current user pmfl?

set group_id 27888


if {![im_user_group_member_p $current_user_id $group_id] } {
	if {![im_is_user_site_wide_or_intranet_admin $current_user_id]} {
    set message "nur fuers PM"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
	}
}

# Determine whether the current_user has read permissions. 
set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

# Write out an error message if the current user doesn't
# have read permissions and abort the execution of the
# current screen.
if {![string equal "t" $read_p]} {
    set message "You don't have the necessary permissions to view this page"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
}




# ------------------------------------------------------------
# Set the default year and month

set days_in_past 0

db_1row todays_date "
select
	to_char(sysdate::date - :days_in_past::integer, 'YYYY') as todays_year,
	to_char(sysdate::date - :days_in_past::integer, 'MM') as todays_month,
	to_char(sysdate::date - :days_in_past::integer, 'DD') as todays_day
from dual
"

if {"" == $year} { 
    set year "$todays_year"
}

if {"" == $month} { 
    set month "$todays_month"
}

set yearmonth "$year$month"

# ------------------------------------------------------------
# Check Parameters

if {"" != $year && ![regexp {^[2][0][0-3][0-9]$} $year]} {
    ad_return_complaint 1 "Year doesn't have the right format.<br>
    Current value: '$year'<br>
    Expected format: 'YYYY'"
}

if {"" != $month && ![regexp {^[0-1][0-9]$} $month]} {
    ad_return_complaint 1 "Month doesn't have the right format.<br>
    Current value: '$month'<br>
    Expected format: 'MM'"
}



# ------------------------------------------------------------
# Page Title, help text

set page_title "Provision nach Monaten"
set context_bar [im_context_bar $page_title]
set help_text "
	<strong>Provision</strong><br>
	
	<br>
	<br>
	<br>
	<br>
	
"


# ------------------------------------------------------------
# Default Values and Constants
#


# Default report line formatting - alternating between the
# CSS styles "roweven" (grey) and "rowodd" (lighter grey).
#
set rowclass(0) "roweven"
set rowclass(1) "rowodd"

#
set currency_format "999,999,999.09"
set date_format "YYYY-MM-DD"

#Default currency
set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]

# URLs
set this_url [export_vars -base "/intranet-cust-fud/reports/etm-bh-ProvMonth_PMFL.tcl" {year month} ]



# set level of details {2 "Months only" 3 "Months+Projects"} 
set levels {2 "Months only"}

#Default currency
set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]

##### pmfl beteiligungs-satz################################
# 
# 
#
set commission_perc_group1 "0.0000"
set commission_perc_group2 "0.002"
set commission_perc_group3 "0.001"
set commission_perc_group4 "0.00001"
set commission_perc_group5 "0.0011"
set commission_perc_group6 "0.000195"


set commission_perc_group1_members [list  "2381497"]
# viktoria: 2381497
if {$yearmonth > "201807"} {
	set commission_perc_group2_members [list "175824" "1883409"]
   } else { set commission_perc_group2_members [list "175824" ]
   }
# joaquin:175824,# kathrin.w: 1883409 ab 2018-08
set commission_perc_group3_members [list "1520554" "2313402"]
# sophie:1520554 dmitrij2313402

if {$yearmonth < "202010"} {
	set commission_perc_group4_members [list "3634326"]
   } else { set commission_perc_group4_members [list "3634326"]
   }
# sarah: 3634326


if {$yearmonth > "201807"} {
	set commission_perc_group5_members [list ""]
   } else { set commission_perc_group5_members [list "1883409"]
   }
# kathrin.w: 1883409 bis 2018-07, 



# christina schreibt keine POs daher spezi
if {$yearmonth < "202004"} {
	set commission_perc_group6_members [list ""]
   } else { set commission_perc_group6_members [list "3439597"]
   }



############################################################
# sarah bekommt weniger da nur 4h 
if {3634326 == $current_user_id} { set commission_fl_perc "0.005"
} else { set commission_fl_perc "0.016" }


############################################################




# ------------------------------------------------------------
# Conditional SQL Where-Clause
#



##### anfang beteiligung bei verschiedenen usern arbeitsbeginn ###############
#
set where_start "and p.start_date > '2017.08.01'\n"
set where_start_po "and acs.last_modified > '2017.08.01'\n"


if { 3439597 == $current_user_id } {
  set where_start "and p.start_date > '2020.04.01'\n"
  set where_start_po "and acs.last_modified > '2020.04.01'\n"
}

####

set criteria [list]

##if {[info exists project_manager_id]} {
##    lappend criteria "p.project_lead_id = :project_manager_id"
##}

if {![im_is_user_site_wide_or_intranet_admin $current_user_id]} {
	lappend criteria "creation_user = :current_user_id"
}


set where_pm [join $criteria " and\n            "]
if { ![empty_string_p $where_pm] } {
    set where_pm " and $where_pm"
}





# ------------------------------------------------------------
# get values
#

set invoices_total 0.0

db_0or1row invoices_total "
	SELECT sum(p.cost_invoices_cache)  as invoices_total
	FROM   im_projects p
	WHERE  (SELECT EXTRACT (YEAR FROM p.start_date)) = :year
		  and (SELECT EXTRACT (MONTH FROM p.start_date)) = :month
		  and p.project_status_id not in ('81','82', '83')
		  $where_start
"
if {$invoices_total eq "" } {set invoices_total 0.0}
set invoices_total [expr {double($invoices_total) * 1.} ]


set flcount_total 0.0
set flcount_total [db_string flcount_total "
select count(provider_id)
from im_costs c,	 
	acs_objects acs
where 
	c.cost_id = acs.object_id 
	and (SELECT EXTRACT (YEAR FROM acs.creation_date)) = :year
   	and (SELECT EXTRACT (MONTH FROM  acs.creation_date)) = :month
	and cost_type_id = 3706
	$where_start_po
"]

if {$flcount_total eq "" } {set flcount_total 0.0}
set flcount_total [expr {double($flcount_total) * 1.} ]

set flcount_pm 0.0
set commission_fl 0.0

set invoices_total50 [expr $invoices_total / 2.0]


# ------------------------------------------------------------
# Report Definition
#
# Reports are defined in a "declarative" style. The definition
# consists of a number of fields for header, lines and footer.




if { 3439597 == $current_user_id } {
set report_sql "
select  1 as flcount_pm, 
	:current_user_id as pm_id, 
	im_name_from_user_id (:current_user_id) as pm, 
	:month as month
"
} elseif {[im_is_user_site_wide_or_intranet_admin $current_user_id]} {
set report_sql "
(select count(provider_id) + 1 as flcount_pm, 
	acs.creation_user as pm_id, 
	im_name_from_user_id (creation_user) as pm, 
	(SELECT EXTRACT (MONTH FROM  acs.creation_date)) as month
from im_costs c,	 
	acs_objects acs
where 	c.cost_id = acs.object_id 
 	and (SELECT EXTRACT (YEAR FROM acs.creation_date)) = :year
    	and (SELECT EXTRACT (MONTH FROM  acs.creation_date)) = :month
	and cost_type_id = 3706
 	$where_pm
 	$where_start_po
	group by creation_user, month
	order by flcount_pm desc )
union (select  1 as flcount_pm, 
	3439597 as pm_id, 
	im_name_from_user_id (3439597) as pm, 
	:month as month
)
"
} else {
set report_sql "
select count(provider_id) + 1 as flcount_pm, 
	acs.creation_user as pm_id, 
	im_name_from_user_id (creation_user) as pm, 
	(SELECT EXTRACT (MONTH FROM  acs.creation_date)) as month
from im_costs c,	 
	acs_objects acs
where 	c.cost_id = acs.object_id 
 	and (SELECT EXTRACT (YEAR FROM acs.creation_date)) = :year
    	and (SELECT EXTRACT (MONTH FROM  acs.creation_date)) = :month
	and cost_type_id = 3706
 	$where_pm
 	$where_start_po
	group by creation_user, month
	order by flcount_pm desc
"
}
# if { 3439597 == $current_user_id } {
#   set flcount_pm 1
#   set pm_id $current_user_id
#   set pm "Christina Bützer"
#   set month $month
# }

# Global Header Line
set header0 {"Jahr-Monat" "PM" "Prov" }
# Report

set report_def [list \
    group_by month \
    header {
	<b>$year-$month</b>

    } \
		content [list \
			group_by pm_id \
			header { } \
		content [list \
			header {
					""
					"<nobr><i>$pm </i></nobr>"
					"<nobr><b>$commission $default_currency</b></nobr>"
				} \
		    content {} \
	    ] \
            footer { } \
    ] \
    footer { } \
]

# Global Footer Line
set footer0 {}


# admin-report

if {[im_is_user_site_wide_or_intranet_admin $current_user_id]} {
	
	set header0 {"Jahr-Monat" "PM" "comTotal" "comBase" "comFL" "inv50" "flcountpm" "flcount_total" }

	set report_def [list \
	group_by month \
	header {
		<b>$year-$month</b>
		
	} \
		content [list \
			group_by pm_id \
			header { } \
		content [list \
			header {
					""
					"<nobr><i>$pm </i> ($commission_perc)</nobr>"
					"<nobr><b>$commission $default_currency</b></nobr>"
					"<nobr><i>$commissionbase $default_currency</i></nobr>"
					"<nobr>$commission_fl $default_currency</nobr>"
					"($invoices_total50)"
					"$flcount_pm ($flpmpercentage)"
					" $flcount_total "
					} \
			content {} \
		] \
			footer { } \
	] \
	footer { } \
	]


	set footer0 { }

}





# ------------------------------------------------------------
# Start Formatting the HTML Page Contents
#
# Writing out a report can take minutes and hours, so we are
# writing out the HTML contents incrementally to the HTTP 
# connection, allowing the user to read the first lines of the
# report (in particular the help_text) while the rest of the
# report is still being calculated.
#

# Write out the report header with Parameters
# We use simple "input" type of fields for start_date 
# and end_date with default values coming from the input 
# parameters (the "value='...' section).
#

#
ad_return_top_of_page "
	[im_header]
	[im_navbar]
	
	<table cellspacing=0 cellpadding=0 border=0>
	 <tr valign=top>
	  <td width='30%'>
		<!-- 'Filters' - Show the Report parameters -->
		<form>
			<table cellspacing=2>
				<tr class=rowtitle>
				<td class=rowtitle colspan=2 align=center>Filters</td>
				</tr>
				<tr>
				<td><nobr>Jahr (YYYY):</nobr></td>
				<td><input type=text name=year value='$year'></td>
				</tr>
				<tr>
				<td><nobr>Monat (MM):</nobr></td>
				<td><input type=text name=month value='$month'></td>
				</tr>
				<tr>
				<td</td>
				<td><input type=submit value='Submit'></td>
				</tr>
			</table>
		</form>
       </td>
	  <td align=center>
	
		<table cellspacing=2 width='90%'>
		<tr>
		  <td>$help_text</td>
		</tr>
		</table>
	  </td>
	 </tr>
	</table>
	
	<!-- Here starts the main report table -->
	<table border=0 cellspacing=2 cellpadding=1>
"

set footer_array_list [list]
set last_value_list [list]

im_report_render_row \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"
    
set class ""

 ns_log Notice "invoices_total: $invoices_total \n commission_perc_group6: $commission_perc_group6"
# christina bekommt spezi
if { 3439597 == $current_user_id } {
	set commission [expr (double($invoices_total * $commission_perc_group6 ))]
}



db_foreach sql $report_sql {
	im_report_display_footer \
	    -group_def $report_def \
	    -footer_array_list $footer_array_list \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	
# find out users commission percentage 
set commission_perc 0

if { $pm_id in $commission_perc_group1_members } { 
	set commission_perc $commission_perc_group1 
	} elseif { $pm_id in $commission_perc_group2_members } { 
	set commission_perc $commission_perc_group2 
	} elseif { $pm_id in $commission_perc_group3_members } { 
	set commission_perc $commission_perc_group3 
	} elseif { $pm_id in $commission_perc_group4_members } { 
	set commission_perc $commission_perc_group4 
	} elseif { $pm_id in $commission_perc_group5_members } { 
	set commission_perc $commission_perc_group5 
	} elseif { $pm_id in $commission_perc_group6_members } { 
	set commission_perc $commission_perc_group6 
	} 




set commissionbase [expr double($invoices_total50) * double($commission_perc)]
if { 0 != $flcount_total } {
	set commission_fl [expr (double($flcount_pm) / double($flcount_total)) * double($invoices_total50)  * double($commission_fl_perc)]
    } else { set commission_fl 0 }

	
# sarah 50% da nur 4h
if { 3634326 == $current_user_id } { 
	set $commission_fl [ expr double($commission_fl) * 0.3 ]
} 	
	
set commission [expr double($commissionbase) + double($commission_fl)]
set flpmpercentage  [expr (double($flcount_pm) / double($flcount_total))]


# mareike 552979 ausschliessen 
if { 552979 == $current_user_id } { 
	set commission 0.0
}
# sabrina stand 09.10.2017
if { 1049125 == $current_user_id } { 
	set commission 61.60 
} 	

 ns_log Notice "inlooooop: \n invoices_total: $invoices_total \n commission_perc_group6: $commission_perc_group6"

# christina bekommt spezi
if { 3439597 == $current_user_id } {
	set commission [ expr (double($invoices_total * $commission_perc_group6 )) ]
}

# sarah 50% da nur 4h
# if { 3634326 == $current_user_id } { 
# 	set commission [ expr double($commission) * 0.5 ]
# } 

 ns_log Notice "commission: $commission \n PM: $ current_user $current_user_id "


set commission [format "%.2f" $commission]
set commission_perc [format "%.4f" $commission_perc]
set commissionbase [format "%.2f" $commissionbase]
set commission_fl [format "%.2f" $commission_fl]
set flcount_total [format "%.0f" $flcount_total]
set flcount_pm [format "%.0f" $flcount_pm]
set flpmpercentage [format "%.2f" $flpmpercentage]
set invoices_total50 [format "%.0f" $invoices_total50]


	set last_value_list [im_report_render_header \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	set footer_array_list [im_report_render_footer \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

}




im_report_display_footer \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1


# Write out the HTMl to close the main report table
# and write out the page footer.
#
ns_write "
	</table>
	[im_footer]
"

