# /packages/intranet-reporting-tutorial/www/projects-05.tcl
#
# Copyright (c) 2003-2006 ]project-open[
#
# All rights reserved. 
# Please see http://www.project-open.com/ for licensing.




# ------------------------------------------------------------
# Page Contract 
#
# ZIS PAN FUD Beauftragung
#

ad_page_contract {
    ZIS PAN FUD Beauftragung.

    @param start_date Start date (YYYY-MM-DD format) 
    @param end_date End date (YYYY-MM-DD format) 
} {
    { start_date "2014-01-01" }
    { end_date "2099-12-31" }
    { level_of_detail:integer 3 }
    { agentur_id:integer 0 }

}



# What is the "label" of the Menu Item linking to this report?
set menu_label "etm-zispan-fud_pob"

# Get the current user and make sure that he or she is
# logged in. Registration is handeled transparently - 
# the user is redirected to this URL after registration 
# if he wasn't logged in.
set current_user_id [ad_maybe_redirect_for_registration]

# Determine whether the current_user has read permissions. 
# "db_string" takes a name as the first argument 
# ("report_perms") and then executes the SQL statement in 
# the second argument. 
# It returns an error if there is more then one result row.
# im_object_permission_p is a PlPg/SQL procedure that is 
# defined as part of ]project-open[.
set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

# For testing - set manually
#set read_p "t"

# Write out an error message if the current user doesn't
# have read permissions and abort the execution of the
# current screen.
if {![string equal "t" $read_p]} {
    set message "You don't have the necessary permissions to view this page"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
}

# defaults
set days_in_past 0

db_1row todays_date "
select
	to_char(sysdate::date - :days_in_past::integer, 'YYYY') as todays_year,
	to_char(sysdate::date - :days_in_past::integer, 'MM') as todays_month,
	to_char(sysdate::date - :days_in_past::integer, 'DD') as todays_day
from dual
"


# start date setzen

if {"" == $start_date} { 
	set start_date "$todays_year-01-01"
	}


# ------------------------------------------------------------
# Check Parameters (New!)
#
# Check that start_date and end_date have correct format.
# We are using a regular expression check here for convenience.

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $end_date]} {
    ad_return_complaint 1 "End Date doesn't have the right format.<br>
    Current value: '$end_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

# Maxlevel is 3. 
if {$level_of_detail != 3} { set level_of_detail 3 }




# ------------------------------------------------------------
# Page Title, Bread Crums and Help
#
# We always need a "page_title".
# The "context_bar" defines the "bread crums" at the top of the
# page that allow a user to return to the home page and to
# navigate the site.
# Every reports should contain a "help_text" that explains in
# detail what exactly is shown. Reports can get very messy and
# it can become very difficult to interpret the data shown.
#

set page_title "panzis2fud pm invoices"
set context_bar [im_context_bar $page_title]
set help_text ""


# ------------------------------------------------------------
# Default Values and Constants
#
# In this section we define constants and default variables
# that are used in the sections further below.
#

# Default report line formatting - alternating between the
# CSS styles "roweven" (grey) and "rowodd" (lighter grey).
#
set rowclass(0) "roweven"
set rowclass(1) "rowodd"

# Variable formatting - Default formatting is quite ugly
# normally. In the future we will include locale specific
# formatting. 
#
set currency_format "999,999,999.09"
set date_format "YYYY-MM-DD"

# Set URLs on how to get to other parts of the system
# for convenience. (New!)
# This_url includes the parameters passed on to this report.
#
set company_url "/intranet/companies/view?company_id="
set project_url "/intranet/projects/view?project_id="
set invoice_url "/intranet-invoices/view?invoice_id="
set user_url "/intranet/users/view?user_id="
set this_url [export_vars -base "/intranet-cust-fud/reports/etm-bh_panzis-fud-inv" {start_date end_date} ]
set pob_url "/intranet-cust-fud/reports/etm-zispan-fud_pob-New"
# Level of Details
# Determines the LoD of the grouping to be displayed
#
set levels {2 "Agent" 3 "Agent+Rg"} 



# ------------------------------------------------------------
# Report SQL - This SQL statement defines the raw data 
# that are to be shown.


# This version of the select query uses a join with 
# im_companies on (p.company_id = company.company_id).
# This join is possible, because the p.company_id field
# is a non-null field constraint via referential integrity
# to the im_companies table.
# In the absence of such strong integrity contraints you
# will have to use "LEFT OUTER JOIN"s instead. (New!)
#
set report_sql "
select 	c.cost_id,
	c.cost_name,
	c.customer_id,
	im_name_from_id(c.customer_id) as agent,
	to_char(c.effective_date, 'YYYY-MM-DD') as datum,
	to_char(c.effective_date, 'YYYY') as year,
	im_name_from_id(c.project_id),	
	c.amount,
	to_char((round(c.amount::numeric,2)), '9999999D99')  as amount_net,
	c.currency as amount_cur,
	to_char((round(c.paid_amount::numeric,2)), '9999999D99')  amount_paid,
	c.paid_currency paid_cur,
	c.vat,
	round(((c.amount * c.vat / 100))::numeric,2)  as amount_vat,
	to_char(((c.amount * (1+(c.vat/100)))), '9999999D99')   as amount_gross,
	im_name_from_id(c.cost_status_id) as status,
	*
from 	im_costs c 
where 	c.customer_id in ('788621' , '741973')
	and c.cost_type_id in ('3700' , '3725')
	and c.effective_date > :start_date
	and c.effective_date < :end_date
order by c.customer_id,year,c.cost_name
"




# Global Header Line
set header0 {
	"Agentur" 
	"Name"
	""	
	"Datum"
	"Betrag netto"
	"MwSt"
	"Betrag brutto"
	"bezahlt"	
	"Status"
}

# The entries in this list include <a HREF=...> tags
# in order to link the entries to the rest of the system (New!)
#
set report_def [list \
    group_by customer_id \
    header {
	"#colspan=11 <b>$agent<br>" 
    } \
        content [list \
            group_by year \
            header {$year } \
	    content [list \
		    header {
			""
			"<a href=$invoice_url$cost_nr>$cost_name</a>"
			""
			"$datum"
			"$amount_net"
			"$amount_vat"
			"$amount_gross"
			"$amount_paid "
			"$status"
		    } \
		    content {} \
	    ] \
            footer {} \
    ] \
    footer {} \
]




# Global Footer Line
set footer0 {
							"" 
							""
							""
							"<b>$agent Total: </b>"	
							"<b>EUR $amount_net_total</b>"
							"<b>EUR $amount_vat_total</b"
							"<b>EUR $amount_gross_total</b>"
							"<b>EUR $amount_paid_total</b"
							""
}




#
# Subtotal Counters (per project)
#
set invoice_subtotal_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_subtotal \
        reset \$customer_id \
        expr "\$cost_invoices_cache+0" \
]

set quote_subtotal_counter [list \
        pretty_name "Quote Amount" \
        var quote_subtotal \
        reset \$customer_id \
        expr "\$cost_quotes_cache+0" \
]

set delnote_subtotal_counter [list \
        pretty_name "Delnote Amount" \
        var delnote_subtotal \
        reset \$customer_id \
        expr "\$cost_delivery_notes_cache+0" \
]

set bill_subtotal_counter [list \
        pretty_name "Bill Amount" \
        var bill_subtotal \
        reset \$customer_id \
        expr "\$cost_bills_cache+0" \
]

set expense_subtotal_counter [list \
        pretty_name "Expense Amount" \
        var expense_subtotal \
        reset \$customer_id \
        expr "\$cost_expense_logged_cache+0" \
]

set po_subtotal_counter [list \
        pretty_name "Po Amount" \
        var po_subtotal \
        reset \$customer_id \
        expr "\$cost_purchase_orders_cache+0" \
]

#
# Grand Total Counters
#
set amount_net_total_counter [list \
        pretty_name "Net Amount" \
        var amount_net_total \
        reset 0 \
        expr "\$amount_net+0" \
]

set amount_vat_total_counter [list \
        pretty_name "VAT Amount" \
        var amount_vat_total \
        reset 0 \
        expr "\$vat_amount+0" \
]

set amount_gross_total_counter [list \
        pretty_name "Amount gross" \
        var amount_gross_total \
        reset 0 \
        expr "\$amount_gross+0" \
]

set amount_paid_total_counter [list \
        pretty_name "Amount paid" \
        var amount_paid_total \
        reset 0 \
        expr "\$amount_paid+0" \
]

set counters [list \
	$amount_net_total_counter \
	$amount_vat_total_counter \
	$amount_gross_total_counter \
	$amount_paid_total_counter \
]

# Set the values to 0 as default
set amount_net_total 0
set amount_vat_total 0
set amount_gross_total 0
set amount_paid_total 0



# ------------------------------------------------------------
# Start Formatting the HTML Page Contents
#


#
ad_return_top_of_page "
	[im_header]
	[im_navbar]
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	  <td width='30%'>
		<!-- 'Filters' - Show the Report parameters -->
		<form>
		<table cellspacing=2>
		<tr class=rowtitle>
		  <td class=rowtitle colspan=2 align=center>Filters</td>
		</tr>
		<tr>
		  <td>Level of<br>Details</td>
		  <td>
		    [im_select -translate_p 0 level_of_detail $levels $level_of_detail]
		  </td>
		</tr>
		<tr>
		  <td><nobr>Start Date:</nobr></td>
		  <td><input type=text name=start_date value='$start_date'></td>
		</tr>
		<tr>
		  <td>End Date:</td>
		  <td><input type=text name=end_date value='$end_date'></td>
		</tr>
		<tr>
		  <td</td>
		  <td><input type=submit value='Submit'></td>
		</tr>
		</table>
		</form>
	  </td>
	  <td align=center>
		<table cellspacing=2 width='90%'>
		<tr>
		  <td>ZISPAN FUD-Rgen 
		<br><br><br><br><br><br><br><br><br><br><br><br><br>
		<a href=$pob_url> ZIS/Pan Auftraege -> FUED </a></td>
		</tr>
		</table>
	  </td>
	</tr>
	</table>
	
	<!-- Here starts the main report table -->
	<table border=0 cellspacing=1 cellpadding=1>
"

# report loop 

set footer_array_list [list]
set last_value_list [list]

im_report_render_row \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"

set counter 0
set class ""
db_foreach sql $report_sql {

	# Select either "roweven" or "rowodd" from
	# a "hash", depending on the value of "counter".
	# You need explicite evaluation ("expre") in TCL
	# to calculate arithmetic expressions. 
	set class $rowclass([expr $counter % 2])



	im_report_display_footer \
	    -group_def $report_def \
	    -footer_array_list $footer_array_list \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class

	im_report_update_counters -counters $counters



	set last_value_list [im_report_render_header \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	set footer_array_list [im_report_render_footer \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	incr counter
}

im_report_display_footer \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1


# Write out the HTMl to close the main report table
# and write out the page footer.
#
ns_write "
	</table>
	[im_footer]
"

