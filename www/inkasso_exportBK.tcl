# /package/intranet-hmd/download-fbasc-all.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download inkasso preps as  CSV File 
    @author etm 2019
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set current_date [ns_localsqltimestamp]
set current_date_formatted [db_string curdat "select to_char(current_date, 'YYYYMMDD')" -default ""]

# ---------------------------------------------------------------
# Customer invoices
# ---------------------------------------------------------------

set interco_options [db_list_of_lists interco "select c.company_name, 
										lower(c.company_name) as agent,
											c.company_id 
										from im_companies c 
										where c.company_status_id in (select * from im_sub_categories(46)) 
											and c.company_type_id = 11000000 
											order by lower(c.company_name)"]

set zip_dir [ns_tmpnam]
file mkdir $zip_dir
set file_created_p 0
set failed_invoice_ids [list]

foreach interco_option $interco_options {
    set company_id [lindex $interco_option 2]
    set company_name [lindex $interco_option 0]
    set agent [lindex $interco_option 1]
    set inkasso_prep  11000426

        # ---------------------------------------------------------------
        # Get the list of invoices
        # ---------------------------------------------------------------
        set invoice_ids [db_list export_invoices "select c.cost_id 
										from im_costs c,
											im_projects p
										where p.project_id = c.project_id 
										     and substring(im_name_from_id(template_id),1,3) = :agent
											and p.interco_company_id = :company_id 
											and cost_status_id = :inkasso_prep "]

        set csv_contents [list]
      
        set csv_values [list "Kunden-ID"]; # 0 Kunden-ID
        lappend csv_values "Belegnummer" ; # 1 Belegnummer
        lappend csv_values "Rechnungs-Datum" ; # 2 Rechnungs-Datum
        lappend csv_values "Anrede" ; # 3 Anrede        
        lappend csv_values "Vorname" ; # 4 Vorname        
        lappend csv_values "Nachname oder Firmenname" ; # 5 Nachname oder Firmenname
        lappend csv_values "Geburtsdatum" ; # 6 Geburtsdatum
        lappend csv_values "Anrede" ; # 7 Anrede
        lappend csv_values "Ansprechpartner-Vorname" ; # 8 Ansprechpartner-Vorname
        lappend csv_values "Ansprechpartner-Nachname" ; # 9 Ansprechpartner-Nachname
        lappend csv_values "Strasse" ; # 10 Strasse
        lappend csv_values "LKZ" ; # 11 LKZ
        lappend csv_values "PLZ" ; # 12 PLZ
        lappend csv_values "Ort" ; # 13 Ort
        lappend csv_values "Land" ; # 14 Land
        lappend csv_values "Telefon" ; # 15 Telefon
        lappend csv_values "" ; # 16 Fax
        lappend csv_values "" ; # 17 Mobiltelefon
        lappend csv_values "" ; # 18 Email-Adresse
        lappend csv_values "Hauptforderung" ; # 19 Hauptforderung
        lappend csv_values "Währung" ; # 20 Währung
        lappend csv_values "Nebenforderung" ; # 21 Nebenforderung
        lappend csv_values "Bemerkung" ; # 22 Bemerkung
        
	
		set csv_line [::csv::join $csv_values ";"]
 
          lappend csv_contents $csv_line
        
        
        foreach invoice_id $invoice_ids {
            
            # set carriage return and linefeed
            set content "[intranet_cust_fud::inkasso_csv -invoice_id $invoice_id]"
            if {$content eq ""} {
                lappend failed_invoice_ids $invoice_id
            } else {
                lappend csv_contents $content
			 db_dml exported_date "update im_costs set cost_status_id = [etm_coststatus_collectionagency] where cost_id=:invoice_id"
                db_dml exported_date "update im_invoices set exported_to_collagency_date = :current_date where invoice_id=:invoice_id"
            }
        }
        
        if {$csv_contents ne ""} {
            set csv_content [join $csv_contents "\r\n"]
            
            set inkasso_csv [ns_tmpnam]
            set file [open $inkasso_csv w]
            fconfigure $file -encoding "iso8859-1"
            puts $file $csv_content
            flush $file
            close $file
           
            file rename $inkasso_csv ${zip_dir}/${company_name}.inkasso.${current_date_formatted}.csv
            set file_created_p 1
            
        }
    
}


if {$file_created_p} {
    # Generate the zipfile
    set zip_file [ns_tmpnam]
    exec zip -r $zip_file ${zip_dir}
    
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"inkasso_$current_date_formatted.zip\""
    ns_returnfile 200 application/zip ${zip_file}.zip
    
    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Inkasso Export" -body "Failed downloads: $failed_invoice_ids "
    
    file delete -force ${zip_file}.zip
    file delete -force $zip_dir
} else {
    ad_return_error "No new bookings" "No file generated, no new invoices found"
}