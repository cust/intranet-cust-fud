# packages/intranet-cust-fud/www/cost_status.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	change fl-status
    
    @author etm
    @creation-date 2018-10-29
    @cvs-id $Id$
} {
    
    
    user_id:notnull
    company_status_id:optional
    return_url:optional
    member_status:optional
} 


#change user status
if {[catch {
	acs_user::change_state -user_id $user_id -state $member_state
	} errmsg]} {
	ad_return_error "Database Update Failed" "Database update failed with the following error:
	<pre>$errmsg</pre>"
	}


#if user is first contact change company to new status, too

set users_company_id [db_string users_company_id "select company_id as users_company_id from im_companies where primary_contact_id = :user_id" -default ""]


if { "" != $users_company_id } {
	db_dml upd "update im_companies set company_status_id = $company_status_id where company_id =  :users_company_id"
}


				
if { "" == $return_url } { set return_url "/intranet/users/view?user_id=$user_id" }
ad_returnredirect $return_url 



