# packages/intranet-cust-fud/www/ma.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	List missing invoice numbers
    
    @author etm
    @creation-date 2018-04-19
    @cvs-id $Id$
} {
    { start_date "" }
    { end_date "" }
    { output_format "html" }
} 



# ------------------------------------------------------------
# Argument Checking

# Check that Start & End-Date have correct format
if {"" != $start_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
}

if {"" != $end_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $end_date]} {
    ad_return_complaint 1 "End Date doesn't have the right format.<br>
    Current value: '$end_date'<br>
    Expected format: 'YYYY-MM-DD'"
}


# ------------------------------------------------------------
# Page Settings

set page_title "Missing Invoices"
set context_bar [im_context_bar $page_title]
set context ""

set help_text "find missing invoice numbers"

# ------------------------------------------------------------
# Set the default start and end date
if {"" == $start_date} {set start_date "2017-01-01"}
if {"" == $end_date} {set end_date "2099-12-01"}


set missing_html "<table><tr><td>\n"
set inv_nr_not_used ""

foreach ag {fud pan zis} {

	switch $ag {

		fud {set invformat 908917}
		pan {set invformat 31317}
		zis {set invformat 50117}		
	}

	set first_inv [db_string last "select min(invoice_nr) as last_inv from im_invoices where invoice_nr like ('${invformat}%')"] 
	set last_inv [db_string last "select max(invoice_nr) as last_inv from im_invoices where invoice_nr like ('${invformat}%')"] 



		for {set i $first_inv} {$i < $last_inv} {incr i} {
		set check_number [expr $i + 1]
		set inv_exists_p  [db_string invexists "select invoice_nr from im_invoices where invoice_nr = :check_number" -default "f"]
		set lastinv_date  [db_string invexists "select to_char(effective_date, 'YYYY-MM-DD') as inv_date from im_costs where cost_name = :check_number" -default ""]

		
		if {$inv_exists_p == "f" } {
			#lappend inv_nr_not_used "<tr><td>$ag\t$check_number</tr></td>"   
			append inv_nr_not_used "<tr><td>$ag</td><td>$check_number</td><td>$lastinv_date</td></tr>"   
			} else { continue  }

		#set inv_nr_not_used "<td>check inv: $i (not used: $inv_nr_not_used )</td>\n"

		} 

	
}
#set missing_html $inv_nr_not_used
append missing_html "$inv_nr_not_used</td></tr></table>\n"

# # Report Global Header Line
# set header0 {"ag-nr"}



# # Report Body
# set report_def [list \
#     group_by project_id \
#     header {
# 	$missing_html
#     } \
#     content {} \
#     footer {} \
# ]






# ------------------------------------------------------------
# Start formatting the page header
#

# Write out HTTP header, considering CSV/MS-Excel formatting
#im_report_write_http_headers -output_format $output_format

# Add the HTML select box to the head of the page
# switch $output_format {
#     html {
# 	ns_write "
# 	[im_header]
# 	[im_navbar]
# 	<table cellspacing=0 cellpadding=0 border=0>
# 	<tr valign=top>
# 	<td>
# 	<form>
# 		[export_form_vars customer_id]
# 		<table border=0 cellspacing=1 cellpadding=1>
# 		<tr>
# 		  <td class=form-label>Start Date</td>
# 		  <td class=form-widget>
# 		    <input type=textfield name=start_date value=$start_date>
# 		  </td>
# 		</tr>
# 		<tr>
# 		  <td class=form-label>End Date</td>
# 		  <td class=form-widget>
# 		    <input type=textfield name=end_date value=$end_date>
# 		  </td>
# 		</tr>
# 		<tr>
# 			<td class=form-label>Format</td>
# 			<td class=form-widget>
# 			[im_report_output_format_select output_format "" $output_format]
# 			</td>
# 		</tr>
# 		<tr>
# 		  <td class=form-label></td>
# 		  <td class=form-widget><input type=submit value=Submit></td>
# 		</tr>
# 		</table>
# 	</form>
# 	</td>
# 	<td>
# 		<table cellspacing=2 width=90%>
# 		<tr><td>$help_text</td></tr>
# 		</table>
# 	</td>
# 	</tr>
# 	</table>
# 	<table border=0 cellspacing=3 cellpadding=3>\n"
#     }
# }

 #    im_report_display_footer \
	# -output_format $output_format \
	# -group_def $report_def 
	


