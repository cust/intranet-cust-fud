# /packages/intranet-translation/projects/new.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: form to quickly add a translation project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    {project_type_id ""}
    {project_status_id:integer,optional}
    {company_id:integer}
    {parent_id ""}
    {project_nr ""}
    {project_name ""}
    {workflow_key ""}
    {return_url ""}
    target_language_ids:multiple,optional
}

if {![exists_and_not_null company_id]} {
    ad_returnredirect [export_vars -base "/intranet-translation/projects/new" -url {project_type_id parent_id project_nr project_name workflow_key}]
}
# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
if {"" != $parent_id} {
    im_project_permissions $user_id $parent_id view read write admin
    if {$admin} { set perm_p 1 }
}
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 }
if {!$perm_p} {
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}
    
# --------------------------------------------
# Create Form
# --------------------------------------------

set form_id "project-ae"


set target_language_options [db_list_of_lists languages "select category,category_id from im_categories where category_type = 'Intranet Translation Language' and enabled_p = 't' and length(category)>2 order by sort_order,category"]

set project_lead_list "
select 	im_name_from_user_id(u.user_id),u.user_id
from registered_users u, group_distinct_member_map gm
where u.user_id = gm.member_id and gm.group_id in (select group_id from groups where group_name = 'Project Managers')
order by lower(im_name_from_user_id(u.user_id))"


set project_lead_options [db_list_of_lists  lead $project_lead_list]



# Get the final company options
set final_company_options [db_list_of_lists final_companies "select company_name, company_id 
							    from im_companies 
							    where company_type_id in (
							      select * from im_sub_categories(10247)) 
							      and company_status_id in (select * from im_sub_categories(46)) 
							    order by lower(company_name)"]
# Add optional
set final_company_options [concat [list [list "" ""]] $final_company_options]

# Get the contact options
set customer_group_id [im_customer_group_id]
set freelance_group_id [im_freelance_group_id]

set company_contacts_sql "
select DISTINCT
    im_name_from_user_id(u.user_id) as user_name,
    u.user_id
from
    cc_users u,
    group_distinct_member_map m,
    acs_rels ur
where
    u.member_state = 'approved'
    and u.user_id = m.member_id
    and m.group_id in (:customer_group_id, :freelance_group_id)
    and u.user_id = ur.object_id_two
    and ur.object_id_one = :company_id
    and ur.object_id_one != 0
"

set company_contact_options [db_list_of_lists contacts $company_contacts_sql]
ad_form -name $form_id -html { enctype multipart/form-data } -action /intranet-cust-fud/project-quick -cancel_url $return_url -form {
    project_id:key
    project_type_id:text(hidden)
    {company_id:integer(hidden) {value $company_id}}
    {company_contact_id:text(select),optional
        {label "[_ intranet-translation.Client_contact]"}
        {options "$company_contact_options"}
    }
    {source_language_id:text(select)
        {label "[_ intranet-translation.Source_Language]"}
        {options "$target_language_options"}
    }
    {target_language_ids:text(jq_multiselect)
        {label "[_ intranet-translation.Target_Languages]"}
        {options "$target_language_options"}
    }
}

# ---------------------------------------------------------------
# Deal with additional skills
# ---------------------------------------------------------------

if {[apm_package_installed_p "intranet-freelance"]} {
    # Append the skills attributes
    im_freelance_append_skills_to_form \
        -object_subtype_id $project_type_id \
        -form_id $form_id
}

if {[lsearch [template::form::get_elements $form_id] subject_area_id]<0} {
    ad_form -extend -name $form_id -form {
        {subject_area_id:integer(im_category_tree),optional
            {label "[_ intranet-translation.Subject_Area]"}
            {custom {category_type "Intranet Translation Subject Area" translate_p 1}}
        }
    }
}

ad_form -extend -name $form_id -form {
    {project_lead_id:text(select),optional
        {label "Project lead"}
        {options "$project_lead_options"}
        {value $user_id}
    }
    {processing_time:float(text),optional
        {label "Processing Time"}
    }
    {certified_translation:boolean(checkbox),optional
        {label "Certified Translation"}
        {options {{"" t}}}
    }
    {final_company_id:integer(select),optional
        {label "[_ intranet-translation.Final_User]"}
        {options "$final_company_options"}
    }
    {description:text(textarea),optional
        {label "[_ intranet-core.Description]"}
    }
    {upload_file:file(file),optional
        {label "[_ intranet-translation.Source_File]"}
        {help_text "The Source file can be a zip folder with all source files."}
    }

} -on_request {
} -on_submit {

    # Permission check. Cases include a user with full add_projects rights,
    # but also a freelancer updating an existing project or a freelancer
    # creating a sub-project of a project he or she can admin.
    set perm_p 0
    
    # Check for the case that this guy is a freelance
    # project manager of the project or similar...
    im_project_permissions $user_id $project_id view read write admin
    if {$write} { set perm_p 1 }
    
    # Check if the user has admin rights on the parent_id
    # to allow freelancers to add sub-projects
    if {"" != $parent_id} {
        im_project_permissions $user_id $parent_id view read write admin
        if {$write} { set perm_p 1 }
    }

    # Users with "add_projects" privilege can always create new projects...
    if {[im_permission $user_id add_projects]} { set perm_p 1 }

    if {!$perm_p} {
        ad_return_complaint "Insufficient Privileges" "<li>You don't have sufficient privileges to see this page."
        ad_script_abort
    }
    
} -new_data {

    set target_language_ids [element get_values $form_id target_language_ids]
    set project_nr [im_next_project_nr -customer_id $company_id -parent_id $parent_id]
    set project_name $project_nr
    set interco_company_id [etm_agency_by_employee_dept]

    set project_id [im_translation_create_project \
        -company_id $company_id \
        -project_name $project_name \
        -project_nr $project_nr \
        -project_type_id $project_type_id \
        -project_lead_id $project_lead_id \
        -source_language_id $source_language_id \
        -target_language_ids $target_language_ids \
        -subject_area_id $subject_area_id \
        -final_company_id $final_company_id \
	-no_callback]

    if {$project_id eq 0} {
	# There was  an error creating the project, try to find out if it is because it was a duplicate
	set project_id [db_string project_id "select project_id from im_projects where
			(	upper(trim(project_name)) = upper(trim(:project_name)) OR
				upper(trim(project_nr)) = upper(trim(:project_nr)) OR
				upper(trim(project_path)) = upper(trim(:project_name))
			)" -default 0]
	
	ns_log Notice "Tried to find the project ... $project_id .. $project_nr"
	if {$project_id eq 0} {
	    set project_id $random_errors_token
	    set error_msg "Failed to create project. We sadly don't know why"
	    ad_return_error "Project creation failed " $error_msg
	} 
    } else {

	# -----------------------------------------------------------------
	# Call the "project_create" or "project_update" user_exit
	
	im_user_exit_call project_create $project_id
    
	db_dml update_project "update im_projects 
				set company_contact_id = :company_contact_id, 
				    interco_company_id = :interco_company_id,
				    project_status_id = [im_project_status_potential], 
				    certified_translation = :certified_translation, 
				    processing_time = :processing_time, 
				    description = :description 
				 where project_id = :project_id"
        
	# ---------------------------------------------------------------
	# Make sure we have the skills
	# ---------------------------------------------------------------
	im_freelance_store_skills_from_form \
	    -object_subtype_id $project_type_id \
	    -form_id $form_id \
	    -object_id $project_id
            
            
	# ---------------------------------------------------------------
	# We need to store source and target language again
	# ---------------------------------------------------------------
	foreach lang $target_language_ids {
	    im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
	}
	im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
	
	if {[lsearch [template::form::get_elements $form_id] expected_quality_id]>-1} {
	    db_dml update_quality "update im_projects set expected_quality_id = :expected_quality_id where project_id = :project_id"
	}
    
	# ---------------------------------------------------------------------
	# Create the directory structure necessary for the project
	# ---------------------------------------------------------------------
	
	if {[exists_and_not_null upload_file]} {
	    # Save the file in the correct directory
	    set project_dir [im_filestorage_project_path $project_id]
	    set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
	    set source_dir "$project_dir/${source_folder}"
	    set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
	    set filename [template::util::file::get_property filename $upload_file]
	    
	    file copy $tmp_filename ${source_dir}/$filename
	    
	    if {[file extension $filename] eq ".zip"} {
		exec unzip -d ${source_dir} ${source_dir}/$filename
		file delete -force ${source_dir}/$filename
	    }
	    
	    #callbacks to MemoQ & Trados
	    callback im_project_after_file_upload -project_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]
	    set create_invoice_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
	    
	    # Check for the uploaded files
	    set uploaded_files [glob -nocomplain -directory $source_dir -type f *]
	    
	    foreach file $uploaded_files {
		set task_filename [file tail $file]
		set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
		if {$count_created_tasks eq 0} {
		    set task_uom_id [im_uom_s_word] 
		    ns_log Debug "Inserted $task_filename for project: $project_id"
		    im_task_insert $project_id $task_filename $task_filename 0 $task_uom_id $project_type_id $target_language_ids
		}
	    }
	    
	    # Check again if we have created tasks
	    set create_invoice_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
	    
	    if {$create_invoice_p} {
		set quote_id [im_trans_invoice_create_from_tasks -project_id $project_id -cost_type_id [im_cost_type_quote]]
	    } else {
		set quote_id ""
	    }
	    set cost_center_id [fud_cost_center_from_interco -interco_company_id $interco_company_id]
	}
    }
} -after_submit {
    
    # -----------------------------------------------------------------
    # Flush caches related to the project's information
    
    util_memoize_flush_regexp "im_project_has_type_helper.*"
    util_memoize_flush_regexp "db_list_of_lists company_info.*"
    
    if {[exists_and_not_null upload_file]} {
	if {$quote_id ne ""} {
	    ad_returnredirect [export_vars -base "/intranet-invoices/view" -url {{invoice_id $quote_id}}]
	} else {
	    ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-list" -url {project_id}]
	}
    } else {
        ad_returnredirect [export_vars -base "project-quick-2" -url {project_id}]
    }
    ad_script_abort
}
