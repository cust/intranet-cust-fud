# packages/intranet-cust-fud/www/cost_status.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	coststatus change
    
    @author etm
    @creation-date 2017-05-29
    @cvs-id $Id$
} {
    
    cost_status_id:notnull
    invoice_id:notnull
    return_url
} 



db_dml cost_stat_upd " update im_costs set cost_status_id = :cost_status_id where cost_id = :invoice_id "


if { "" == $return_url } { set return_url "/intranet-invoices/view?invoice_id=$invoice_id" }
ad_returnredirect $return_url 



